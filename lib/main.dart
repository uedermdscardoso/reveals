
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:bridge/widgets/auth/anonymous_profile_screen.dart';
import 'package:bridge/widgets/auth/waiting_list_message.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/store/main_store.dart';
import 'package:provider/provider.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:camera/camera.dart';
import 'package:bridge/widgets/auth/welcome_message.dart';
import 'package:flutter/services.dart';
import 'home.dart';
import 'AppLanguage.dart';
import 'Translate.dart';

//create icon resource
//https://romannurik.github.io/AndroidAssetStudio/
//https://romannurik.github.io/AndroidAssetStudio/icons-launcher.html

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  final AppLanguage appLanguage = AppLanguage();
  await appLanguage.fetchLocale();

  final List<CameraDescription> cameras = await availableCameras();
  await mainStore.addCameras(cameras: cameras);

  await Firebase.initializeApp();
  FirebaseDatabase.instance.setPersistenceEnabled(true);

  final MediaService mediaService = MediaService();
  final UserService userService = UserService();

  await mediaService.createMediaViewTable(); //Creating database musly_db.db
  await userService.createUserTable();

  if (FirebaseAuth.instance.currentUser == null) {
    final AuthenticationService _authService = AuthenticationService();
    _authService.doAnonymousLogin();
  }

  runApp(App(appLanguage: appLanguage));
}

final mainStore = MainStore();

//gradlew signingReport
class App extends StatefulWidget {

  AppLanguage appLanguage;

  App({ Key? key, required this.appLanguage }) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {

  final AuthenticationService _authService = AuthenticationService();
  final UserService _userService = UserService();

  late String _token;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    mainStore.auth.addLocalUser();
  }

  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider<AppLanguage>(
      create: (_) => widget.appLanguage,
      child: Consumer<AppLanguage>(
        builder: (context, model, child) {

          return MaterialApp(
            debugShowCheckedModeBanner: false,
            debugShowMaterialGrid: false,
            home: FutureBuilder(
              future: _userService.getToken(),
              builder: (context, snapshot) {

                if(snapshot.hasData) {

                  _token = snapshot.data as String;

                  return  Visibility(
                    visible: _token.isEmpty,
                    child: Observer(
                      builder: (_) {

                        return FutureBuilder(
                          future: mainStore.auth.localUser,
                          builder: (context, snapshot) {

                          if(snapshot.hasData) {
                            final Users localUser = snapshot.data as Users;
                            final bool isWaiting = localUser != null && localUser.phone != null;

                            if(isWaiting) {
                              return Container(color: Colors.yellow);
                              //return WaitingListMessage();
                            } else {
                              if(!_authService.isAnonymous()) {
                                return Container(color: Colors.pink);
                                //return Home();
                              }

                              return Container(color: Colors.green);
                              //return WelcomeMessage();
                            }
                          }

                          return Container(color: Colors.red);
                          //return AnonymousProfileScreen();
                          },
                        );
                      }
                    ),
                    replacement: Container(color: Colors.amber), //Home(),
                  );
                }

                return Container(color: Colors.blue);
              },
            ),
            locale: model.appLocal, //Locale('en'),
            supportedLocales: const [
              Locale('en', 'US'),
              Locale('es', ''),
              Locale('pt', ''),
            ],
            localizationsDelegates: const [
              Translate.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
          );
        },
      ),
    );
  }
}