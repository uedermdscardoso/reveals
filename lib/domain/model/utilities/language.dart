
class Language {

  late String text;
  late String abbreviation;
  late bool isChecked;

  Language({ required this.text, required this.abbreviation, required this.isChecked });

}