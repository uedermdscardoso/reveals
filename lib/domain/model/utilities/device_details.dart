
class DeviceDetails {

  late String brand;
  late String device;
  late String os;
  late String model;
  late String version;
  late bool isPhysicalDevice;

  DeviceDetails({
    required this.brand,
    required this.device,
    required this.os,
    required this.model,
    required this.version,
    this.isPhysicalDevice : true,
  });
}