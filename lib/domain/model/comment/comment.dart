
import 'package:bridge/domain/model/user/users.dart';

class Comment {

  String? id;
  int? createdIn;
  String? message;
  Users? author;

  num? likes;

  Comment({ this.id, this.createdIn, this.message, this.author, this.likes });

}