
import 'package:bridge/domain/model/media/media.dart';

class Users {

  late String id;
  late int createdIn;
  late String firstName;
  late String lastName;
  late String username;
  late String phone;
  late String photo;
  late String description;
  late int totalFollowers;
  late int totalFollowing;
  late int totalLikes;
  late List<Media> medias;
  late String twitter;
  late String instagram;
  late String tiktok;

  //waiting list
  bool exited = false;
  bool invited = false;
  late String invitedBy;

  Users();

  Users.toInvite({
    required this.firstName, required this.lastName, required this.username,
    required this.phone, required this.exited, required this.invited,
    required this.invitedBy });

  Users.toShortDetails({
    required this.id, required this.firstName, required this.lastName,
    required this.username, required this.photo });

  Users.fromWaitingList({
    required this.firstName, required this.lastName,
    required this.username, required this.phone });

  Users.toLongDetails({
    required this.id, required this.description, required this.photo,
    required this.twitter, required this.instagram, required this.tiktok,
    required this.totalFollowers, required this.totalFollowing,
    required this.totalLikes, required this.firstName, required this.lastName,
    required this.username, required this.phone, required this.invitedBy });

  Users.onlyId({ required String id }) {
    this.id = id;
  }

  Users.onlyPhone({ required String phone }) {
    this.phone = phone;
  }

  Users.onlyUsername({ required String username }) {
    this.username = username;
  }

  Users.onlyPhoneAndUsername({ required String phone, required String username }) {
    this.phone = phone;
    this.username = username;
  }

  Users.onlyIdAndUsername({ required String id, required String username }) {
    this.id = id;
    this.username;
  }

  Users.onlyIdAndDescription({ required String id, required String description }) {
    this.id = id;
    this.description = description;
  }

  Users.findAuthor({ required String id, required String username, required String photo }) {
    this.id = id;
    this.username = username;
    this.photo = photo;
  }

}
