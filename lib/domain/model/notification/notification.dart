
class Notification {
  int? createdIn;
  String? docId;
  String? firedBy;
  String? message;
  String? received;
  String? type;

  Notification({ this.createdIn, this.docId, this.firedBy, this.message,
      this.received, this.type });

}