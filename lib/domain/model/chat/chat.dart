
class Chat {

  num? chatId;
  late int createdIn;
  late String from;
  late String to;
  late String message;

  Chat({ this.chatId, required this.createdIn, required this.from, required this.to,
    required this.message });

}