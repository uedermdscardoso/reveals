
import 'package:bridge/domain/model/user/users.dart';

class Feedbacks {

  int? createdIn;
  String? message;
  Users? sentBy;

  Feedbacks({ this.createdIn, this.message, this.sentBy });

}