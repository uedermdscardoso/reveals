
class Effect {

  String? id;
  String? name;
  String? syntax;
  String? image;
  bool? unavailable;

  Effect({ this.id, this.name, this.syntax, this.image, this.unavailable });

}