
enum MediaScreenType {
  DEFAULT,
  HOME,
  SEARCH,
  NOTIFICATION,
  MINI,
  LIKED_AUDIO,
  AUDIOBOOK,
  PODCAST,
}