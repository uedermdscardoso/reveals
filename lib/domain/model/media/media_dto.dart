
import 'package:bridge/domain/model/media/media_type.dart';
import 'package:flutter_cache_manager/file.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/domain/model/user/users.dart';

class MediaDTO {

  late String id;
  late num createdIn;
  late String path;

  late MediaType type;
  late String title;
  late String message;
  late String filename;
  late String author;
  late String image;
  late String locale;
  late bool isOfficial;
  late String linkId;
  late String linkType;

  late int duration;
  late num comments;
  late num likes;
  late num shared;
  late num totalView;
  late bool wasHeard; //foi ouvido

  Media? media;
  Users? createdBy;

  List<dynamic>? hashtags;
  File? file;
  File? imageFile;

  bool isPlaying = false;
  bool isSaved = false;
  bool isRecorded = false;
  bool isLikedByCurrentUser = false;

  MediaDTO({
    required Media media,
    required String title,
    required String message,
    required List<dynamic> hashtags,
    required String locale,
    String author : "",
    bool isOfficial : false,
    required String linkId,
    required String linkType,
  }) {
    this.id = media.id;
    this.createdIn = media.createdIn;
    this.path = media.path;
    this.type = media.type;
    this.title = media.title;
    this.message = media.message;
    this.filename = media.filename;
    this.author = media.author;
    this.image = media.image;
    this.locale = media.locale;
    this.isOfficial = media.isOfficial;
    this.linkId = media.linkId;
    this.linkType = media.linkType;
    this.duration = media.duration;
    this.comments = media.comments;
    this.likes = media.likes;
    this.shared = media.shared;
    this.totalView = media.totalView;
    this.wasHeard = media.wasHeard;
    this.createdBy = media.createdBy;
    this.isPlaying = media.isPlaying;
    this.file = media.mediaFile as File;
    this.imageFile = media.imageFile as File;
    this.hashtags = media.hashtags;
    this.isSaved = media.isSaved;
    this.isRecorded = media.isRecorded;
    this.isLikedByCurrentUser = media.isLikedByCurrentUser;
  }

}