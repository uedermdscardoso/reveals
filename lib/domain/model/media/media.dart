
import 'dart:io';

import 'package:bridge/domain/model/media/cutted_media_dto.dart';
import 'package:bridge/domain/model/media/media_type.dart';
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:bridge/domain/model/user/users.dart';

class Media {

  late String id;
  late num createdIn;
  late String path;
  late MediaType type;
  late String title;
  late String message;
  late String filename;
  late int duration;
  late String author;
  late String image;
  late String locale;
  late bool isOfficial;
  late String linkId;
  late String linkType;
  late num comments;
  late num likes;
  late num shared;
  late int totalView;
  late bool wasHeard;
  late List<dynamic> hashtags;

  Users? createdBy;

  //in memory
  bool isPlaying = false;
  bool isSaved = false;
  bool isRecorded = false;
  bool isLikedByCurrentUser = false;

  CuttedMediaDTO? originalMedia; //stitched audio
  File? imageFile;
  File? mediaFile;

  Media();

  Media.onlyMediaFile({ required mediaFile });
  Media.onlyId({ required this.id });
  Media.onlyIdAndPath({ required this.id, required this.path });

  Media.toLongDetails({
    required this.title,
    required this.createdBy,
    required this.message,
    required this.mediaFile,
    required this.type,
    required this.isOfficial,
    required this.duration,
    required this.isLikedByCurrentUser,
  });

  Media.toShortDetails({
    required this.id,
    required this.type,
    required this.createdIn,
    required this.path,
    required this.createdBy,
  });

  Media.withLongAudio({
    required this.id,
    required this.title,
    required this.duration,
    required this.image,
    required this.linkType,
  });

  Media.forNewMedia({
    required this.id,
    required this.filename,
    required this.duration,
    required this.createdBy,
    required this.mediaFile,
    required this.imageFile,
    required this.originalMedia,
  });

  Media.forLiveDetails({
    required this.id,
    required this.createdBy,
    required this.path,
    required this.locale,
    this.isOfficial : false,
  });

  Media.withCloudSound({
    required this.id,
    required this.type,
    required this.title,
    required this.duration,
    required this.path,
    required this.image,
    required this.createdBy,
    required this.isSaved,
    required this.isOfficial,
  });

  Media.withLocalSound({
    required this.title,
    required this.type,
    required this.mediaFile,
    required this.path,
    required this.isSaved,
    required this.isRecorded,
    required this.isOfficial,
    required this.duration,
    required this.createdIn,
    required this.createdBy,
  });

  Media.findMedia({
    required this.id,
    required this.type,
    required this.title,
    required this.message,
    required this.duration,
    required this.path,
    required this.image,
    required this.createdIn,
    required this.author,
    required this.createdBy,
  });

  Media.createMedia({
    required this.id,
    required this.type,
    required this.createdIn,
    required this.createdBy,
    required this.path,
    required this.title,
    required this.image,
    required this.author,
    required this.message,
    required this.duration,
    required this.likes,
    required this.comments,
    required this.shared,
    required this.totalView,
    required this.wasHeard,
  });

  Media.fromJsonOfficial(Map<String,dynamic> json, Users createdBy, bool isLiked):
        id=json['id'] ?? '',
        type=json['type'] ?? '',
        createdIn=json['createdIn'] ?? 0,
        createdBy=createdBy,
        isLikedByCurrentUser=isLiked,
        path=json['path'] ?? '',
        title=json['title'] ?? '',
        message=json['message'] ?? '',
        image=json['image'] ?? '',
        linkId=json['linkId'] ?? "",
        linkType=json['linkType'] ?? "",
        duration=json['duration'] ?? '',
        author=json['author'] ?? '',
        likes=json['likes'] ?? 0,
        comments=json['comments'] ?? 0,
        shared=json['shared'] ?? 0,
        totalView=json['totalView'] ?? 0,
        wasHeard=json['wasHeard'] ?? false,
        hashtags=json['hashtag'] ?? [],
        originalMedia=CuttedMediaDTO.deserialize(data: json['originalMedia'] ?? {});

  Media.withAudio(Map<String,dynamic> json, Users createdBy, bool isLiked, File audio):
    id=json['id'] ?? '',
    type=json['type'] ?? '',
    createdIn=json['createdIn'] ?? 0,
    createdBy=createdBy,
    isLikedByCurrentUser=isLiked,
    path=json['path'] ?? '',
    title=json['title'] ?? '',
    message=json['message'] ?? '',
    image=json['image'] ?? '',
    linkId=json['linkId'] ?? '',
    linkType=json['linkType'] ?? '',
    duration=json['duration'] ?? 0,
    author=json['author'] ?? '',
    likes=json['likes'] ?? 0,
    comments=json['comments'] ?? 0,
    shared=json['shared'] ?? 0,
    totalView=json['totalView'] ?? 0,
    wasHeard=json['wasHeard'] ?? false,
    hashtags=json['hashtag'] ?? [],
    originalMedia=CuttedMediaDTO.fromJson(data: json['originalMedia'] ?? {}, audio: audio);
  }