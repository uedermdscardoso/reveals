
import 'dart:io';

class CuttedMedia {

  late String id;
  late String image;
  late String author;
  late int startDuration;
  late int duration;

  CuttedMedia({
    this.id : '',
    this.image : '',
    this.author : '',
    this.startDuration : 0,
    this.duration : 0,
  });

  CuttedMedia.fromJson(Map<String, dynamic> data):
      id=data['id'],
      image=data['image'],
      author=data['author'],
      startDuration=data['startDuration'],
      duration=data['duration'];

}