
import 'dart:io';

import 'package:bridge/domain/model/media/cutted_media.dart';

class CuttedMediaDTO {

  CuttedMedia? cuttedMedia;

  String? id;
  late String image;
  late String author;
  late int startDuration;
  late int duration;

  File? audio;

  CuttedMediaDTO({ required CuttedMedia cuttedMedia }) {
    this.id = cuttedMedia.id;
    this.image = cuttedMedia.image;
    this.author = cuttedMedia.author;
    this.startDuration = cuttedMedia.startDuration;
    this.duration = cuttedMedia.duration;
  }

  CuttedMediaDTO.withAudio({ required CuttedMedia cuttedMedia, required File audio }) {
    this.id = cuttedMedia.id;
    this.image = cuttedMedia.image;
    this.author = cuttedMedia.author;
    this.startDuration = cuttedMedia.startDuration;
    this.duration = cuttedMedia.duration;
    this.audio = audio;
  }

  CuttedMediaDTO.deserialize({ required Map<String, dynamic> data }):
        this.id=data['id'] ?? '',
        this.image=data['image'] ?? '',
        this.author=data['author'] ?? '',
        this.startDuration=data['startDuration'] ?? 0,
        this.duration=data['duration'] ?? 0;

  CuttedMediaDTO.fromJson({ required Map<String, dynamic> data, required File audio }):
     this.id=data['id'] ?? '',
     this.image=data['image'] ?? '',
     this.author=data['author'] ?? '',
     this.startDuration=data['startDuration'] ?? 0,
     this.duration=data['duration'] ?? 0,
     this.audio=audio;

}