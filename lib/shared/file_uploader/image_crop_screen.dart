
import 'dart:io';
import 'dart:typed_data';

import 'package:image_crop/image_crop.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/domain/model/utilities/file_uploader_type.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:opencv_4/factory/pathfrom.dart';
import 'package:opencv_4/opencv_4.dart';
import 'package:uuid/uuid.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:path_provider/path_provider.dart';
import 'package:bridge/widgets/header.dart';

class ImageCropScreen extends StatelessWidget {

  final AuthenticationService authService = AuthenticationService();
  final UserService userService = UserService();

  final cropKey = GlobalKey<CropState>();
  final File image;

  late Translate _intl;

  ImageCropScreen({ Key? key, required this.image }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarDividerColor: Colors.black,
      ),
      child: Material(
        color: Colors.black,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(
              color: Colors.transparent,
              left: Row(
                children: [
                  GestureDetector(
                      onTap:() => Navigator.pop(context),
                      child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25)
                  ),
                  const SizedBox(width: 4),
                  Text('${ _intl.translate('SCREEN.PHOTO_GRID.IMAGE_CROP.TITLE') }',
                      style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 18, fontWeight: FontWeight.bold, fontFamily: 'Arial')),
                ],
              ),
            ),
            Expanded(
              child: Crop(
                key: cropKey,
                image: FileImage(image, scale: 2.0),
                aspectRatio: 4.0 / 3.0,
                onImageError: (_, e) {
                  print('error: ${ e }');
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 25, right: 20),
              child: GestureDetector(
                onTap: () async => _save(context: context, image: image),
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    padding: EdgeInsets.only(left: 30, right: 30),
                    width: 150,
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(100),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset('assets/icons/cut_photo.png', scale: 3.0),
                        const SizedBox(width: 12),
                        Text('${ _intl.translate('SCREEN.PHOTO_GRID.IMAGE_CROP.BUTTON.CUT') }',
                            style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 18, fontWeight: FontWeight.bold, fontFamily: 'Arial'))
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<File?> _convertImageToGrayScale({ required File file }) async {
    Uint8List? bytes;

    bytes = await Cv2.cvtColor(
      pathFrom: CVPathFrom.GALLERY_CAMERA,
      pathString: file.path,
      outputType: Cv2.COLOR_BGR2GRAY,
    );

    if(bytes != null) {
      Directory tempDir = await getTemporaryDirectory();
      String tempPath = tempDir.path;

      return File(tempPath + '/${ Uuid().v4() }.tmp').writeAsBytes(bytes);
    }
  }

  _save({ required BuildContext context, required File image }) async {
    final ConnectivityResult connectivityResult = await (Connectivity().checkConnectivity());
    final nav = Navigator.of(context);

    if(connectivityResult != ConnectivityResult.none){

      final crop = cropKey.currentState;

      if(crop != null && crop.scale != null) {

        final File croppedFile = await ImageCrop.sampleImage(
          file: image,
          preferredWidth: (1024 / crop.scale).round(),
          preferredHeight: (4096 / crop.scale).round(),
        );

        final File? newImage = await _convertImageToGrayScale(file: croppedFile);
        await mainStore.imageChossing.addTempImage(image: newImage!);

        final FileUploaderType type = await mainStore.imageChossing.type;

        if(type == FileUploaderType.PROFILE) {
          await mainStore.imageChossing.addTempImage(image: newImage);

          //final Users currentUser = await authService.getCurrentUser();

          //await userService.uploadPhoto(user: currentUser, photo: newImage);

          await mainStore.imageChossing.clear();
        }
      }

      nav.pop();

    } else
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.PHOTO_GRID.IMAGE_CROP.ALERT.NO_CONNECTION') }');

    nav.pop();
    nav.pop();
  }

}
