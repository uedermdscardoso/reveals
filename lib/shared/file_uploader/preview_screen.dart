
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/file_uploader/image_crop_screen.dart';
import 'package:bridge/widgets/header.dart';

class PreviewScreen extends StatefulWidget {

  final File preview;

  const PreviewScreen({ Key? key, required this.preview }) : super(key: key);

  @override
  _PreviewScreenState createState() => _PreviewScreenState();
}

class _PreviewScreenState extends State<PreviewScreen> {

  late Translate _intl;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
        systemNavigationBarDividerColor: Colors.black,
      ),
      child: Material(
        color: Colors.black,
        child: Column(
          children: [
            Header(
              color: Colors.transparent,
              left: Row(
                children: [
                  GestureDetector(
                    onTap:() => Navigator.pop(context),
                    child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25)
                  ),
                  const SizedBox(width: 4),
                  Text('${ _intl.translate('SCREEN.PHOTO_GRID.PREVIEW.TITLE') }',
                      style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 18, fontWeight: FontWeight.bold, fontFamily: 'Arial')),
                ],
              ),
            ),
            Expanded(
              child: Image.file(widget.preview, filterQuality: FilterQuality.low, fit: BoxFit.fitWidth),
            ),
            Container(
              height: 75,
              child: Padding(
                padding: const EdgeInsets.only(right: 40),
                child: GestureDetector(
                  onTap: () async {
                    Navigator.push(context, UtilService.createRoute(ImageCropScreen(image: widget.preview)));
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text('${ _intl.translate('SCREEN.PHOTO_GRID.PREVIEW.BUTTON.CONFIRM') }',
                      style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 18, fontWeight: FontWeight.bold, fontFamily: 'Arial')),
                  )
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
