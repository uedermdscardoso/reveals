

import 'package:flutter/material.dart';
import 'package:bridge/Translate.dart';

class MultiModalDialog extends StatefulWidget {

  String title;
  List<String> buttons;
  List values;

  MultiModalDialog({ required this.title, required this.buttons, required this.values });

  @override
  _MultiModalDialogState createState() => _MultiModalDialogState();
}

class _MultiModalDialogState extends State<MultiModalDialog> {

  int _isButtonPressed = -1;

  late Translate _intl;
  List<String> _buttons = [];

  @override
  void initState() {
    _addKey();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _addKey() {
    _buttons = widget.buttons;
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return SimpleDialog(
      title: Text(widget.title, style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal)),
      elevation: 5,
      children: List.generate(_buttons.length, (index) {

        return Padding(
          padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
          child: ElevatedButton(
              onPressed: () {
                if(_buttons != null && _buttons.isNotEmpty) {
                  setState(() => _isButtonPressed = index);
                }

                if(widget.values != null && widget.values.isNotEmpty && widget.values.length == _buttons.length)
                  Navigator.pop(context, widget.values.elementAt(index));
              },
              onFocusChange: (pressed) {
                if(_buttons != null && _buttons.isNotEmpty) {
                  setState(() => _isButtonPressed = index);
                }
              },
              style: ElevatedButton.styleFrom(
                elevation: 0,
                backgroundColor: Colors.white,
                textStyle: TextStyle(color: _isButtonPressed == index ? Colors.white : Colors.black),
              ),
              child: Text('${ _buttons != null && _buttons.isNotEmpty ?
                _buttons.elementAt(index) : 'YES' }')
          ),
        );
      }),
    );
  }
}
