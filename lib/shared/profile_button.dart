
import 'package:flutter/material.dart';

class ProfileButton extends StatelessWidget {

  Widget child;
  Widget page;
  Color color;
  double width;
  double height;

  ProfileButton({Key? key,  required this.child, required this.width, required this.height, required this.color, required this.page }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(100),
      ),
      child: child,
    );
  }

}
