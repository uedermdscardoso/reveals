
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class AnimatedText extends StatefulWidget {

  ScrollController scrollCtrl;
  String? text;
  IconData? icon;
  Axis direction;
  double? width;
  MainAxisAlignment alignment;

  AnimatedText({Key? key,
    required this.scrollCtrl,
    this.text,
    this.icon,
    required this.direction,
    this.width,
    required this.alignment,
  }) : super(key: key);

  @override
  _AnimatedTextState createState() => _AnimatedTextState();
}

class _AnimatedTextState extends State<AnimatedText> with SingleTickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {

    return Container(
      height: 18,
      child: Row(
        mainAxisAlignment: widget.alignment,
        children: <Widget>[
          Icon(widget.icon, size: 15, color: Colors.white),
          const SizedBox(width: 5),
          Container(
            width: widget.width, //160,
            padding: EdgeInsets.only(top: 2),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ListView.builder(
                      scrollDirection: widget.direction,
                      controller: widget.scrollCtrl,
                      itemBuilder: (context, index) {
                        return Container(
                          child: Text(
                              "${ widget.text }       ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12, fontFamily: "Arial", fontWeight: FontWeight.normal, decoration: TextDecoration.none)
                          ),
                        );
                      }),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
