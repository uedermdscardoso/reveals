import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Translate {
  final Locale locale;

  Translate(this.locale);

  static Translate of(BuildContext context) {
    return (Localizations.of<Translate>(context, Translate))!;
  }

  static const LocalizationsDelegate<Translate> delegate = _TranslateDelegate();

  late Map<String, dynamic> _localizedStrings;

  Future<bool> load() async {
    String jsonString =
    await rootBundle.loadString('assets/i18n/${locale.languageCode}.json');
    Map<String, dynamic> jsonMap = json.decode(jsonString);

    _localizedStrings = jsonMap.map((key, value) {
      return MapEntry(key, value);
    });

    return true;
  }

  String translate(String key) {
    List<String> keys = key.split('.');
    if(keys.length > 0){
      var msgTranslate;
      keys.asMap().forEach((i,elem) {
        if(i == 0)
          msgTranslate = _localizedStrings[elem];
        if(msgTranslate[elem] != null)
          msgTranslate = msgTranslate[elem];
      });
      return msgTranslate;
    }
    return _localizedStrings[key];
  }

}

class _TranslateDelegate
    extends LocalizationsDelegate<Translate> {
  const _TranslateDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'es', 'pt'].contains(locale.languageCode); //add pt
  }

  @override
  Future<Translate> load(Locale locale) async {
    Translate localizations = new Translate(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(_TranslateDelegate old) => false;
}