
class TranslationService {

  static String getLanguage({ required String language }){
    if(language.compareTo('pt') == 0) {
      language = "Português";
    } else if(language.compareTo('es') == 0) {
      language = "Español";
    } else {
      language = "English";
    }
    return language;
  }

  static String getInitials({ required String initials }){
    if(initials.toLowerCase().compareTo('português') == 0) {
      initials = "pt";
    } else if(initials.toLowerCase().compareTo('español') == 0) {
      initials = "es";
    } else {
      initials = "en";
    }

    return initials;
  }

}