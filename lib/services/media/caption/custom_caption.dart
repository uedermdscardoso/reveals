
import 'dart:convert';

import 'package:bridge/services/media/caption/caption_timestamp.dart';

class CustomCaption {

  String? transcript;
  double? confidence;
  List<CaptionTimestamp>? timestamps;
  double? wordConfidence;

  CustomCaption({ this.transcript, this.confidence, this.timestamps, this.wordConfidence });

  CustomCaption.fromJson(Map<String, dynamic> data)
      : transcript = data['transcript'],
        confidence = data['confidence'],
        timestamps = List<CaptionTimestamp>.from((json.decode(data['timestamps']) as List).map((timestampJson) => CaptionTimestamp.fromJson(timestampJson)).toList()),
        wordConfidence = data['wordConfidence'];

  void setTranscript({ required String transcript }) => this.transcript = transcript;

}