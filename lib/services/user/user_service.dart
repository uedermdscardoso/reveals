
import 'package:bridge/domain/model/user/users.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class UserService {

  /* DATABASE */
  Future<Database> createUserTable() async {
    try{
      return openDatabase(
        join(await getDatabasesPath(), 'bridge.db'),
        onCreate: (db, version) async {
          await db.execute(
            'CREATE TABLE user_login (username TEXT PRIMARY KEY, phone TEXT)',
          );
        },
        version: 1,
        singleInstance: true,
      );
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<void> saveLocalUser({ required Users user }) async {
    try {
      final Database db = await openDatabase(join(await getDatabasesPath(), 'bridge.db'));

      if (user != null && (user.username.isNotEmpty && user.phone.isNotEmpty)) {
        await db.insert('user_login', {
          'username': user.username,
          'phone': user.phone,
        },
          conflictAlgorithm: ConflictAlgorithm.replace,
        );
      }
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<Users?> getLocalUser() async {
    try {
      final Database db = await openDatabase(join(await getDatabasesPath(), 'bridge.db'));
      List<Map<String, dynamic>> maps = await db.rawQuery('SELECT username, phone FROM user_login');
      List<Users> users = [];

      if(maps != null && maps.isNotEmpty) {
        maps.forEach((map) => users.add(
            Users.onlyPhoneAndUsername(
              username: map['username'],
              phone: map['phone'],
            ),
          ),
        );

        if(users.isNotEmpty) {
          return users.elementAt(0);
        }
      }
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<Database> createTokenTable() async {
    try{
      return openDatabase(
        join(await getDatabasesPath(), 'bridge.db'),
        onCreate: (db, version) async {
          await db.execute(
            'CREATE TABLE token (token TEXT)',
          );
        },
        version: 1,
        singleInstance: true,
      );
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<void> saveToken({ required String token }) async {
    try {
      final Database db = await openDatabase(join(await getDatabasesPath(), 'bridge.db'));

      if (token != null && token.isNotEmpty) {
        await db.insert('token', {
          'token': token,
        },
          conflictAlgorithm: ConflictAlgorithm.replace,
        );
      }
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<String> getToken() async {
    try {
      final Database db = await openDatabase(join(await getDatabasesPath(), 'bridge.db'));
      List<Map<String, dynamic>> maps = await db.rawQuery('SELECT token FROM token');
      late String token;

      if(maps != null && maps.isNotEmpty) {
        maps.forEach((map) => {
          token = map['token'],
        });

        if(token.isNotEmpty) {
          return token;
        }
      }

      return '';
    } catch(e) {
      throw Exception(e);
    }
  }

}