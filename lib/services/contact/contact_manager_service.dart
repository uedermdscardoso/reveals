
import 'dart:typed_data';

import 'package:contacts_service/contacts_service.dart';

class ContactManagerService {

  Future<List<Contact>> findContacts({ required bool withThumbnails }) async {
    try {
      return (await ContactsService.getContacts(withThumbnails: withThumbnails)).toList();
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<void> save({ required Contact contact }) async {
    try {
      await ContactsService.addContact(contact);
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<void> update({ required Contact contact }) async {
    try{
      await ContactsService.updateContact(contact);
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<void> delete({ required Contact contact }) async {
    try{
      await ContactsService.deleteContact(contact);
    } catch(e) {
      throw Exception(e);
    }
  }
}