
import 'package:contacts_service/contacts_service.dart';
import 'package:bridge/services/contact/contact_manager_service.dart';
import 'package:mobx/mobx.dart';

part 'contact_manager_store.g.dart';

class ContactManagerStore = _ContactManagerStore with _$ContactManagerStore;

abstract class _ContactManagerStore with Store {

  @observable
  Future<List<Contact>>? contacts;

  @action
  addContacts({ required bool withThumbnails }) async {
    final contactService = ContactManagerService();

    this.contacts = contactService.findContacts(withThumbnails: withThumbnails);
  }

  @action
  addNewContact({ required Contact contact }) async {
    final contactService = ContactManagerService();

    await contactService.save(contact: contact);
  }

  @action
  clearContactList() => this.contacts = Future.value();

  @action
  updateContact({ required Contact contact }) async {
    final contactService = ContactManagerService();

    await contactService.update(contact: contact);
  }

  @action
  deleteContact({ required Contact contact }) async {
    final contactService = ContactManagerService();

    await contactService.delete(contact: contact);
  }

}