
import 'package:bridge/domain/model/user/users.dart';
import 'package:mobx/mobx.dart';

import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

part 'auth_store.g.dart';

class AuthStore = _AuthStore with _$AuthStore;

abstract class _AuthStore with Store {

  @observable
  Users currentUser = Users();


  @action
  addCurrentUser() async {
  }

}

