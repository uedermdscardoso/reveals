
import 'package:bridge/services/media/caption/custom_caption.dart';
import 'package:mobx/mobx.dart';

part 'watson_store.g.dart';

class WatsonStore = _WatsonStore with _$WatsonStore;

abstract class _WatsonStore with Store {

  @observable
  Future<List<CustomCaption>>? captions;

  @action
  addCaptions({ required List<CustomCaption> captions }) => this.captions = Future.value(captions);

  @action
  removeCaptions() => this.captions = Future.value(List.of({}));

}
