// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'file_uploader_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$FileUploaderStore on _FileUploaderStore, Store {
  late final _$foldersAtom =
      Atom(name: '_FileUploaderStore.folders', context: context);

  @override
  Future<List<AssetPathEntity>> get folders {
    _$foldersAtom.reportRead();
    return super.folders;
  }

  @override
  set folders(Future<List<AssetPathEntity>> value) {
    _$foldersAtom.reportWrite(value, super.folders, () {
      super.folders = value;
    });
  }

  late final _$currentPositionAtom =
      Atom(name: '_FileUploaderStore.currentPosition', context: context);

  @override
  Future<int> get currentPosition {
    _$currentPositionAtom.reportRead();
    return super.currentPosition;
  }

  @override
  set currentPosition(Future<int> value) {
    _$currentPositionAtom.reportWrite(value, super.currentPosition, () {
      super.currentPosition = value;
    });
  }

  late final _$imagesAtom =
      Atom(name: '_FileUploaderStore.images', context: context);

  @override
  BehaviorSubject<List<Uint8List>> get images {
    _$imagesAtom.reportRead();
    return super.images;
  }

  @override
  set images(BehaviorSubject<List<Uint8List>> value) {
    _$imagesAtom.reportWrite(value, super.images, () {
      super.images = value;
    });
  }

  late final _$submenusAtom =
      Atom(name: '_FileUploaderStore.submenus', context: context);

  @override
  Future<List<String>> get submenus {
    _$submenusAtom.reportRead();
    return super.submenus;
  }

  @override
  set submenus(Future<List<String>> value) {
    _$submenusAtom.reportWrite(value, super.submenus, () {
      super.submenus = value;
    });
  }

  late final _$addFoldersAsyncAction =
      AsyncAction('_FileUploaderStore.addFolders', context: context);

  @override
  Future addFolders() {
    return _$addFoldersAsyncAction.run(() => super.addFolders());
  }

  late final _$addFilesByPageAsyncAction =
      AsyncAction('_FileUploaderStore.addFilesByPage', context: context);

  @override
  Future addFilesByPage({required String selected, required int total}) {
    return _$addFilesByPageAsyncAction
        .run(() => super.addFilesByPage(selected: selected, total: total));
  }

  late final _$_FileUploaderStoreActionController =
      ActionController(name: '_FileUploaderStore', context: context);

  @override
  dynamic addCurrentPosition({required int currentPosition}) {
    final _$actionInfo = _$_FileUploaderStoreActionController.startAction(
        name: '_FileUploaderStore.addCurrentPosition');
    try {
      return super.addCurrentPosition(currentPosition: currentPosition);
    } finally {
      _$_FileUploaderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic resetImagesList() {
    final _$actionInfo = _$_FileUploaderStoreActionController.startAction(
        name: '_FileUploaderStore.resetImagesList');
    try {
      return super.resetImagesList();
    } finally {
      _$_FileUploaderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
folders: ${folders},
currentPosition: ${currentPosition},
images: ${images},
submenus: ${submenus}
    ''';
  }
}
