
import 'package:bridge/store/file_uploader/file_uploader_store.dart';
import 'package:bridge/store/media/media_store.dart';
import 'package:bridge/store/auth/auth_store.dart';
import 'package:bridge/store/chat/chat_store.dart';
import 'package:bridge/store/comment/comment_store.dart';
import 'package:bridge/store/connectivity/connectivity_store.dart';
import 'package:bridge/store/effect/effect_store.dart';
import 'package:bridge/store/hashtag/hashtag_store.dart';
import 'package:bridge/store/payment/payment_store.dart';
import 'package:bridge/store/notification/notification_store.dart';
import 'package:bridge/store/studio/studio_store.dart';
import 'package:bridge/store/user/user_store.dart';
import 'package:bridge/store/recommendation/recommendation_store.dart';
import 'package:camera/camera.dart';
import 'package:mobx/mobx.dart';
import 'package:bridge/store/media_upload/media_upload_store.dart';
import 'package:bridge/store/image_choosing/image_choosing_store.dart';
import 'package:bridge/store/contact/contact_manager_store.dart';
import 'package:bridge/store/watson/watson_store.dart';

part 'main_store.g.dart';

class MainStore = _MainStore with _$MainStore;

abstract class _MainStore with Store {

  @observable
  List<CameraDescription>? cameras;

  @observable
  bool isHome = true;

  final connectivity = ConnectivityStore();

  final auth = AuthStore();

  final chat = ChatStore();

  final comment = CommentStore();

  final contactManager = ContactManagerStore();

  final recommendation = RecommendationStore();

  final media = MediaStore();

  final studio = StudioStore();

  final mediaUpload = MediaUploadStore();

  final imageChossing = ImageChoosingStore();

  final effect = EffectStore();
  
  final hashtag = HashtagStore();

  final notification = NotificationStore();

  final user = UserStore();

  final watson = WatsonStore();

  final fileUploader = FileUploaderStore();

  @action
  changeIsHome({ required bool isHome }) {
    this.isHome = isHome;
  }

  @action
  addCameras({ required List<CameraDescription> cameras }) {
    this.cameras = cameras;
  }

}

//flutter packages pub run build_runner build --delete-conflicting-outputs
//flutter packages pub run build_runner clean

//https://balta.io/blog/flutter-mobx
