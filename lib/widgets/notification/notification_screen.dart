
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/domain/model/utilities/loading_icon_type.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/notification/notification_service.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/chat/conversation_group.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/widgets/notification/personal_notifications.dart';
import 'package:bridge/widgets/notification/comments_notifications.dart';
import 'package:bridge/widgets/notification/followers_notifications.dart';
import 'package:bridge/widgets/notification/likes_notifications.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/notification/notification_type.dart';
import 'package:bridge/shared/modal_dialog.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {

  final _authService = AuthenticationService();
  final _notificationService = NotificationService();
  final _userService = UserService();

  final PageController _pageController = PageController(initialPage: 0);
  final List<String> menus = ['all', 'followers', 'comments', 'likes'];

  int _menuSelected = NotificationType.ALL.index;
  late List<DocumentSnapshot> _notifications;

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    mainStore.notification.addNewNotificationStatus();

    mainStore.notification.addNotifications();
    mainStore.notification.addFollowersNotified();
    mainStore.notification.addNewPersonalNotifications();
    mainStore.notification.addCommentsNotified();
    mainStore.notification.addLikeNotified();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Column(
          children: [
            Header(
              left: Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Container(
                        child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                      ),
                    ),
                    const SizedBox(width: 6),
                    Text('${ _intl.translate('SCREEN.NOTIFICATION.TITLE') }',
                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                  ],
                ),
              ),
              color: Colors.transparent,
              hasBorder: false,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height: 38, //52
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 0.30, color: Colors.grey)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 25, left: 25),
                        child: NotificationListener<OverscrollIndicatorNotification>(
                          onNotification: (overscroll) {
                            overscroll.disallowIndicator();

                            return false;
                          },
                          child: ListView.builder(
                            itemCount: menus.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {

                              final String menu = menus.elementAt(index);

                              return GestureDetector(
                                onTap: () {
                                  setState(() => _menuSelected = index);

                                  _pageController.jumpToPage(_menuSelected);
                                },
                                child: Container(
                                  padding: EdgeInsets.only(left: 30, right: 30, top: 8, bottom: 8),
                                  decoration: index == _menuSelected ? BoxDecoration(
                                    border: Border(bottom: BorderSide(width: 2.75, color: Colors.black)),
                                  ) : null,
                                  child: Text('${ _translate(item: menu) }',
                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: index == _menuSelected ? Colors.grey[800] : Colors.grey[400])),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15, left: 12, right: 12),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: GestureDetector(
                            onTap: () {

                              showDialog<bool>(
                                  context: context,
                                  barrierDismissible: true,
                                  barrierColor: Colors.transparent,
                                  builder: (BuildContext context) {

                                    final String title = _getClearModalMessage(menu: _menuSelected);

                                    return ModalDialog(title: title);
                                  }
                              ).then((isNotClear) {
                                if(!(isNotClear ?? false))
                                  _clear(context: context);
                              });

                            },
                            child: Container(
                              padding: const EdgeInsets.only(top: 8, bottom: 8, left: 18, right: 18),
                              decoration: BoxDecoration(
                                color: Colors.redAccent,
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child: Text('${ _intl.translate('SCREEN.NOTIFICATION.BUTTON.CLEAR.TEXT').toUpperCase() }',
                                style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.white,
                                    decoration: TextDecoration.none,
                                    fontWeight: FontWeight.normal,
                                    fontFamily: 'Arial'
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8, right: 12, left: 12),
                        child: PageView(
                          key: Key('notification_page'),
                          physics: NeverScrollableScrollPhysics(),
                          controller: _pageController,
                          allowImplicitScrolling: false,
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            PersonalNotifications(),
                            FollowersNotifications(),
                            CommentsNotifications(),
                            LikesNotifications(),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _clear({ required BuildContext context }) async {
    if(await _authService.isConnected()){
      final Users currentUser = await _authService.getCurrentUser();

      UtilService.onLoading(
        context: context,
        type: LoadingIconType.CIRCLE,
        isDark: true,
        message: '${ _intl.translate('COMPONENT.LOADING.DEFAULT.MESSAGE') }',
      );

      if(this._menuSelected == NotificationType.ALL.index) {
        await _notificationService.deleteAll(currentUser: currentUser);
      } else if(this._menuSelected == NotificationType.FOLLOWERS.index) {
        await _notificationService.deleteAllFollowersNotified(currentUser: currentUser);
      } else if(this._menuSelected == NotificationType.COMMENTS.index) {
        await _notificationService.deleteAllCommentsNotified(currentUser: currentUser);
      } else if(this._menuSelected == NotificationType.LIKES.index) {
        await _notificationService.deleteAllLikesNotified(currentUser: currentUser);
      }

      Navigator.pop(context);
    } else
      UtilService.notify(context: context, message: '${ _intl.translate('CONNECTION.INTERNET.FAILED') }');
  }

  _translate({ required String item }) {
    switch(item) {
      case 'all':
        return _intl.translate('SCREEN.NOTIFICATION.MENU.ALL');
        break;
      case 'followers':
        return _intl.translate('SCREEN.NOTIFICATION.MENU.FOLLOWERS');
        break;
      case 'comments':
        return _intl.translate('SCREEN.NOTIFICATION.MENU.COMMENTS');
        break;
      case 'likes':
        return _intl.translate('SCREEN.NOTIFICATION.MENU.LIKES');
        break;
    };
  }

  _getClearModalMessage({ required int menu }) {
    if(menu == NotificationType.ALL.index)
      return _intl.translate('SCREEN.NOTIFICATION.MODAL_DIALOG.CLEAR.MESSAGES.ALL');
    else if(menu == NotificationType.FOLLOWERS.index)
      return _intl.translate('SCREEN.NOTIFICATION.MODAL_DIALOG.CLEAR.MESSAGES.FOLLOWERS');
    else if(menu == NotificationType.COMMENTS.index)
      return _intl.translate('SCREEN.NOTIFICATION.MODAL_DIALOG.CLEAR.MESSAGES.COMMENTS');
    else
      return _intl.translate('SCREEN.NOTIFICATION.MODAL_DIALOG.CLEAR.MESSAGES.LIKES');
  }

}
