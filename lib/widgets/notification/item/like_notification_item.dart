
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/notification/notification_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:bridge/widgets/profile/profile_details.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/widgets/media/media_group.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/main.dart';

class LikeNotificationItem extends StatelessWidget {

  final Users firedBy;
  final String type;
  final String? message;
  final DocumentSnapshot notification;

  final MediaService _mediaService = MediaService();
  final NotificationService _notificationService = NotificationService();
  final List<String> _items = ['remove'];

  late Translate _intl;

  LikeNotificationItem({Key? key,  required this.type, this.message, required this.notification, required this.firedBy }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return GestureDetector(
      onTap: () => _openAudio(context: context),
      child: Container(
        padding: EdgeInsets.only(top: 30, bottom: 30),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border(bottom: BorderSide(color: Colors.grey, width: 0.25)),
        ),
        child: Row(
          children: [
            GestureDetector(
              onTap: () => Navigator.of(context).push(UtilService.createRoute(ProfileDetails(isCurrentUser: false, displayUser: firedBy))),
              child: ProfilePhoto(user: firedBy, width: 45, height: 45),
            ),
            const SizedBox(width: 15),
            Expanded(
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () => Navigator.of(context).push(UtilService.createRoute(ProfileDetails(isCurrentUser: false, displayUser: firedBy))),
                    child: Row(
                      children: [
                        Text('${ firedBy != null ? firedBy.firstName : '' } ',
                          style: TextStyle(fontSize: 16, color: Colors.grey[700], decoration: TextDecoration.none, fontWeight: FontWeight.bold),
                        ),
                        Text('${ firedBy != null ? firedBy.lastName : '' } ',
                          style: TextStyle(fontSize: 16, color: Colors.grey[700], decoration: TextDecoration.none, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 5),
                  Text(firedBy != null ? '@${ firedBy.username }' : '@unknown',
                    style: TextStyle(fontSize: 14, color: Colors.grey, fontWeight: FontWeight.bold, decoration: TextDecoration.none, fontFamily: 'Arial'),),
                  const SizedBox(height: 5),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Visibility(
                      visible: notification.get('type').toString().compareTo('like_media') == 0,
                      child: Text('${ _intl.translate('SCREEN.NOTIFICATION.INDICATOR.LIKE.MEDIA') }',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 12, color: Colors.grey, decoration: TextDecoration.none, fontWeight: FontWeight.normal, fontFamily: 'Arial'),
                      ),
                      replacement: Text('${ _intl.translate('SCREEN.NOTIFICATION.INDICATOR.LIKE.COMMENT') }',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 12, color: Colors.grey, decoration: TextDecoration.none, fontWeight: FontWeight.normal, fontFamily: 'Arial'),
                      ),
                    ),
                  ),
                  const SizedBox(height: 5),
                  Visibility(
                    visible: notification.get('type').toString().compareTo('like_comment') == 0,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 12),
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Text(notification.get('type').toString().compareTo('like_comment') == 0 ?
                            notification.get('message') : '',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 14, color: Colors.grey, decoration: TextDecoration.none, fontWeight: FontWeight.normal, fontFamily: 'Arial'),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Image.asset('assets/images/thinker-dark.jpg', scale: 8, fit: BoxFit.cover),
                    ),
                    Image.asset("assets/icons/arrow.png", scale: 2.5, color: Colors.white70),
                  ],
                ),
                PopupMenuButton<String>(
                  elevation: 5,
                  onSelected: (String value) => {},
                  itemBuilder: (BuildContext context) {
                    return _items.map((String item) {

                      String translated = _translate(item: item);

                      return PopupMenuItem<String>(
                        value: item,
                        child: GestureDetector(
                          onTap: () async => await _makeAction(item: item),
                          child: Container(
                            child: Text(translated),
                          ),
                        ),
                      );
                    }).toList();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _translate({ required String item }) {
    switch(item) {
      case 'remove':
        return _intl.translate('SCREEN.NOTIFICATION.MENU.REMOVE');
        break;
    };
  }

  _makeAction({ required String item }) async {
    switch(item) {
      case 'remove':
        await _notificationService.delete(id: this.notification.id);
        break;
    };
  }

  _openAudio({ required BuildContext context }) async {
    final String mediaId = notification.get('mediaId');

    if(mediaId != null) {

      final Media media = await _mediaService.getMediaById(id: mediaId);

      if(media != null) {
        final List<Media> medias = [ media ];
        final String type = notification.get('type');

        _openChat(type: type);

        Navigator.of(context).push(UtilService.createRoute(
          MediaGroup(
            type: MediaScreenType.NOTIFICATION,
            keyName: 'like_notification',
            medias: medias.toSet(),
            isHome: true,
            isRecommend: true,
          ),
        ));
      }
    }
  }

  void _openChat({ required String type }) {
    if(type.compareTo('like_comment') == 0)
      mainStore.media.openChatWindow();
    else
      mainStore.media.closeChatWindow();
  }
}
