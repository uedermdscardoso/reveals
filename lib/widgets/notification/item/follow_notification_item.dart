
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/notification/notification_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:bridge/widgets/profile/profile_details.dart';

class FollowNotificationItem extends StatelessWidget {

  final Users firedBy;
  final DocumentSnapshot notification;

  final _notificationService = NotificationService();
  final List<String> _items = ['remove'];

  late Translate _intl;

  FollowNotificationItem({ Key? key, required this.firedBy, required this.notification }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return GestureDetector(
      onTap: () => Navigator.of(context).push(UtilService.createRoute(ProfileDetails(isCurrentUser: false, displayUser: this.firedBy))),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border(bottom: BorderSide(color: Colors.grey, width: 0.25)),
        ),
        child: Padding(
          padding: EdgeInsets.only(top: 30, bottom: 30),
          child: Row(
            children: [
              GestureDetector(
                onTap: () => Navigator.of(context).push(UtilService.createRoute(ProfileDetails(isCurrentUser: false, displayUser: firedBy))),
                child: ProfilePhoto(user: firedBy, width: 45, height: 45),
              ),
              const SizedBox(width: 15),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        Text('${ firedBy != null ? firedBy.firstName : '' } ',
                          style: TextStyle(fontSize: 16, color: Colors.grey[700], decoration: TextDecoration.none, fontWeight: FontWeight.bold),
                        ),
                        Text('${ firedBy != null ? firedBy.lastName : '' } ',
                          style: TextStyle(fontSize: 16, color: Colors.grey[700], decoration: TextDecoration.none, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(width: 5),
                        Flexible(child: Text('${ _intl.translate('SCREEN.NOTIFICATION.INDICATOR.FOLLOW') }',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 12, color: Colors.grey, decoration: TextDecoration.none, fontWeight: FontWeight.normal, fontFamily: 'Arial'))),
                      ],
                    ),
                    const SizedBox(height: 5),
                    Row(
                      children: [
                        Text(firedBy != null ? '@${ firedBy.username }' : '@unknown',
                          style: TextStyle(fontSize: 14, color: Colors.grey, decoration: TextDecoration.none, fontWeight: FontWeight.bold, fontFamily: 'Arial')),
                      ],
                    ),
                  ],
                ),
              ),
              PopupMenuButton<String>(
                //icon: const Icon(Icons.more_vert),
                elevation: 5,
                onSelected: (String value) => {},
                itemBuilder: (BuildContext context) {
                  return _items.map((String item) {

                    String translated = _translate(item: item);

                    return PopupMenuItem<String>(
                      value: item,
                      child: GestureDetector(
                        onTap: () async => await _makeAction(item: item),
                        child: Container(
                          child: Text(translated),
                        ),
                      ),
                    );
                  }).toList();
                },
              ),
              /*DropdownButton<String>(
                icon: const Icon(Icons.more_vert),
                iconSize: 25,
                elevation: 0,
                style: const TextStyle(color: Colors.grey),
                onChanged: (_) {},
                underline: Container(),
                items: _items.map<DropdownMenuItem<String>>((String item) {

                  String translated = _translate(item: item);

                  return DropdownMenuItem<String>(
                    value: item,
                    child: GestureDetector(
                      onTap: () async => await _makeAction(item: item),
                      child: Container(
                        child: Text(translated),
                      ),
                    ),
                  );
                }).toList(),
              ),*/
            ],
          ),
        ),
      ),
    );
  }

  _translate({ required String item }) {
    switch(item) {
      case 'remove':
        return _intl.translate('SCREEN.NOTIFICATION.MENU.REMOVE');
        break;
    };
  }

  _makeAction({ required String item }) async {
    switch(item) {
      case 'remove':
        await _notificationService.delete(id: notification.id);
        break;
    };
  }
}
