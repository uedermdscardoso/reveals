
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:bridge/widgets/notification/item/like_notification_item.dart';
import 'package:bridge/services/notification/notification_service.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/main.dart';

class LikesNotifications extends StatelessWidget {

  final ScrollController _scrollController = ScrollController();
  final NotificationService _notificationService = NotificationService();
  final UserService _userService = UserService();

  late Translate _intl;

  LikesNotifications({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Observer(
      builder: (_) {

        return FutureBuilder(
          future: mainStore.notification.likeNotified,
          builder: (context, snapshot) {

            if(snapshot.hasData) {

              final Query query = snapshot.data as Query;

              return Container();

              /*return RealtimePagination(
                itemsPerPage: 10,
                initialLoading: Loading(),
                emptyDisplay: NoResultFound(icon: Icons.notifications),
                bottomLoader: Loading(),
                itemBuilder: (int index, BuildContext context, DocumentSnapshot document) {

                  return FutureBuilder(
                    future: _userService.getSimpleUserById(user: Users.onlyId(id: document.get('firedBy') ?? '')),
                    builder: (context, snapshot) {

                      if(snapshot.hasData) {

                        final Users user = snapshot.data as Users;

                        return Container(
                          color: Colors.transparent,
                          child: LikeNotificationItem(
                            notification: document,
                            firedBy: user,
                            type: 'media',
                          ),
                        );
                      }

                      return Container();
                    },
                  );
                },
                query: query,
                scrollController: _scrollController,
                customPaginatedBuilder: (itemCount, controller, itemBuilder) {

                  return NotificationListener<OverscrollIndicatorNotification>(
                    onNotification: (overscroll) {
                      overscroll.disallowIndicator();

                      return false;
                    },
                    child: ListView.builder(
                      padding: EdgeInsets.only(right: 4, left: 4),
                      itemCount: itemCount,
                      scrollDirection: Axis.vertical,
                      controller: controller,
                      itemBuilder: itemBuilder,
                    ),
                  );
                },
              );*/
            }

            return Container();
          },
        );

        /*return StreamBuilder(
          stream: mainStore.notification.likeNotified,
          builder: (context, snapshot) {

            if(snapshot.hasData) {

              _likesNotified = snapshot.data;

              if(_likesNotified.docs.isNotEmpty) {

                return NotificationListener<OverscrollIndicatorNotification>(
                  onNotification: (overscroll) {
                    overscroll.disallowGlow();
                  },
                  child: ListView.builder(
                    padding: EdgeInsets.only(right: 4, left: 4),
                    itemCount: _likesNotified.docs.length ?? 0,
                    itemBuilder: (context, index) {

                      final DocumentSnapshot notification = _likesNotified.docs.elementAt(index);

                      return FutureBuilder(
                        future: _userService.getSimpleUserById(user: Users(id: notification.get('firedBy'))),
                        builder: (context, snapshot) {

                          if(snapshot.hasData) {

                            final Users user = snapshot.data;

                            return Container(
                              child: LikeNotificationItem(
                                text: '${ _intl.translate(
                                    'SCREEN.NOTIFICATION.INDICATOR.LIKE.MEDIA') }',
                                notification: notification,
                                firedBy: user,
                                type: 'media',
                              ),
                            );
                          }

                          return Container();
                        },
                      );
                    },
                  ),
                );
              }
            }

            return NoResultFound(icon: Icons.notifications);
          },
        );*/
      },
    );
  }
}
