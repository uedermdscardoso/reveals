
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/domain/model/utilities/file_uploader_type.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/file_uploader/file_uploader.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:photo_manager/photo_manager.dart';

class PhotoEdition extends StatelessWidget {

  final AuthenticationService _authService = AuthenticationService();
  final Users displayUser;

  PhotoEdition({Key? key, required this.displayUser }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.light,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Container(
        width: 90,
        height: 90,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(100))
        ),
        child: Stack(
          alignment: Alignment.bottomRight,
          children: [
            ProfilePhoto(
              width: 90,
              height: 90,
              user: this.displayUser,
            ),
            Visibility(
              visible: _authService.checkUserAndCurrentUser(user: this.displayUser),
              child: GestureDetector(
                onTap: () async => await _openFileUploader(context: context),
                child: Container(
                    width: 22,
                    height: 22,
                    decoration: BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(100)),
                    child: Icon(Icons.edit, color: Colors.white, size: 12)
                ),
              ),
            ),
          ],
        ),
      ),
    );

  }

  _openFileUploader({ required BuildContext context }) async {
    final result = await PhotoManager.requestPermissionExtend();

    if(result.hasAccess) {
      mainStore.imageChossing.addFileUploaderType(type: FileUploaderType.PROFILE);

      Navigator.push(context, UtilService.createRoute(FileUploader()));
    }
  }
}
