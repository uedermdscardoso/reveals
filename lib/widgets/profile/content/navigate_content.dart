
import 'package:bridge/domain/model/media/media_type.dart';
import 'package:flutter/material.dart';

class NavigateContent extends StatefulWidget {

  final PageController controller;
  
  const NavigateContent({Key? key, required this.controller }) : super(key: key);
  
  @override
  _NavigateContentState createState() => _NavigateContentState();
}

class _NavigateContentState extends State<NavigateContent> {

  MediaType _defaultMediaType = MediaType.SHORT_FORM_AUDIO;

  @override
  void initState() {
    super.initState();
  }
  
  @override
  void dispose() {
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Row( //Menus
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              onTap: () => setState(() {
                widget.controller.jumpToPage(1);
                _defaultMediaType = MediaType.AUDIOBOOK;
              }),
              child: Container(
                  padding: EdgeInsets.only(top: 20, bottom: 6),
                  width: (MediaQuery.of(context).size.width-16) / 3,
                  decoration: BoxDecoration(
                    border: _defaultMediaType == MediaType.AUDIOBOOK ?
                      Border(top: BorderSide(color: Colors.grey, width: 1.8)) :
                      Border(top: BorderSide(color: Colors.grey, width: 0.25)),
                  ),
                  child: Image.asset("assets/icons/audiobook.png", width: 25, height: 25, color: Colors.grey[800])
              ),
            ),
            GestureDetector(
              onTap: () => setState(() {
                widget.controller.jumpToPage(0);
                _defaultMediaType = MediaType.SHORT_FORM_AUDIO;
              }),
              child: Container(
                  padding: EdgeInsets.only(top: 20, bottom: 6),
                  width: (MediaQuery.of(context).size.width-16) / 3,
                  decoration: BoxDecoration(
                    border: _defaultMediaType == MediaType.SHORT_FORM_AUDIO ?
                    Border(top: BorderSide(color: Colors.grey, width: 1.8)) :
                    Border(top: BorderSide(color: Colors.grey, width: 0.25)),
                  ),
                  child: Image.asset("assets/icons/audio.png", width: 25, height: 25, color: Colors.grey)
              ),
            ),
            GestureDetector(
              onTap: () => setState(() {
                widget.controller.jumpToPage(2);
                _defaultMediaType = MediaType.PODCAST;
              }),
              child: Container(
                padding: EdgeInsets.only(top: 20, bottom: 6),
                width: (MediaQuery.of(context).size.width-16) / 3,
                decoration: BoxDecoration(
                  border: _defaultMediaType == MediaType.PODCAST ?
                    Border(top: BorderSide(color: Colors.grey, width: 1.8)) :
                    Border(top: BorderSide(color: Colors.grey, width: 0.25)),
                ),
                child: Image.asset("assets/icons/podcast.png", width: 25, height: 25, color: Colors.grey[800])
              ),
            )
          ],
        ),
      ),
    );
  }
}
