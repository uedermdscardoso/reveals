
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:bridge/services/media/media_service.dart';

class MediaGroupMini extends StatefulWidget {

  final Query query;
  final String keyName;
  final num currentPosition;
  final MediaScreenType type;
  final bool isHome;

  const MediaGroupMini({
    required this.query,
    this.type : MediaScreenType.DEFAULT,
    this.isHome : false,
    required this.keyName,
    this.currentPosition : 0,
  });

  @override
  State<StatefulWidget> createState() => _MediaGroupMiniState();
}

class _MediaGroupMiniState extends State<MediaGroupMini> {

  final ScrollController _scrollController = ScrollController();
  final MediaService _mediaService = MediaService();

  double _totalHeight = 0;
  double _avgFirstHeight = 0;
  int _index = 0;
  int _currentIndex = 0;
  bool _isCurrent = false;
  bool _isUserPause = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    //_scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
        systemNavigationBarDividerColor: Colors.black,
      ),
      child: Material(
        color: Colors.transparent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Container(), /*RealtimePagination(
                itemsPerPage: 10, //10
                initialLoading: Container(),
                emptyDisplay: NoResultFound(
                  image: Image.asset('assets/icons/music_dark.png',
                    color: Colors.grey[400],
                    scale: 1.6,
                  ),
                ),
                bottomLoader: Loading(type: LoadingIconType.THREE_BALLS, message: ""),
                itemBuilder: (int index, BuildContext context, DocumentSnapshot document) {

                  if(_index == index) {
                    _currentIndex = _index; //Recebe o índice atual
                    _isCurrent = true;
                  } else {
                    _isCurrent = false;
                  }

                  return FutureBuilder(
                    future: _mediaService.build(document: document),
                    builder: (context, snapshot) {

                      if(snapshot.hasData) {

                        final Media media = snapshot.data as Media;

                        if(media != null) {

                          return MediaScreen(
                            type: widget.type,
                            keyName: widget.keyName,
                            isCurrent: _isCurrent,
                            isDemo: false,
                            mediaType: MediaType.AUDIO,
                            //isUserPause: _isUserPause,
                            isHome: widget.isHome,
                            position: index,
                            media: media,
                          );
                        }
                      }

                      return Container();
                    },
                  );

                },
                query: widget.query,
                scrollController: _scrollController,
                customPaginatedBuilder: (itemCount, controller, itemBuilder) {

                  if(itemCount > 0) {
                    _totalHeight = MediaQuery.of(context).size.height * itemCount;
                    _avgFirstHeight = _totalHeight / itemCount;
                    _avgFirstHeight = _avgFirstHeight.isNaN ? 0 : _avgFirstHeight;
                    controller = ScrollController(initialScrollOffset: (_avgFirstHeight * widget.currentPosition).toDouble());
                  }

                  return NotificationListener<OverscrollIndicatorNotification>(
                    onNotification: (overscroll) {
                      overscroll.disallowIndicator();

                      return false;
                    },
                    child: NotificationListener(
                      onNotification: (ScrollNotification notification) => _controlMediaList(notification: notification),
                      child: ListView.builder(
                        padding: EdgeInsets.only(top: 0),
                        key: Key('${ widget.keyName }'),
                        physics: const PageScrollPhysics(),
                        itemExtent: _avgFirstHeight,
                        cacheExtent: _avgFirstHeight,
                        itemCount: itemCount,
                        controller: controller,
                        itemBuilder: itemBuilder,
                      ),
                    ),
                  );
                },
              ),*/
            ),
          ],
        ),
      ),
    );
  }

  _controlMediaList({ required ScrollNotification notification }) {
    /*if(notification is ScrollStartNotification){
      setState(() {
        _isUserPause = true;
      });
    }*/
    if (notification is ScrollEndNotification && notification.metrics.axis == Axis.vertical) {
      double currentPosition = !notification.metrics.pixels.isInfinite ? notification.metrics.pixels : 0;

      setState(() {
        _index = (currentPosition /_avgFirstHeight).round();
      });

      //Remover o pause adicionado pelo usuário
      if(currentPosition.round() == (_avgFirstHeight * (_currentIndex + 1) - _avgFirstHeight).round()){ //false para index atual
        setState(() => _isUserPause = false);
      } else { //para index diferente do index atual
        if (currentPosition > (_avgFirstHeight * (_index + 1)) ||
            currentPosition < (_avgFirstHeight * (_index + 1))) {
          setState(() => _isUserPause = true);
        } else {
          setState(() => _isUserPause = false);
        }
      }
    }
    return false;
  }

}
