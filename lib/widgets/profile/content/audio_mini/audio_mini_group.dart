
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:bridge/Translate.dart';

class AudioMiniGroup extends StatelessWidget {

  final _mediaService = MediaService();

  final Users? displayUser;
  final Color? color;
  final MediaScreenType type;
  final Query? query;
  final List<Media>? medias;

  late Translate _intl;

  AudioMiniGroup({Key? key,
    required this.query,
    this.displayUser,
    this.color,
    this.type : MediaScreenType.DEFAULT,
    this.medias,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Container();

    /*return RealtimePagination(
      itemsPerPage: 10,
      initialLoading: Loading(),
      emptyDisplay: NoResultFound(
        image: Image.asset('assets/icons/music_dark.png',
          color: Colors.grey[400],
          scale: 1.6,
        ),
      ),
      bottomLoader: Loading(),
      itemBuilder: (int index, BuildContext context, DocumentSnapshot document) {

        return FutureBuilder(
          future: _mediaService.build(document: document),
          builder: (context, snapshot) {

            if(snapshot.hasData) {

              final Media audio = snapshot.data as Media;
              final totalView = UtilService.formatNumberToHumanReadable(number: audio.totalView, languageCode: _intl.locale.languageCode);

              return GestureDetector(
                onTap: () => Navigator.of(context).push(UtilService.createRoute(
                    MediaGroupMini(
                      keyName: 'MiniMyAudios',
                      type: type,
                      query: query!,
                      currentPosition: index,
                      isHome: false,
                    ),
                  ),
                ),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(18),
                        child: CachedNetworkImage(
                          imageUrl: audio.image,
                          width: 180, //175
                          height: 180, //175
                          fit: BoxFit.cover,
                          placeholder: (context, url) => Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover),
                          errorWidget: (context, url, error) => Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(12),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Icon(Icons.remove_red_eye, color: Colors.white, size: 18),
                                  const SizedBox(width: 6),
                                  Text('${ totalView }', style: TextStyle(color: Colors.white, fontSize: 18)),
                                ],
                              )
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }

            return Container();
            //return NoResultFound(icon: Icons.data_usage);
          },
        );

      },
      query: query!,
      customPaginatedBuilder: (itemCount, controller, itemBuilder) {

        return GridView.builder(
          primary: false,
          padding: const EdgeInsets.all(8), //2
          controller: controller,
          itemCount: itemCount,
          itemBuilder: itemBuilder,
          scrollDirection: Axis.vertical,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisSpacing: 6,
            crossAxisSpacing: 6,
            childAspectRatio: 0.80, //0.90
          ),
        );
      },
    );*/

    /*return NotificationListener<OverscrollIndicatorNotification>(
      onNotification: (overscroll) { overscroll.disallowGlow(); },
      child: GridView.builder(
        itemCount: medias != null ? medias.length : 0,
        padding: EdgeInsets.all(2),
        scrollDirection: Axis.vertical,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 2,
          crossAxisSpacing: 2,
          childAspectRatio: 0.80, //0.90
        ),
        itemBuilder: (context, position) {

          Media audio = medias.elementAt(position);

        },
      ),
    );*/
  }
}
