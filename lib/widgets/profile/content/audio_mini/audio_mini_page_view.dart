
import 'package:bridge/domain/model/media/media_type.dart';
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/widgets/profile/content/audio_mini/audio_mini_group.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:bridge/Translate.dart';

class AudioMiniGroupScreen extends StatefulWidget {

  final PageController genericController;
  final Users displayUser;

  const AudioMiniGroupScreen({Key? key, required this.genericController, required this.displayUser }) : super(key: key);

  @override
  _AudioMiniGroupScreenState createState() => _AudioMiniGroupScreenState();
}

class _AudioMiniGroupScreenState extends State<AudioMiniGroupScreen> {

  final PageController _shortController = PageController();
  final _mediaService = MediaService();

  bool _isLikes = true;
  List<Media> _audios = [];

  late Translate _intl;

  @override
  void initState() {
    initValues();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  initValues() {
    mainStore.media.addMyMedias(user: widget.displayUser); //My audios
    mainStore.media.addLikedMedias(user: widget.displayUser); //My saved medias
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Material(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: PageView(
                physics: NeverScrollableScrollPhysics(),
                controller: widget.genericController,
                scrollDirection: Axis.vertical,
                children: [
                  getAudioType(type: MediaType.SHORT_FORM_AUDIO),
                  getAudioType(type: MediaType.AUDIOBOOK),
                  getAudioType(type: MediaType.PODCAST),
                ],
              ),
            ),
          ],
        ),
    );
  }

  Widget getAudioType({ required MediaType type }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12, left: 8, right: 8),
      child: Stack(
        fit: StackFit.expand,
        children: [
          Stack(
            children: [
              Visibility(
                visible: type.index == MediaType.SHORT_FORM_AUDIO.index,
                child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _shortController,
                  scrollDirection: Axis.vertical,
                  children: [
                    getMedias(type: 'my'),
                    getMedias(type: 'liked'),
                  ],
                ),
                replacement: Container(),
              ),
              //Audiobook,
              //Podcast,
            ],
          ),
          Visibility(
            visible: type.index == MediaType.SHORT_FORM_AUDIO.index,
            child: Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 4, right: 18),
                child: GestureDetector(
                  onTap: () {
                    print(_isLikes);
                    if(_isLikes)  //likes
                      _shortController.jumpToPage(1);
                    else //audios
                      _shortController.jumpToPage(0);

                    setState(() => _isLikes = !_isLikes);
                  },
                  child: Stack(
                    children: [
                      Container(
                        padding: EdgeInsets.all(17),
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                          color: _isLikes ? Colors.black : Colors.grey,
                          borderRadius: BorderRadius.circular(99),
                        ),
                        child: Image.asset('assets/icons/stars.png', scale: 0.25, color: Colors.white),
                      ),
                      Visibility(
                        visible: !_isLikes,
                        child: Positioned(
                          top: 32,
                          left: 1,
                          child: RotationTransition(
                            turns: new AlwaysStoppedAnimation(50 / 360),
                            child: Container(
                              width: 68,
                              height: 2,
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.circular(12),
                              ),
                            ),
                          )
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getMedias({ required String type }){

    return Visibility(
      visible: type.compareTo('my') == 0,
      child: Observer(
        builder: (_) {

          return FutureBuilder(
            future: mainStore.media.myMedias,
            builder: (context, snapshot) {

              if(snapshot.hasData) {

                final Query query = snapshot.data as Query;

                if(query != null) {

                  return Padding(
                    padding: const EdgeInsets.only(top: 12),
                    child: AudioMiniGroup(
                      displayUser: widget.displayUser,
                      type: MediaScreenType.MINI,
                      color: Colors.purple,
                      medias: _audios,
                      query: query,
                    ),
                  );
                }
              }

              return NoResultFound(
                image: Image.asset('assets/icons/music_dark.png',
                  color: Colors.grey[400],
                  scale: 1.6,
                ),
              );
            },
          );
        },
      ),
      replacement: Observer(
        builder: (_) {

          return FutureBuilder(
            future: mainStore.media.likedMedias,
            builder: (context, snapshot) {

              if(snapshot.hasData) {

                final Query query = snapshot.data as Query;

                if(query != null) {

                  return Padding(
                    padding: const EdgeInsets.only(top: 12),
                    child: AudioMiniGroup(
                      displayUser: widget.displayUser,
                      type: MediaScreenType.MINI,
                      color: Colors.purple,
                      medias: _audios,
                      query: query,
                    ),
                  );
                }
              }

              return NoResultFound(
                image: Image.asset('assets/icons/music_dark.png',
                  color: Colors.grey[400],
                  scale: 1.6,
                ),
              );
            },
          );
        },
      ),
    );
  }

}
