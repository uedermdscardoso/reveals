
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/profile/content/photo_edition.dart';
import 'package:bridge/domain/model/utilities/social_network_type.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/main.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/Translate.dart';

class PersonDetails extends StatefulWidget {

  final Users displayUser;

  const PersonDetails({Key? key,  required this.displayUser }) : super(key: key);

  @override
  _PersonDetailsState createState() => _PersonDetailsState();
}

class _PersonDetailsState extends State<PersonDetails> {

  final AuthenticationService _authService = AuthenticationService();

  bool _isDoingLive = false;
  int _totalLikes = 0;
  int _totalFollowers = 0;
  int _totalFollowing = 0;

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    mainStore.user.addIsFollowing(followed: widget.displayUser);
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 12),
              child: Align(
                child: Observer(
                  builder: (_) {

                    return Visibility(
                      visible: _authService.isAnonymous() || !_authService.checkUserAndCurrentUser(user: widget.displayUser),
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: StreamBuilder(
                          stream: mainStore.user.isFollowing,
                          builder: (context, snapshot) {

                            if(snapshot.hasData) {

                              final QuerySnapshot snap = snapshot.data as QuerySnapshot;
                              final bool isFollowing = snap.docs.isNotEmpty;

                              return Visibility(
                                visible: !isFollowing,
                                child: GestureDetector(
                                  onTap: () async => await _follow(context: context),
                                  child: Container(
                                    padding: EdgeInsets.only(left: 30, right: 30, top: 8, bottom: 8),
                                    decoration: BoxDecoration(
                                      color: Colors.black,
                                      border: Border.all(color: Colors.black, width: 0.5),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Text('${ _intl.translate('SCREEN.PROFILE.BUTTON.FOLLOW') }',
                                      style: TextStyle(color: Colors.white, fontSize: 12),
                                    ),
                                  ),
                                ),
                                replacement: GestureDetector(
                                  onTap: () async => await _disfollow(context: context),
                                  child: Container(
                                    padding: EdgeInsets.only(left: 30, right: 30, top: 8, bottom: 8),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(color: Colors.black, width: 0.5),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Text('${ _intl.translate('SCREEN.PROFILE.BUTTON.FOLLOWING') }',
                                        style: TextStyle(color: Colors.black, fontSize: 12)),
                                  ),
                                ),
                              );
                            }

                            return Container();
                          },
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 12, left: 12, right: 12),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 12, right: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          PhotoEdition(displayUser: widget.displayUser),
                          const SizedBox(width: 16),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Visibility(
                                  visible: (widget.displayUser.firstName != null || widget.displayUser.lastName != null) || (widget.displayUser.firstName.isNotEmpty || widget.displayUser.lastName.isNotEmpty),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text('${ widget.displayUser != null ?  widget.displayUser.firstName : '' } ${ widget.displayUser != null ? widget.displayUser.lastName : '' }',
                                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                                  ),
                                ),
                                const SizedBox(height: 8),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text('@${ widget.displayUser != null ? widget.displayUser.username : '' }', style: TextStyle(color: Colors.grey[600], fontSize: 14)),
                                ),
                                const SizedBox(height: 8),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    GestureDetector(
                                      onTap: () => _showSocialNetwork(
                                        context: context,
                                        currentUser: widget.displayUser,
                                        type: SocialNetworkType.TWITTER,
                                      ),
                                      child: Container(
                                        padding: EdgeInsets.all(6),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(8),
                                          border: Border.all(width: 0.5, color: Colors.black),
                                        ),
                                        child: Image.asset('assets/icons/social_network/dark/bird_twitter.png', scale: 2.25),
                                      ),
                                    ),
                                    const SizedBox(width: 8),
                                    GestureDetector(
                                      onTap: () => _showSocialNetwork(
                                        context: context,
                                        currentUser: widget.displayUser,
                                        type: SocialNetworkType.INSTAGRAM,
                                      ),
                                      child: Container(
                                        padding: EdgeInsets.all(6),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(8),
                                          border: Border.all(width: 0.5, color: Colors.black),
                                        ),
                                        child: Image.asset('assets/icons/social_network/dark/instagram.png', scale: 2.25),
                                      ),
                                    ),
                                    const SizedBox(width: 8),
                                    GestureDetector(
                                      onTap: () => _showSocialNetwork(
                                        context: context,
                                        currentUser: widget.displayUser,
                                        type: SocialNetworkType.TIKTOK,
                                      ),
                                      child: Container(
                                        padding: EdgeInsets.all(6),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(6),
                                          border: Border.all(width: 0.5, color: Colors.black),
                                        ),
                                        child: Image.asset('assets/icons/social_network/dark/tiktok.png', scale: 2.75),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: widget.displayUser != null && widget.displayUser.description != null && widget.displayUser.description.isNotEmpty,
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Padding( //196 characters
                          padding: const EdgeInsets.only(top: 30, left: 15, right: 25),
                          child: Text('${ widget.displayUser != null ?
                            widget.displayUser.description : '${ _intl.translate('SCREEN.PROFILE.DESCRIPTION') }' }',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(color: Colors.grey[500], fontSize: 15)),
                        ),
                      ),
                    ),
                    const SizedBox(height: 30),
                    Container(
                      padding: const EdgeInsets.only(bottom: 20, left: 15),
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(color: Colors.grey[600]!, width: 0.35),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          FutureBuilder(
                            future: mainStore.user.getTotalLikes(user: widget.displayUser),
                            builder: (context, snapshot) {

                              if(snapshot.hasData)
                                _totalLikes = snapshot.data as int;

                              return Column(
                                children: [
                                  Text('${ UtilService.formatNumberToHumanReadable(
                                        languageCode: 'en',
                                        number: _totalLikes,
                                      )}',
                                      style: TextStyle(color: Colors.black, decoration: TextDecoration.none, fontSize: 16, fontWeight: FontWeight.bold, fontFamily: 'Arial')),
                                  const SizedBox(height: 2),
                                  Text('${ _intl.translate('SCREEN.PROFILE.INDICATOR.LIKE') }',
                                    style: TextStyle(color: Colors.black, decoration: TextDecoration.none, fontSize: 12, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                ],
                              );
                            },
                          ),
                          const SizedBox(width: 35),
                          FutureBuilder(
                            future: mainStore.user.getTotalFollowers(user: widget.displayUser),
                            builder: (context, snapshot) {

                              if(snapshot.hasData)
                                _totalFollowers = snapshot.data as int;

                              return Column(
                                children: [
                                  Text('${ UtilService.formatNumberToHumanReadable(
                                    languageCode: 'en',
                                    number: _totalFollowers,
                                  ) }', style: TextStyle(color: Colors.black, decoration: TextDecoration.none, fontSize: 16, fontWeight: FontWeight.bold, fontFamily: 'Arial')),
                                  const SizedBox(height: 2),
                                  Text('${ _intl.translate('SCREEN.PROFILE.INDICATOR.FOLLOWERS') }',
                                      style: TextStyle(color: Colors.black, decoration: TextDecoration.none, fontSize: 12, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                ],
                              );
                            },
                          ),
                          const SizedBox(width: 35),
                          FutureBuilder(
                            future: mainStore.user.getTotalFollowing(user: widget.displayUser),
                            builder: (context, snapshot) {

                              if(snapshot.hasData)
                                _totalFollowing = snapshot.data as int;

                              return Column(
                                children: [
                                  Text('${ UtilService.formatNumberToHumanReadable(
                                    languageCode: 'en',
                                    number: _totalFollowing,
                                  ) }',
                                      style: TextStyle(color: Colors.black, decoration: TextDecoration.none, fontSize: 16, fontWeight: FontWeight.bold, fontFamily: 'Arial')),
                                  const SizedBox(height: 2),
                                  Text('${ _intl.translate('SCREEN.PROFILE.INDICATOR.FOLLOWING') }',
                                      style: TextStyle(color: Colors.black, decoration: TextDecoration.none, fontSize: 12, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                ],
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _showSocialNetwork({ required BuildContext context, required Users currentUser, required SocialNetworkType type }) {

    bool isValidated = _isValidated(context: context, currentUser: currentUser, type: type);

    if(isValidated){

      switch(type) {
        case SocialNetworkType.TWITTER:
          _showDialog(
            context: context,
            username: currentUser.twitter,
            icon: Image.asset('assets/icons/social_network/dark/bird_twitter.png', color: Colors.grey, scale: 2.25),
          );
          break;
        case SocialNetworkType.INSTAGRAM:
          _showDialog(
            context: context,
            username: currentUser.instagram,
            icon: Image.asset('assets/icons/social_network/dark/instagram.png', color: Colors.grey, scale: 2.25),
          );
          break;
        case SocialNetworkType.TIKTOK:
          _showDialog(
            context: context,
            username: currentUser.tiktok,
            icon: Image.asset('assets/icons/social_network/dark/tiktok.png', color: Colors.grey, scale: 2.25),
          );
          break;
      }
    } else {
        switch(type) {
          case SocialNetworkType.TWITTER: {
            UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.PROFILE.NOTIFICATION.SOCIAL_NETWOrk.NO_TWITTER') }');
            return false;
          } break;
          case SocialNetworkType.INSTAGRAM: {
            UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.PROFILE.NOTIFICATION.SOCIAL_NETWOrk.NO_INSTAGRAM') }');
            return false;
          } break;
          case SocialNetworkType.TIKTOK: {
            UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.PROFILE.NOTIFICATION.SOCIAL_NETWOrk.NO_TIKTOK') }');
            return false;
          } break;
          default:
            return false;
            break;
        }
    }
  }

  _showDialog({ required BuildContext context, required String username, required Image icon }) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          elevation: 0,
          backgroundColor: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(18),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                icon, //Icon(Icons.person, color: Colors.grey, size: 20),
                const SizedBox(width: 6),
                Text('${ username }', style: TextStyle(fontSize: 14, color: Colors.grey)),
              ],
            ),
          ),
        );
      },
    );
  }

  _follow({ required BuildContext context }) async {
    bool isAnonymous = await _authService.isAnonymous();
    if(!isAnonymous)
      await mainStore.user.follow(author: widget.displayUser);
    else
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.PROFILE.NOTIFICATION.REGISTER_NOW').toString() }');
  }

  _disfollow({ required BuildContext context }) async {
    if(!_authService.isAnonymous())
      await mainStore.user.disfollow(author: widget.displayUser);
    else
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.PROFILE.NOTIFICATION.REGISTER_NOW').toString() }');
  }

  bool _isValidated({ required BuildContext context, required Users currentUser, required SocialNetworkType type }) {

    if(currentUser != null) {

      switch(type) {
        case SocialNetworkType.TWITTER:
          return currentUser != null && currentUser.twitter != null && currentUser.twitter.isNotEmpty;
        case SocialNetworkType.INSTAGRAM:
          return currentUser != null && currentUser.instagram != null && currentUser.instagram.isNotEmpty;
        case SocialNetworkType.TIKTOK:
          return currentUser != null && currentUser.tiktok != null && currentUser.tiktok.isNotEmpty;
        default:
          return false;
      }
    }

    return false;
  }
}
