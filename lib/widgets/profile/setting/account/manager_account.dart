
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/services/utilities/email_service.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/profile/setting/account/description_edit.dart';
import 'package:bridge/widgets/profile/setting/account/social_network_edit.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:bridge/main.dart';

class ManagerAccount extends StatefulWidget {
  @override
  _ManagerAccountState createState() => _ManagerAccountState();
}

class _ManagerAccountState extends State<ManagerAccount> {

  final UserService _userService = UserService();

  Users _currentUser = Users();

  late Translate _intl;

  @override
  void initState() {
    mainStore.user.addCurrentUser();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Column(
          children: [
            Header(
              color: Colors.transparent,
              left: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                    ),
                  ),
                  const SizedBox(width: 4),
                  Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.TITLE') }', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                ],
              ),
              hasBorder: true,
            ),
            Observer(
              builder: (_) {

                return StreamBuilder(
                  stream: mainStore.user.currentUser,
                  builder: (context, snapshot) {

                    if(snapshot.hasData) {
                      final QuerySnapshot snap = snapshot.data as QuerySnapshot;
                      _currentUser = _userService.makeUser(doc: snap.docs.first);
                    }

                    return Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 25, right: 18, left: 18, bottom: 12),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.DETAILS.TITLE') }', style: TextStyle(color: Colors.grey, fontSize: 12)),
                            ),
                            const SizedBox(height: 12),
                            GestureDetector(
                              onTap: () => UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.NOTIFICATION.UPDATE_NOT_ALLOWED.PERSON_NAME') }'),
                              child: Container(
                                color: Colors.white,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.DETAILS.PERSON_NAME') }',
                                        style: TextStyle(color: Colors.grey, fontSize: 16)),
                                    Row(
                                      children: [
                                        Text("${ _currentUser.firstName } ", style: TextStyle(color: Colors.grey, fontSize: 14)),
                                        Text("${ _currentUser.lastName }", style: TextStyle(color: Colors.grey, fontSize: 14)),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(height: 48),
                            GestureDetector(
                              onTap: () => Navigator.of(context).push(UtilService.createRoute(DescriptionEdit())),
                              child: Container(
                                color: Colors.white,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.DETAILS.DESCRIPTION') }',
                                      style: TextStyle(color: Colors.grey, fontSize: 16)),
                                    Container(
                                      width: MediaQuery.of(context).size.width * 0.50,
                                      child: Text('${ _currentUser != null && _currentUser.description != null && _currentUser.description.isNotEmpty ? _currentUser.description : _intl.translate('SCREEN.PROFILE.DESCRIPTION') }',
                                        maxLines: 8,
                                        textAlign: TextAlign.right,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(height: 1.5, color: Colors.grey, fontSize: 14)),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(height: 48),
                            GestureDetector(
                              onTap: () => UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.NOTIFICATION.UPDATE_NOT_ALLOWED.USERNAME') }'),
                              child: Container(
                                color: Colors.white,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.DETAILS.USERNAME') }',
                                      style: TextStyle(color: Colors.grey, fontSize: 16)),
                                    Text('@${ _currentUser.username }',
                                      style: TextStyle(color: Colors.grey, fontSize: 14)),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(height: 48),
                            GestureDetector(
                              onTap: () => UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.NOTIFICATION.UPDATE_NOT_ALLOWED.CELL_PHONE_NUMBER') }'),
                              child: Container(
                                color: Colors.white,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.DETAILS.CELL_PHONE') }',
                                      style: TextStyle(color: Colors.grey, fontSize: 16)),
                                    Text('${ EmailService.obscureText(limit: 8, text: _currentUser.phone) }', //********
                                      style: TextStyle(color: Colors.grey, fontSize: 14)),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(height: 48),
                            GestureDetector(
                              onTap: () => Navigator.of(context).push(UtilService.createRoute(SocialNetworkEdit())),
                              child: Container(
                                color: Colors.white,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.DETAILS.SOCIAL_NETWORK.TITLE') }',
                                          style: TextStyle(color: Colors.grey, fontSize: 16)),
                                      ],
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width * 0.35,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                              Image.asset('assets/icons/social_network/dark/bird_twitter.png', color: Colors.grey, scale: 2.25),
                                              const SizedBox(width: 8),
                                              Visibility(
                                                visible: _currentUser == null || _currentUser.twitter == null || _currentUser.twitter.isEmpty,
                                                child: Flexible(
                                                  child: Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.DETAILS.SOCIAL_NETWORK.NO_DATA.TWITTER') }',
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(color: Colors.grey[500], decoration: TextDecoration.none, fontSize: 14, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                                ),
                                                replacement: Flexible(
                                                  child: Text('${ _currentUser.twitter }',
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 14, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                                ),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(height: 18),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                              Image.asset('assets/icons/social_network/dark/instagram.png', color: Colors.grey, scale: 2.25),
                                              const SizedBox(width: 8),
                                              Visibility(
                                                visible: _currentUser == null || _currentUser.instagram == null || _currentUser.instagram.isEmpty,
                                                child: Flexible(
                                                  child: Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.DETAILS.SOCIAL_NETWORK.NO_DATA.INSTAGRAM') }',
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(color: Colors.grey[500], decoration: TextDecoration.none, fontSize: 14, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                                ),
                                                replacement: Flexible(
                                                  child: Text('${ _currentUser.instagram }',
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 14, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                                ),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(height: 18),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                              Image.asset('assets/icons/social_network/dark/tiktok.png', color: Colors.grey, scale: 2.25),
                                              const SizedBox(width: 8),
                                              Visibility(
                                                visible: _currentUser == null || _currentUser.tiktok == null || _currentUser.tiktok.isEmpty,
                                                child: Flexible(
                                                  child: Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.DETAILS.SOCIAL_NETWORK.NO_DATA.TIKTOK') }',
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(color: Colors.grey[500], decoration: TextDecoration.none, fontSize: 14, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                                ),
                                                replacement: Flexible(
                                                  child: Text('${ _currentUser.tiktok }',
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 14, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              }
            ),
          ],
        ),
      ),
    );
  }

}
