
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/AppLanguage.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/utilities/language.dart';
import 'package:bridge/widgets/header.dart';
import 'package:provider/provider.dart';

class LanguageEdit extends StatefulWidget {

  final AppLanguage appLanguage;

  const LanguageEdit({Key? key,  required this.appLanguage }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LanguageEditState();
}

class _LanguageEditState extends State<LanguageEdit> {

  final List<Language> _languages = [
    Language(text: 'English', abbreviation: 'en', isChecked: false),
    Language(text: 'Español', abbreviation: 'es', isChecked: false),
    Language(text: 'Português', abbreviation: 'pt', isChecked: false),
  ];

  late AppLanguage _appLanguage;
  late String _selectedLanguage;
  late Translate _intl;

  @override
  void initState() {
    //_changeLanguagesList();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /*_changeLanguagesList() {
    _languages.asMap().forEach((index, language) {
      language.isChecked = language.abbreviation.compareTo(widget.appLanguage.appLocal.languageCode) == 0;
      switch(language.abbreviation){
        case 'en':
          return "English";
        case 'es':
          return "Español";
        case 'pt':
          return "Português";
      }
    });
  }*/

  @override
  Widget build(BuildContext context) {

    _appLanguage = Provider.of<AppLanguage>(context);
    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Column(
          children: [
            Header(
              color: Colors.transparent,
              left: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                    ),
                  ),
                  const SizedBox(width: 4),
                  Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_LANGUAGE.TITLE') }', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                ],
              ),
              right: GestureDetector(
                onTap: () {
                  List<Language> chosen = _languages.where((language) => language.isChecked == true).toList();
                  _appLanguage.changeLanguage(Locale(chosen.elementAt(0).abbreviation));
                  Navigator.pop(context);
                },
                child: Padding(
                  padding: const EdgeInsets.only(top: 5, bottom: 5, right: 8, left: 8),
                  child: Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_LANGUAGE.BUTTON.SAVE') }',
                    style: TextStyle(color: Colors.black, decoration: TextDecoration.none, fontSize: 16, fontWeight: FontWeight.normal, fontFamily: 'Arial')
                  ),
                ),
              ),
              hasBorder: true,
            ),
            Expanded(
              child:  Padding(
                padding: const EdgeInsets.all(12),
                child: NotificationListener<OverscrollIndicatorNotification>(
                  onNotification: (overscroll) {
                    overscroll.disallowIndicator();

                    return false;
                  },
                  child: ListView.builder(
                    itemCount: _languages.length,
                    itemBuilder: (context, position) {
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            _languages.asMap().forEach((index, language) {
                              if(index == position)
                                language.isChecked = true;
                              else
                                language.isChecked = false;
                            });
                          });
                        },
                        child: Container(
                          color: Colors.transparent,
                          child: Padding(
                            padding: const EdgeInsets.all(15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('${ _languages.elementAt(position).text }',
                                  style: TextStyle(color: Colors.grey)
                                ),
                                Container(
                                  width: 22,
                                  height: 22,
                                  decoration: BoxDecoration(
                                    color: !_languages.elementAt(position).isChecked ? Colors.grey[400] : Colors.black,
                                    borderRadius: BorderRadius.circular(100),
                                  ),
                                  child: Icon(Icons.check, color: Colors.white, size: 15),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

}
