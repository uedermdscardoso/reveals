
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/AppLanguage.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/translation/translation_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/auth/anonymous_profile_screen.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/profile/setting/account/language_edit.dart';
import 'package:bridge/widgets/profile/setting/account/manager_account.dart';
import 'package:bridge/widgets/profile/setting/support/send_feedback.dart';
import 'package:provider/provider.dart';
import 'package:bridge/widgets/auth/document_viewer.dart';
import 'package:bridge/domain/model/utilities/report_type.dart';
import 'package:bridge/Translate.dart';

class SettingScreen extends StatefulWidget {

  final Users currentUser;

  const SettingScreen({Key? key, required this.currentUser }) : super(key: key);

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {

  final _authenticationService = AuthenticationService();

  late AppLanguage _appLanguage;
  late Locale _myLocale;

  bool _isSubcribed = false;
  bool _hasProposal = false;

  late Translate _intl;

  @override
  void initState() {
    mainStore.payment.isSubcribedToMuslyAds();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _appLanguage = Provider.of<AppLanguage>(context);

    _myLocale = Localizations.localeOf(context);
    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Column(
          children: [
            Header(
              color: Colors.transparent,
              left: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                    ),
                  ),
                  const SizedBox(width: 4),
                  Text('${ _intl.translate('SCREEN.SETTING.TITLE') }', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                ],
              ),
              hasBorder: true,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  width: double.infinity,
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(12),
                    child: Column(
                      children: [
                        showLabel(label: '${ _intl.translate('SCREEN.SETTING.MENU.ACCOUNT.LABEL') }'),
                        GestureDetector(
                          onTap: () => Navigator.of(context).push(UtilService.createRoute(ManagerAccount())),
                          child: Container(
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(18),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(Icons.person_outline, size: 22, color: Colors.grey),
                                  const SizedBox(width: 8),
                                  Text('${ _intl.translate('SCREEN.SETTING.MENU.ACCOUNT.BUTTON.MANAGE_ACCOUNT') }', style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 15, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                ],
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () => Navigator.of(context).push(UtilService.createRoute(LanguageEdit(appLanguage: _appLanguage))),
                          child: Container(
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(18),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Icon(Icons.language, size: 19, color: Colors.grey),
                                      const SizedBox(width: 12),
                                      Text('${ _intl.translate('SCREEN.SETTING.MENU.ACCOUNT.BUTTON.LANGUAGE') }', style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 15, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                    ],
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(5),
                                      border: Border.all(color: Colors.grey, width: 0.5),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 12, right: 12, top: 4, bottom: 4),
                                      child: Text(
                                          '${ TranslationService.getLanguage(language: _myLocale.languageCode) }',
                                          style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 15, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        showLabel(label: '${_intl.translate('SCREEN.SETTING.MENU.SUPPORT.LABEL') }'),
                        GestureDetector(
                          onTap: () => Navigator.of(context).push(UtilService.createRoute(SendFeedback(currentUser: widget.currentUser))),
                          child: Container(
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(18),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(Icons.edit, size: 19, color: Colors.grey),
                                  const SizedBox(width: 12),
                                  Text('${ _intl.translate('SCREEN.SETTING.MENU.SUPPORT.BUTTON.SEND_FEEDBACK') }', style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 15, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                ],
                              ),
                            ),
                          ),
                        ),
                        showLabel(label: '${_intl.translate('SCREEN.SETTING.MENU.ABOUT.LABEL') }'),
                        GestureDetector(
                          onTap: () => Navigator.of(context).push(UtilService.createRoute(
                            DocumentViewer(type: ReportType.TERMS_OF_USE),
                          )),
                          child: Container(
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(18),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(Icons.book, size: 19, color: Colors.grey),
                                  const SizedBox(width: 12),
                                  Text('${ _intl.translate('SCREEN.SETTING.MENU.ABOUT.BUTTON.TERMS_OF_USE') }', style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 15, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                ],
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () => Navigator.of(context).push(UtilService.createRoute(
                            DocumentViewer(type: ReportType.PRIVACY_POLICY),
                          )),
                          child: Container(
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(18),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(Icons.privacy_tip, size: 19, color: Colors.grey),
                                  const SizedBox(width: 12),
                                  Text('${ _intl.translate('SCREEN.SETTING.MENU.ABOUT.BUTTON.PRIVACY_POLICY') }', style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 15, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                ],
                              ),
                            ),
                          ),
                        ),
                        addSpacing(height: 0.25),
                        GestureDetector(
                          onTap: () => doLogout(context: context),
                          child: Container(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 18, right: 18, left: 18, top: 35),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Image.asset('assets/icons/logout.png', scale: 1.20, color: Colors.grey),
                                  const SizedBox(width: 12),
                                  Text('${ _intl.translate('SCREEN.SETTING.MENU.SIGN_OUT') }', style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 17, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
          ),
    );
  }


  void doLogout({ required BuildContext context }) async {
    await _authenticationService.doLogout();

    Navigator.of(context).pushAndRemoveUntil(UtilService.createRoute(AnonymousProfileScreen()), (Route<dynamic> route) => false);
  }

  Widget showLabel({ required String label }){
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 18, right: 18, left: 18),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Align(
                alignment: Alignment.centerLeft,
                child: Text(label, style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 15, fontWeight: FontWeight.normal, fontFamily: 'Arial'))
            ),
            const SizedBox(height: 12),
            addSpacing(height: 0.25),
          ],
        ),
      ),
    );
  }

  Widget addSpacing({ required double height }){
    return Row(
      children: [
        Expanded(child: Container(color: Colors.grey, height: height)),
      ],
    );
  }
}
