
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/profile/content/audio_mini/audio_mini_page_view.dart';
import 'package:bridge/widgets/profile/content/navigate_content.dart';
import 'package:bridge/widgets/profile/content/person_details.dart';
import 'package:bridge/widgets/profile/setting/setting_screen.dart';
import 'package:bridge/widgets/profile/invite_friend.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:bridge/main.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/shared/loading.dart';

class ProfileDetails extends StatefulWidget {

  final bool isCurrentUser;
  final Users? displayUser;

  const ProfileDetails({ required this.isCurrentUser, this.displayUser });

  @override
  _ProfileDetailsState createState() => _ProfileDetailsState();
}

class _ProfileDetailsState extends State<ProfileDetails> with AutomaticKeepAliveClientMixin {

  final PageController _genericController = PageController();
  final AuthenticationService _authService = AuthenticationService();
  final UserService _userService = UserService();

  late Users _displayUser;
  double _expandedHeight = 290;
  String _title = '';

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }
  _init() async {
    mainStore.user.addCurrentUser();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    _title = _intl.translate('SCREEN.PROFILE.TITLE.${ widget.displayUser != null ? 'FROM_OTHER_USER' : 'FROM_CURRENT_USER' }');

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Observer(
            builder: (_) {

              return StreamBuilder(
                stream: mainStore.user.currentUser,
                builder: (context, snapshot) {

                  if(snapshot.hasData) {

                    final QuerySnapshot snap = snapshot.data as QuerySnapshot;

                    if(snap.docs.isNotEmpty && widget.isCurrentUser != null) {
                      final Users currentUser = _userService.makeUser(doc: snap.docs.elementAt(0));

                      if(widget.isCurrentUser)
                        _displayUser = currentUser;
                      else
                        _displayUser = widget.displayUser ?? Users();

                      if(_displayUser.description != null && _displayUser.description.isNotEmpty)
                        _expandedHeight = 290;
                      else
                        _expandedHeight = 240;

                      return Column(
                        children: [
                          Header(
                            color: Colors.transparent,
                            left: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                GestureDetector(
                                  onTap: () => Navigator.pop(context),
                                  child: Container(
                                    child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                                  ),
                                ),
                                Text('${ _title }', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                              ],
                            ),
                            right: Visibility( //Only current User
                              visible: _authService.checkUserAndCurrentUser(user: _displayUser),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  GestureDetector(
                                    onTap: () async {
                                      final PermissionStatus permissionStatus = await Permission.contacts.request();

                                      Navigator.of(context).push(UtilService.createRoute(
                                        InviteFriend(permissionStatus: permissionStatus),
                                      ));
                                    },
                                    child: Container(
                                      color: Colors.transparent,
                                      child: Icon(Icons.group_add, color: Colors.black, size: 25),
                                    ),
                                  ),
                                  const SizedBox(width: 10),
                                  GestureDetector(
                                    onTap: () => Navigator.of(context).push(UtilService.createRoute(SettingScreen(currentUser: _displayUser))),
                                    child: Container(
                                      color: Colors.transparent,
                                      child: Icon(Icons.more_vert, size: 25, color: Colors.black),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            hasBorder: true,
                          ),
                          Expanded(
                            child: NotificationListener<OverscrollIndicatorNotification>(
                              onNotification: (overscroll) {
                                overscroll.disallowIndicator();

                                return false;
                              },
                              child: NestedScrollView(
                                headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                                  return <Widget>[
                                    SliverAppBar(
                                      toolbarHeight: 0,
                                      expandedHeight: _expandedHeight, //90
                                      floating: false,
                                      pinned: false,
                                      backgroundColor: Colors.white,
                                      bottom: PreferredSize(
                                        preferredSize: Size.fromHeight(_expandedHeight), //90
                                        child: Container(),
                                      ),
                                      flexibleSpace: PersonDetails(displayUser: _displayUser),
                                    ),
                                    SliverPersistentHeader(
                                      pinned: true,
                                      delegate: _SliverProfileScreenDelegate(
                                        Container(
                                          padding: EdgeInsets.only(top: 8, bottom: 8),
                                          height: 30,
                                          color: Colors.white,
                                          child: Icon(Icons.arrow_drop_down, color: Colors.grey[500], size: 30),
                                        ),
                                      ),
                                    ),
                                  ];
                                },
                                body: AudioMiniGroupScreen(genericController: _genericController, displayUser: _displayUser),
                              ),
                            ),
                          ),
                          NavigateContent(controller: _genericController),
                        ],
                      );
                    }

                  }

                  return Loading();
                },
              );
            }
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}


class _SliverProfileScreenDelegate extends SliverPersistentHeaderDelegate {

  final Widget _widget;

  _SliverProfileScreenDelegate(this._widget);

  @override
  double get minExtent => 30; //83;

  @override
  double get maxExtent => 30; //83

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return _widget;
  }

  @override
  bool shouldRebuild(_SliverProfileScreenDelegate oldDelegate) {
    return false;
  }
}