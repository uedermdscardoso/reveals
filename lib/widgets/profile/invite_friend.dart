
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/main.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:flutter/services.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/shared/loading.dart';

class InviteFriend extends StatefulWidget {

  final PermissionStatus permissionStatus;

  const InviteFriend({Key? key,  required this.permissionStatus }) : super(key: key);

  @override
  _InviteFriendState createState() => _InviteFriendState();
}

class _InviteFriendState extends State<InviteFriend> {

  final AuthenticationService _authService = AuthenticationService();
  final UserService _userService = UserService();
  final TextEditingController _searchController = TextEditingController();

  List<Contact> _contactSearch = [];
  String _hasInvites = '';
  int _invites = 0;
  int _pendents = 0;

  late DocumentSnapshot _document;
  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    mainStore.contactManager.clearContactList();
    super.dispose();
  }

  _init() {
    mainStore.user.addInvites();
    mainStore.user.addPendentInvites();
    if(widget.permissionStatus != null && widget.permissionStatus.isGranted)
      mainStore.contactManager.addContacts(withThumbnails: true);
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.grey[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(
              color: Colors.transparent,
              left: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                    ),
                  ),
                  Text('${ _intl.translate('SCREEN.PROFILE.INVITE_FRIEND.TITLE') }', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                ],
              ),
              right: Observer(
                builder: (_) {

                  return StreamBuilder(
                    stream: mainStore.user.pendentInvites,
                    builder: (context, snapshot) {

                      if(snapshot.hasData) {
                        final QuerySnapshot snap = snapshot.data as QuerySnapshot;
                        _pendents = snap.docs.length;
                      }

                      return Padding(
                        padding: EdgeInsets.only(right: 4),
                        child: Text('${ _intl.translate('SCREEN.PROFILE.INVITE_FRIEND.INDICATORS.PENDING') } (${ _pendents })', style: TextStyle(color: _pendents > 0 ? Colors.grey[600] : Colors.grey, fontSize: 14)),
                      );
                    },
                  );
                },
              ),
              hasBorder: true,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    FutureBuilder(
                      future: mainStore.contactManager.contacts,
                      builder: (context, snapshot) {

                        List<Contact> _contacts = List<Contact>.empty();

                        if(snapshot.hasData)
                          _contacts = snapshot.data as List<Contact>;

                        return CustomField(
                          controller: _searchController,
                          autofocus: false,
                          prefixIcon: Icon(Icons.search, color: Colors.grey, size: 25),
                          text: '${ _intl.translate('SCREEN.PROFILE.BUTTON.SEARCH') }',
                          maxLines: 1,
                          backgroundColor: Colors.grey[200],
                          textColor: Colors.grey,
                          borderColor: Colors.grey[200],
                          onChanged: (value) {
                            _contactSearch.clear();

                            if (_contacts.isNotEmpty) {
                              _contacts.forEach((elem) {
                                if (elem.displayName != null &&
                                    elem.displayName!.toLowerCase().startsWith(value.toString().toLowerCase()))
                                  setState(() => _contactSearch.add(elem));
                              });
                            }
                          },
                        );
                      },
                    ),
                    Expanded(
                      child: FutureBuilder(
                        future: mainStore.contactManager.contacts,
                        builder: (context, snapshot) {

                          if(snapshot.hasData) {

                            final List<Contact> contacts = snapshot.data as List<Contact>;

                            if(contacts.isNotEmpty) {

                              return NotificationListener<OverscrollIndicatorNotification>(
                                onNotification: (overscroll) {
                                  overscroll.disallowIndicator();

                                  return false;
                                },
                                child: ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  itemCount: _contactSearch.isEmpty ? contacts.length : _contactSearch.length,
                                  itemBuilder: (context, index) {

                                    final Contact contact = _contactSearch.isEmpty ? contacts.elementAt(index) : _contactSearch.elementAt(index);
                                    final List<Item> phones = contact.phones!.toList();
                                    final MemoryImage image = MemoryImage(contact.avatar!);

                                    return Visibility(
                                      visible: phones != null && phones.isNotEmpty,
                                      child: Padding(
                                        padding: const EdgeInsets.only(bottom: 16),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              width: 50,
                                              height: 50,
                                              decoration: BoxDecoration(
                                                color: Colors.grey[400],
                                                borderRadius: BorderRadius.circular(100),
                                              ),
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(100),
                                                child: Visibility(
                                                  visible: contact.avatar != null && contact.avatar!.isNotEmpty,
                                                  child: Image.memory(image.bytes, scale: 1.0, fit: BoxFit.cover),
                                                  replacement: Image.asset('assets/icons/profile.png', scale: 14),
                                                ),
                                              ),
                                            ),
                                            const SizedBox(width: 12),
                                            Expanded(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  Align(
                                                    alignment: Alignment.centerLeft,
                                                    child: Text(contact.displayName ?? '', style: TextStyle(color: Colors.black)),
                                                  ),
                                                  const SizedBox(height: 6),
                                                  Align(
                                                    alignment: Alignment.centerLeft,
                                                    child: Text( phones != null && phones.isNotEmpty ? phones.elementAt(0).value ?? '' : 'No phone',
                                                      style: TextStyle(color: Colors.black)),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () async {

                                                /*showDialog(
                                                  context: context,
                                                  barrierDismissible: true,
                                                  barrierColor: Colors.white,
                                                  builder: (BuildContext context) {
                                                    final String message = '${ _intl.translate('SCREEN.PROFILE.INVITE_FRIEND.NOTIFICATION.SENDING_SMS') }';

                                                    return Loading(message: message);
                                                  },
                                                );*/

                                                await _invite(context: context, contact: contact);

                                                //Navigator.pop(context);
                                              },
                                              child: Container(
                                                padding: EdgeInsets.only(left: 20, right: 20, top: 4, bottom: 4),
                                                decoration: BoxDecoration(
                                                  color: Colors.black,
                                                  borderRadius: BorderRadius.circular(8),
                                                ),
                                                child: Text('${ _intl.translate('SCREEN.PROFILE.INVITE_FRIEND.BUTTON.INVITE') }', style: TextStyle(fontSize: 15, color: Colors.white)),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              );
                            }
                          }

                          return NoResultFound(
                            icon: Icons.data_usage,
                            color: Colors.grey,
                          );
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 30),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Observer(
                            builder: (_) {

                              return StreamBuilder(
                                stream: mainStore.user.invites,
                                builder: (context, snapshot) {
                                  if(snapshot.hasData) {
                                    _document = snapshot.data as DocumentSnapshot;
                                    _invites = _document.get('invites');
                                  }

                                  _hasInvites = '${ _intl.translate('SCREEN.PROFILE.INVITE_FRIEND.MESSAGES.HAS_INVITES') }';
                                  _hasInvites = _hasInvites.replaceAll('{INVITES}', '${ _invites }');

                                  return Text(_hasInvites, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.grey[600]));
                                },
                              );
                            }
                          ),
                          const SizedBox(height: 12),
                          Text('${ _intl.translate('SCREEN.PROFILE.INVITE_FRIEND.MESSAGES.ADVICE') }', textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 18, color: Colors.grey[600])),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _invite({ required BuildContext context, required Contact contact }) async {
    if(!_authService.isAnonymous()) {
      String phone = '';

      if (contact != null && contact.phones!.isNotEmpty) {
        final String friendName = contact.displayName ?? '${ _intl.translate('SCREEN.PROFILE.INVITATION.TIP.FRIEND') }';

        phone = contact.phones!.elementAt(0).value ?? '';

        phone = phone.replaceAll(RegExp('[ ()-]'), '');

        await _sendSms(friendName: friendName, phone: phone); //send sms
        await _inviteUser(context: context, phone: phone); //save temp user

      }
    }
  }

  //https://www.youtube.com/watch?v=YgLntVW3dsI

  _sendSms({ required String friendName, required String phone }) async {
    final String link = 'https://www.musly.com.br/app';
    String message = '${ _intl.translate('SCREEN.PROFILE.INVITATION.MESSAGE') }';
    message = message.replaceAll('{FRIEND}', friendName);
    message = message.replaceAll('{PHONE}', phone);
    message = message.replaceAll('{LINK}', link);

    await sendSMS(message: '${ message }', recipients: [ phone ])
        .catchError((e) => print(e));

  }

  _inviteUser({ required BuildContext context, required String phone }) async {

    final Users guest = Users.onlyPhone(phone: phone);
    final bool existsByPhone = await _userService.existsByPhone(user: guest);

    if (!existsByPhone) {
      await _userService.invite(guest: guest);
      await mainStore.user.decrementInvites();
    } else {
      final bool isWaiting = await _userService.isWaiting(user: Users.onlyPhone(phone: phone));

      if(isWaiting) {
        final bool hasInvite = await _userService.hasInvite(user: Users.onlyPhone(phone: phone));

        if(!hasInvite) {
          await _userService.addInviteToExistingUser(guest: guest);
          await mainStore.user.decrementInvites();
        } else
          UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.PROFILE.INVITATION.NOTIFICATION.HAS_INVITE') }');
      } else
        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.PROFILE.INVITATION.NOTIFICATION.USING_APP') }');
    }
  }
}
