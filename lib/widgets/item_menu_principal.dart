
import 'package:flutter/material.dart';

class ItemMenuPrincipal extends StatelessWidget {

  final IconData? icon;
  final Widget? customIcon;
  final String? text;
  final GestureTapCallback onTap;
  final bool isHome;

  const ItemMenuPrincipal({Key? key,
    this.icon,
    this.customIcon,
    this.isHome : true,
    this.text,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: this.onTap,
      child: Container(
        padding: EdgeInsets.all(10),
        height: 50, //75
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Visibility(
                visible: this.icon != null,
                child: Icon(this.icon ?? Icons.all_inclusive,
                  color: this.isHome ? Colors.white : Colors.black,
                  size: 28,
                ),
                replacement: this.customIcon ?? Icon(Icons.all_inclusive, color: this.isHome ? Colors.white : Colors.black),
              ),
            ),
            /*Align(
              alignment: Alignment.center,
              child: Text(text, textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 10, fontFamily: "Arial", fontWeight: FontWeight.normal, decoration: TextDecoration.none)),
            ),*/
          ],
        ),
      ),
    );

  }
}
