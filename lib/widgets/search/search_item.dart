
import 'dart:io';

import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/widgets/profile/content/audio_mini/media_group_mini.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:bridge/domain/model/hashtag/hashtag.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:bridge/main.dart';
import 'dart:ui';

class SearchItem extends StatefulWidget {

  final int position;
  final HashTag hashtag;
  final Media audio;

  const SearchItem({ required this.position, required this.hashtag, required this.audio });

  @override
  _SearchItemState createState() => _SearchItemState();
}

class _SearchItemState extends State<SearchItem> {

  Media _audio = Media();
  String _image = '';

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init(){
    _audio = widget.audio;
    _image = _audio.image;
  }

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.only(left: 8),
      child: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(230, 230, 230, 1),
          borderRadius: BorderRadius.circular(8)
        ),
        child: Stack(
          children: [
            FutureBuilder(
              future: Future.value(_image),
              builder: (context, snapshot) {

                if(snapshot.hasData) {

                  _image = snapshot.data as String;

                  return Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () async => await _showMediasComplete(context: context),
                          child: Stack(
                            fit: StackFit.expand,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(12),
                                child: CachedNetworkImage(
                                  imageUrl: _image,
                                  fit: BoxFit.cover,
                                  filterQuality: FilterQuality.low,
                                  placeholder: (context, url) => Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover),
                                  errorWidget: (context, url, error) => Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover),
                                ),
                              ),
                              ClipRRect(
                                borderRadius: BorderRadius.circular(12),
                                child: BackdropFilter(
                                  filter: ImageFilter.blur(sigmaX: 3.5, sigmaY: 3.5),
                                  child: Container(
                                    color: Colors.grey.withOpacity(0.1),
                                    alignment: Alignment.center,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                }

                return Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(230, 230, 230, 1),
                    borderRadius: BorderRadius.circular(8)
                  ),
               );
              },
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ProfilePhoto(user: _audio.createdBy!, width: 25, height: 25),
                  const SizedBox(height: 6),
                  Container(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Align(
                        alignment: Alignment.center,
                        child: Text('@${ _audio.createdBy != null ? _audio.createdBy!.username : 'unknown' }',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.white, fontSize: 10))
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _showMediasComplete({ required BuildContext context }) async {

    await mainStore.hashtag.addMedias(hashtag: widget.hashtag);

    Navigator.of(context).push(UtilService.createRoute(
      FutureBuilder(
        future: mainStore.hashtag.medias.containsKey(widget.hashtag.name) ?
            mainStore.hashtag.medias[widget.hashtag.name] : Future.value(),
        builder: (context, snapshot) {

          if(snapshot.hasData) {

            final Query query = snapshot.data as Query;

            if(query != null) {

              return MediaGroupMini(
                keyName: 'MiniAudio',
                type: MediaScreenType.SEARCH,
                query: query,
                //medias: widget.audios,
                currentPosition: widget.position,
                isHome: false,
              );
            }
          }

          return Container();
        },
      )),
    );
  }
}
