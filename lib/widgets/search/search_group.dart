
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:bridge/widgets/profile/content/audio_mini/media_group_mini.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/domain/model/hashtag/hashtag.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/search/search_item.dart';
import 'package:bridge/main.dart';
import 'package:bridge/Translate.dart';

class SearchGroup extends StatefulWidget {

  final HashTag hashtag;

  const SearchGroup({Key? key,  required this.hashtag }) : super(key: key);

  @override
  _SearchGroupState createState() => _SearchGroupState();
}

class _SearchGroupState extends State<SearchGroup> {

  final _mediaService = MediaService();

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    mainStore.hashtag.addLimitedMedias(hashtag: widget.hashtag);
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Observer(
      builder: (_) {

        //load only 5 items
        return StreamBuilder(
          stream: mainStore.hashtag.limitedMedias.containsKey(widget.hashtag.name) ? mainStore.hashtag.limitedMedias[widget.hashtag.name] : Stream.empty(),
          builder: (context, snapshot) {

            if(snapshot.hasData) {

              final QuerySnapshot snap = snapshot.data as QuerySnapshot;

              if(snap != null && snap.docs.isNotEmpty) {

                return FutureBuilder(
                  future: _mediaService.makeMediaList2(docs: snap.docs),
                  builder: (context, snapshot) {

                    if(snapshot.hasData) {

                      final List<Media> audios = snapshot.data as List<Media>;

                      print(audios.length);

                      if(audios != null && audios.isNotEmpty) {

                        return Padding(
                          padding: const EdgeInsets.only(top: 4, bottom: 35),
                          child: Column(
                            children: [
                              Container(
                                height: 110,
                                padding: EdgeInsets.all(0),
                                child: NotificationListener<OverscrollIndicatorNotification>(
                                  onNotification: (overscroll) {
                                    overscroll.disallowIndicator();

                                    return false;
                                  },
                                  child: ListView.builder(
                                    padding: EdgeInsets.all(2),
                                    itemCount: audios.length+1,
                                    cacheExtent: 1000,
                                    itemExtent: 120,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (contextMedia, index) {

                                      if(index == audios.length)
                                        return _showPlus();
                                      else
                                        return SearchItem(
                                          hashtag: widget.hashtag,
                                          audio: audios.elementAt(index),
                                          position: index,
                                        );
                                    },
                                  ),
                                ),
                              ),
                              const SizedBox(height: 12),
                              Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(color: Colors.grey, width: 0.5),
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                    child: Icon(Icons.star_half_sharp, size: 20, color: Colors.black),
                                  ),
                                  const SizedBox(width: 12),
                                  Expanded(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text('${ widget.hashtag.name ?? 'hashtag' }', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: [
                                                Text('${ UtilService.formatNumberToHumanReadable(number: widget.hashtag.totalView ?? 0, languageCode: 'en') }',
                                                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal)),
                                                const SizedBox(width: 6),
                                                Icon(Icons.visibility, color: Colors.black, size: 20),
                                              ],
                                            ),
                                          ],
                                        ),
                                        const SizedBox(height: 4),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text('${ _intl.translate('SCREEN.SEARCH.INDICATORS.TRENDING') }', style: TextStyle(color: Colors.grey, fontSize: 13, fontWeight: FontWeight.normal)),
                                            Row(
                                              children: [
                                                Text('${ UtilService.formatNumberToHumanReadable(number: widget.hashtag.totalMedias ?? 0, languageCode: 'en') } reveals',
                                                    style: TextStyle(color: Colors.grey, fontSize: 13, fontWeight: FontWeight.normal)),
                                               ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      }
                    }

                    return Container();
                  },
                );
              }
            }

            return Container();
          },
        );
      },
    );
  }

  Widget _showPlus() {

    int total = 0;

    return FutureBuilder(
      future: mainStore.hashtag.getTotalMedias(hashtag: widget.hashtag),
      builder: (context, snapshot) {

        if(snapshot.hasData)
          total = snapshot.data as int;

        if(total > 5) {

          return Padding(
            padding: const EdgeInsets.only(left: 8),
            child: GestureDetector(
              onTap: () async => await _showMediasComplete(context: context),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 1.5, color: Color.fromRGBO(230, 230, 230, 1)),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Icon(Icons.plus_one, color: Colors.grey,  size: 50),
              ),
            ),
          );
        }

        return Container();
      },
    );
  }

  _showMediasComplete({ required BuildContext context }) async {

    await mainStore.hashtag.addMedias(hashtag: widget.hashtag);

    Navigator.of(context).push(UtilService.createRoute(
        FutureBuilder(
          future: mainStore.hashtag.medias.containsKey(widget.hashtag.name) ?
          mainStore.hashtag.medias[widget.hashtag.name] : Future.value(),
          builder: (context, snapshot) {

            if(snapshot.hasData) {

              final Query query = snapshot.data as Query;

              if(query != null) {

                return MediaGroupMini(
                  keyName: 'MiniAudio',
                  type: MediaScreenType.SEARCH,
                  query: query,
                  //medias: widget.audios,
                  currentPosition: 0,
                  isHome: false,
                );
              }
            }

            return Container();
          },
        )),
    );
  }

}
