
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/main.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:bridge/shared/internet_loading_failed.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/shared/loading.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:bridge/widgets/profile/profile_details.dart';
import 'package:bridge/Translate.dart';

class UserSearch extends StatefulWidget {

  const UserSearch({Key? key}) : super(key: key);

  @override
  _UserSearchState createState() => _UserSearchState();
}

class _UserSearchState extends State<UserSearch> {

  final ScrollController _scrollController = ScrollController();
  final TextEditingController _userController = TextEditingController();
  final UserService _userService = UserService();

  List<Users> _users = [];
  List<Users> _search = [];
  bool _showSearch = false;

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    mainStore.user.addAllUsers();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(left: 18, right: 18, top: 45),
          child: Column(
            children: [
              Column(
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () => Navigator.pop(context),
                        child: Container(
                          child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                        ),
                      ),
                      const SizedBox(width: 6),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text('${ _intl.translate('SCREEN.SEARCH.USER_SEARCH.TITLE') }', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                  const SizedBox(height: 18),
                  CustomField(
                    controller: _userController,
                    autofocus: false,
                    prefixIcon: Icon(Icons.search, color: Colors.grey),
                    text: '${ _intl.translate('SCREEN.SEARCH.USER_SEARCH.FIELD.SEARCH') }',
                    maxLines: 1,
                    backgroundColor: Colors.grey[200],
                    textColor: Colors.grey,
                    borderColor: Colors.grey[200],
                    onChanged: (newName) {
                      _search.clear();

                      if(_showSearch != null && _users.length > 0){
                        if(!newName.isEmpty) {
                          setState(() => _showSearch = true );
                          _users.forEach((elem) {
                            if(elem.username.startsWith(newName))
                              setState(() => _search.add(elem) );
                          });
                        } else
                          setState(() => _showSearch = false );
                      }
                    },
                  ),
                ],
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 25, bottom: 50), //Menu bottom
                  child: Observer(
                    builder: (_) {

                      return StreamBuilder(
                        stream: mainStore.user.allUsers,
                        builder: (context, snapshot) {

                          switch(snapshot.connectionState) {
                            case ConnectionState.none:
                              return Loading(); //return InternetLoadingFailed();
                              break;
                            case ConnectionState.waiting:
                              return Loading();
                              break;
                            case ConnectionState.done:
                              return Loading();
                              break;
                            case ConnectionState.active: {

                              if(snapshot.hasData) {

                                final QuerySnapshot snap = snapshot.data as QuerySnapshot;

                                return FutureBuilder(
                                  future: _userService.makeList(documents: snap.docs),
                                  builder: (context, snapshot) {

                                    if(snapshot.hasData) {

                                      _users = snapshot.data as List<Users>;

                                      return NotificationListener<OverscrollIndicatorNotification>(
                                        onNotification: (overscroll) {
                                          overscroll.disallowIndicator();

                                          return false;
                                        },
                                        child: ListView.builder(
                                          padding: const EdgeInsets.all(0),
                                          itemCount: !_showSearch ? _users.length : _search.length,
                                          scrollDirection: Axis.vertical,
                                          controller: _scrollController,
                                          itemBuilder: (context, index) {

                                            final Users user = !_showSearch ? _users.elementAt(index) : _search.elementAt(index);

                                            return GestureDetector(
                                              onTap: () => Navigator.of(context).push(UtilService.createRoute(ProfileDetails(isCurrentUser: false, displayUser: user))),
                                              child: Container(
                                                padding: EdgeInsets.only(bottom: 25, top: 25),
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: Border(bottom: BorderSide(color: Colors.grey[400]!, width: 0.25)),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: [
                                                    ProfilePhoto(
                                                      width: 60,
                                                      height: 60,
                                                      user: user,
                                                    ),
                                                    const SizedBox(width: 12),
                                                    Expanded(
                                                      child: Padding(
                                                        padding: const EdgeInsets.only(right: 12),
                                                        child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: [
                                                            Align(
                                                              alignment: Alignment.centerLeft,
                                                              child: Row(
                                                                children: [
                                                                  Flexible(
                                                                    child: Text('${ user.firstName } ',
                                                                      overflow: TextOverflow.ellipsis,
                                                                      maxLines: 1,
                                                                      style: TextStyle(color: Colors.grey[700], fontSize: 18, fontWeight: FontWeight.bold),
                                                                    ),
                                                                  ),
                                                                  Flexible(
                                                                    child: Text('${ user.lastName }',
                                                                      overflow: TextOverflow.ellipsis,
                                                                      maxLines: 1,
                                                                      style: TextStyle(color: Colors.grey[700], fontSize: 18, fontWeight: FontWeight.bold),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            const SizedBox(height: 8),
                                                            Align(
                                                              alignment: Alignment.centerLeft,
                                                              child: Text('@${ user.username }',
                                                                overflow: TextOverflow.ellipsis,
                                                                maxLines: 1,
                                                                style: TextStyle(color: Colors.grey[600])),
                                                            ),
                                                            const SizedBox(height: 8),
                                                            Align(
                                                              alignment: Alignment.centerLeft,
                                                              child: Text(user.description != null && user.description.isNotEmpty ?
                                                                user.description : '${ _intl.translate('SCREEN.SEARCH.USER_SEARCH.TIP.NEW_USER') }',
                                                                overflow: TextOverflow.ellipsis,
                                                                maxLines: 1,
                                                                style: TextStyle(color: Colors.grey)),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      );
                                    }

                                    return Container();
                                  },
                                );
                              }
                            } break;
                          }

                          return NoResultFound(icon: Icons.person_search);
                        }
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
