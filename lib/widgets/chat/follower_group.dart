
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/chat/chat_screen.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/main.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:bridge/shared/internet_loading_failed.dart';

class FollowerGroup extends StatefulWidget {

  Users currentUser;

  FollowerGroup({Key? key,  required this.currentUser }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FollowerGroupState();
}

class _FollowerGroupState extends State<FollowerGroup> {

  List<Users> _users = [];
  List<Users> _search = [];
  bool _showSearch = false;

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    mainStore.user.addFollowedUsers();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(
              color: Colors.transparent,
              left: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                    ),
                  ),
                  const SizedBox(width: 6),
                  Text('${ Translate.of(context).translate('SCREEN.NOTIFICATION.FOLLOWER.TITLE') }', style: TextStyle(color: Colors.black, decoration: TextDecoration.none, fontSize: 18, fontWeight: FontWeight.bold, fontFamily: 'Arial')),
                ],
              ),
              hasBorder: true,
            ),
            const SizedBox(height: 15),
            Expanded(
              child: Observer(
                builder: (_) {

                  return Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          color: Colors.transparent,
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          child: CustomField(
                              autofocus: false,
                              maxLines: 1,
                              prefixIcon: Icon(Icons.search, color: Colors.grey, size: 25),
                              onChanged: (value) {
                                _search.clear();
                                if(!value.isEmpty){
                                  setState(() => _showSearch = true );
                                  _users.forEach((elem) {
                                    if(elem.username.startsWith(value))
                                      setState(() => _search.add(elem) );
                                  });
                                } else
                                  setState(() => _showSearch = false );
                              },
                              obscureText: false,
                              textColor: Colors.grey,
                              borderColor: Colors.grey[100]!,
                              backgroundColor: Colors.grey[100]!,
                              textAlign: TextAlign.start,
                              textAlignVertical: TextAlignVertical.center,
                              text: '${ _intl.translate('SCREEN.NOTIFICATION.FOLLOWER.FIELD.SEARCH') }'),
                        ),
                      ),
                      Expanded(
                        child: FutureBuilder(
                          future: mainStore.user.followedUsers,
                          builder: (context, snapshot){

                            switch(snapshot.connectionState) {
                              case ConnectionState.none:
                                return InternetLoadingFailed();
                                  break;
                              case ConnectionState.waiting:
                                return Loading();
                                  break;
                              case ConnectionState.active:
                                return NoResultFound(icon: Icons.group);
                                  break;
                              case ConnectionState.done: {

                                if(snapshot.hasData){

                                  _users = snapshot.data as List<Users>;

                                  if(_users.isNotEmpty) {

                                    return ListView.builder(
                                      itemCount: !_showSearch ? _users.length : _search.length,
                                      physics: const PageScrollPhysics(),
                                      scrollDirection: Axis.vertical,
                                      padding: EdgeInsets.all(0),
                                      itemBuilder: (context, position) {

                                        Users user = !_showSearch ? _users.elementAt(position) : _search.elementAt(position);

                                        return Padding(
                                          padding: const EdgeInsets.all(12),
                                          child: GestureDetector(
                                            onTap: () => Navigator.of(context).push(UtilService.createRoute(ChatScreen(currentUser: widget.currentUser, to: user))),
                                            child: Container(
                                              color: Colors.transparent,
                                              child: Row(
                                                children: [
                                                  ProfilePhoto(user: user, width: 40, height: 40 ),
                                                  const SizedBox(width: 15),
                                                  Expanded(
                                                    child: Column(
                                                      children: [
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: [
                                                            Text('${ user.firstName } ',
                                                              style: TextStyle(color: Colors.grey[700], decoration: TextDecoration.none, fontFamily: 'Arial', fontWeight: FontWeight.bold, fontSize: 18)),
                                                            Text('${ user.lastName }',
                                                              style: TextStyle(color: Colors.grey[700], decoration: TextDecoration.none, fontFamily: 'Arial', fontWeight: FontWeight.bold, fontSize: 18)),
                                                          ],
                                                        ),
                                                        const SizedBox(height: 5),
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: [
                                                            Text('@${ user.username }',
                                                              style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontFamily: 'Arial', fontWeight: FontWeight.normal, fontSize: 15)),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  }
                                }

                                return NoResultFound(icon: Icons.group);

                              } break;
                            }

                            return NoResultFound(icon: Icons.group);
                          },
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
