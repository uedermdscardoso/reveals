
import 'package:flutter/material.dart';

class Header extends StatelessWidget {

  final Widget? left;
  final Widget? center;
  final Widget? right;
  final Color? color;
  final bool? hasBorder;
  final bool? showLeft;
  final GestureTapCallback? onTapLeft;

  Header({
    Key? key,  this.left, this.center,
    this.right, this.color, this.hasBorder,
    this.showLeft, this.onTapLeft }) : super(key: key);

  @override
  Widget build(BuildContext context) { //Color.fromRGBO(246, 246, 246, 1)
    return Padding(
      padding: const EdgeInsets.only(left: 2.5, top: 45),
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          color: this.color ?? Color.fromRGBO(250, 250, 250, 1),
          border: this.hasBorder ?? false ? Border(bottom: BorderSide(color: Colors.grey, width: 0.30)) : null ,
        ),
        child: Padding(
          padding: const EdgeInsets.only(right: 15, left: 15), //, bottom: 12, top: 12
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Visibility(
                visible: this.showLeft ?? true,
                child: this.left ?? GestureDetector(
                    onTap: this.onTapLeft ?? () => Navigator.pop(context),
                    child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25)
                ),
              ),
              this.center ?? Container(),
              this.right ?? Container()
              /*Visibility(
                visible: currentUser.username.compareTo(displayUser.username) == 0,
                child: Padding(
                  padding: const EdgeInsets.only(right: 12),
                  child: GestureDetector(
                    onTap: () => Navigator.of(context).push(CardMessage.createRoute(ConfigScreen(currentUser: currentUser))),
                    child: Icon(Icons.more_vert, color: Colors.grey)
                  ),
                ),
              ),*/
            ],
          ),
        ),
      ),
    );
  }
}
