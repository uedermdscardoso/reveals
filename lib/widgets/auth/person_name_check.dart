
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:bridge/domain/model/utilities/report_type.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/auth/document_viewer.dart';
import 'package:flutter/services.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/auth/waiting_list_message.dart';
import 'package:bridge/widgets/auth/welcome_message.dart';
import 'package:bridge/domain/model/utilities/login_type.dart';
import 'package:bridge/Translate.dart';

class PersonNameCheck extends StatelessWidget {

  final _userService = UserService();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();

  late LoginType type;
  Users? invitedBy;

  double _padding = 0.0;

  PersonNameCheck({
    Key? key,
    required this.type,
    this.invitedBy,
  }) : super(key: key);

  late Translate _intl;

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);
    _padding = MediaQuery.of(context).size.height * 0.057;

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.grey[100],
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.grey[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(
              color: Colors.transparent,
              hasBorder: false,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(_padding), //45
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        Container(
                          width: 200,
                          child: Text('${ _intl.translate('SCREEN.LOGIN.CHECK_PERSON_NAME.TITLE') }',
                            textAlign: TextAlign.center,
                            style: const TextStyle(height: 1.5, fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                        ),
                        const SizedBox(height: 50),
                        Container(
                          width: 260,
                          child: CustomField(
                            style: const TextStyle(color: Colors.black, fontSize: 20),
                            keyboardType: TextInputType.text,
                            prefixIcon: const Icon(Icons.person, size: 22, color: Colors.grey),
                            obscureText: false,
                            controller: _firstNameController,
                            textAlign: TextAlign.start,
                            backgroundColor: Colors.white,
                            textColor: Colors.grey,
                            borderColor: Colors.white,
                            maxLines: 1,
                            text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PERSON_NAME.FIELD.FIRST_NAME') }',
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Column(
                      children: [
                        Container(
                          width: 260,
                          child: CustomField(
                            style: const TextStyle(color: Colors.black, fontSize: 20),
                            keyboardType: TextInputType.text,
                            prefixIcon: Icon(Icons.person, size: 22, color: Colors.grey),
                            obscureText: false,
                            controller: _lastNameController,
                            textAlign: TextAlign.start,
                            backgroundColor: Colors.white,
                            textColor: Colors.grey,
                            borderColor: Colors.white,
                            maxLines: 1,
                            text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PERSON_NAME.FIELD.LAST_NAME') }',
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 50),
                    Container(
                      width: 260,
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          style: const TextStyle(height: 1.25, fontSize: 13, color: Colors.grey),
                          children: <TextSpan>[
                            TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.FIRST_PART_2') } '),
                            TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.TERMS_OF_USE') }',
                              style: const TextStyle(color: Colors.black, decoration: TextDecoration.underline),
                              recognizer: new TapGestureRecognizer()..onTap = () => _showTermsOfUse(context: context),
                            ),
                            TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.MIDDLE_PART') }'),
                            TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.PRIVACY_POLICIES') }',
                              style: const TextStyle(color: Colors.black, decoration: TextDecoration.underline),
                              recognizer: new TapGestureRecognizer()..onTap = () => _showPrivacyPolicy(context: context),
                            ),
                            TextSpan(text: '. ${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.LAST_PART') }'),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 50),
                    GestureDetector(
                      onTap: () async => await _save(context: context),
                      child: Container(
                        width: 175,
                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(15),
                          child: Text('${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.ACTION.DONE') }', textAlign: TextAlign.center,
                              style: const TextStyle(fontSize: 18, color: Colors.white,
                                  fontWeight: FontWeight.bold, decoration: TextDecoration.none)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _save({ required BuildContext context }) async {
    String firstName = _firstNameController.text.trim();
    String lastName = _lastNameController.text.trim();

    final RegExp validCharacters = RegExp(r'^[a-zA-z0-9 ]+$');

    if(firstName.isNotEmpty && lastName.isNotEmpty) {
      if(firstName.length <= 16 && lastName.length <= 16) { //16
        if(validCharacters.hasMatch(firstName) && validCharacters.hasMatch(lastName)) {

          firstName = UtilService.captialize(text: firstName);
          lastName = UtilService.captialize(text: lastName);

            if(this.type.index == LoginType.BY_WAITING_LIST.index) {
              _saveTempUser(context: context, firstName: firstName, lastName: lastName);
            } else {
              _saveByInvite(context: context, firstName: firstName, lastName: lastName, invitedBy: this.invitedBy);
            }

        } else {
          UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PERSON_NAME.NOTIFICATION.CHARACTERS_NOT_VALID') }');
        }
      } else {
        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PERSON_NAME.NOTIFICATION.FIELD_IS_TOO_LONG') }');
      }
    } else {
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PERSON_NAME.NOTIFICATION.FIELD_INVALID') }');
    }
  }

  _saveTempUser({ required BuildContext context, required String firstName, required String lastName }) async {
    UtilService.onLoading(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_USERNAME.NOTIFICATION.ADD_WAITING_LIST') }');

    await mainStore.auth.addUserToWaitingList(firstName: firstName, lastName: lastName);

    Navigator.pop(context); //close loading

    Navigator.of(context).pushAndRemoveUntil(UtilService.createRoute(WaitingListMessage()), (Route<dynamic> route) => false);
  }

  void _saveByInvite({
    required BuildContext context, required String firstName,
    required String lastName, required invitedBy,
  }) async {
    UtilService.onLoading(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_USERNAME.NOTIFICATION.LOADING') }');

    final String username = mainStore.auth.username;
    final String phone = mainStore.auth.phoneNumber;

    if(username.isNotEmpty && phone.isNotEmpty && firstName.isNotEmpty && lastName.isNotEmpty) {
      final Users guest = Users.toInvite(
        firstName: firstName,
        lastName: lastName,
        username: username,
        phone: phone,
        invited: true,
        invitedBy: invitedBy.id ?? '',
        exited: true,
      );

      await mainStore.user.saveInvite(guest: guest);

      Navigator.pop(context); //close loading

      Navigator.of(context).pushAndRemoveUntil(UtilService.createRoute(WelcomeMessage()), (Route<dynamic> route) => false);
    }
  }

  _showPrivacyPolicy({ required BuildContext context }) {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => const DocumentViewer(
        type: ReportType.PRIVACY_POLICY,
      )),
    );
  }

  _showTermsOfUse({ required BuildContext context }) {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => const DocumentViewer(
        type: ReportType.TERMS_OF_USE,
      )),
    );
  }

}
