
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/main.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/auth/phone_number_check.dart';
import 'package:country_codes/country_codes.dart';
import 'package:bridge/domain/model/utilities/login_type.dart';
import 'package:bridge/Translate.dart';

class WaitingListMessage extends StatefulWidget {
  @override
  _WaitingListMessageState createState() => _WaitingListMessageState();
}

class _WaitingListMessageState extends State<WaitingListMessage> {

  Users _user = Users();

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    mainStore.auth.addLocalUser();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.dark,
          systemNavigationBarColor: Colors.grey[100],
          systemNavigationBarIconBrightness: Brightness.dark,
          systemNavigationBarDividerColor: Colors.white,
        ),
        child: Material(
          color: Colors.grey[100],
          child: Padding(
            padding: const EdgeInsets.only(top: 45,left: 52, right: 52),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset('assets/icons/congratulations.png', scale: 1.5),
                const SizedBox(height: 30),
                FutureBuilder(
                  future: mainStore.auth.localUser,
                  builder: (context, snapshot) {

                    if(snapshot.hasData)
                      _user = snapshot.data as Users;

                    return RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: const TextStyle(
                          height: 1.75,
                          color: Colors.grey,
                          fontSize: 18,
                        ),
                        children: <TextSpan>[
                          TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.WAITING_LIST.MESSAGE.FIRST') } '),
                          TextSpan(text: _user != null && _user.username != null && _user.username.isNotEmpty ?
                             '@${ _user.username } ' : '',
                            style: const TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold)),
                          TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.WAITING_LIST.MESSAGE.LAST') }'),
                        ],
                      ),
                    );
                  },
                ),
                const SizedBox(height: 30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('${ _intl.translate('SCREEN.LOGIN.WAITING_LIST.MESSAGE.HAVE_AN_INVITE') }',
                      style: const TextStyle(color: Colors.black54, fontSize: 14)),
                    const SizedBox(width: 6),
                    GestureDetector(
                      onTap: () async => await _checkPhoneNumber(context: context),
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 0.25),
                        child: Text('${ _intl.translate('SCREEN.LOGIN.WAITING_LIST.ACTION.SIGN_IN') }',
                            style: const TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 14)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _checkPhoneNumber({ required BuildContext context }) async {
    await CountryCodes.init();

    final CountryDetails details = CountryCodes.detailsForLocale();

    Navigator.of(context).push(UtilService.createRoute(
      PhoneNumberCheck(details: details, type: LoginType.BY_WAITING_LIST),
    ));
  }
}
