
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:bridge/domain/model/utilities/loading_icon_type.dart';
import 'package:bridge/domain/model/utilities/report_type.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/auth/document_viewer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:bridge/main.dart';
import 'package:bridge/widgets/auth/person_name_check.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/widgets/auth/user_name_check.dart';
import 'package:phone_number/phone_number.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/widgets/auth/waiting_list_message.dart';
import 'package:bridge/widgets/auth/welcome_message.dart';
import 'package:bridge/domain/model/utilities/login_type.dart';
import 'package:bridge/widgets/auth/sms_code.dart';
import 'package:country_codes/country_codes.dart';
import 'package:bridge/services/utilities/email_service.dart';

class PhoneNumberCheck extends StatefulWidget {

  final CountryDetails details;
  final LoginType type;

  const PhoneNumberCheck({ required this.details, required this.type });

  @override
  _PhoneNumberCheckState createState() => _PhoneNumberCheckState();
}

class _PhoneNumberCheckState extends State<PhoneNumberCheck> {

  final _phoneNumberController = TextEditingController();

  String _countrySelectedCode = '';

  late Translate _intl;

  @override
  void initState() {
    _addPhoneDetails();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _addPhoneDetails() {
    _countrySelectedCode = widget.details.alpha2Code!.toLowerCase(); //alpha2Code //for example US.
    _phoneNumberController.text = '${ widget.details.dialCode } '; //dialCode //for example +1.
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.grey[100],
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.grey[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(
              color: Colors.transparent,
              hasBorder: false,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FIELD.PHONE_NUMBER') }',
                    style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                  const SizedBox(height: 50),
                  Container(
                    width: 260,
                    child: CustomField(
                      autofocus: false,
                      style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
                      keyboardType: TextInputType.phone,
                      prefixIcon: Container(
                        width: 1,
                        child: Image.asset('icons/flags/png/${ _countrySelectedCode.toLowerCase() }.png',
                          package: 'country_icons', scale: 4),
                      ),
                      obscureText: false,
                      controller: _phoneNumberController,
                      textAlign: TextAlign.start,
                      backgroundColor: Colors.white,
                      textColor: Colors.black,
                      borderColor: Colors.white,
                      maxLines: 1,
                      //text: '+55',
                      inputFormatters: [
                        PhoneInputFormatter(
                          onCountrySelected: (PhoneCountryData? countryData) {
                            if(countryData != null)
                              setState(() => _countrySelectedCode = countryData.countryCode!.toLowerCase());
                            else {
                              setState(() => _countrySelectedCode = widget.details.alpha2Code!.toLowerCase());
                            }
                          }
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 50),
                  Container(
                    width: 260,
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: const TextStyle(height: 1.5, fontSize: 13, color: Colors.grey),
                        children: <TextSpan>[
                          TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.FIRST_PART') } '),
                          TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.TERMS_OF_USE') }',
                            style: const TextStyle(color: Colors.black, decoration: TextDecoration.underline),
                            recognizer: TapGestureRecognizer()..onTap = () => _showTermsOfUse(context: context),
                          ),
                          TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.MIDDLE_PART') }'),
                          TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.PRIVACY_POLICIES') }',
                            style: const TextStyle(color: Colors.black, decoration: TextDecoration.underline),
                            recognizer: TapGestureRecognizer()..onTap = () => _showPrivacyPolicy(context: context),
                          ),
                          TextSpan(text: '. ${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.LAST_PART') }'),
                        ]
                      ),
                    ),
                  ),
                  const SizedBox(height: 50),
                  GestureDetector(
                    onTap: () async {
                      await _checkPhone(context: context);
                    },
                    child: Container(
                      width: 175,
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 15, bottom: 15, right: 20, left: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.ACTION.NEXT') }', textAlign: TextAlign.center,
                              style: const TextStyle(fontSize: 18, color: Colors.white,
                                fontWeight: FontWeight.bold, decoration: TextDecoration.none)),
                            const Icon(Icons.arrow_forward_ios, color: Colors.white, size: 20),
                          ],
                        ),
                      ),
                    ),
                  ),
                  /*const SizedBox(height: 12),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      child: Text('Back', textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 15.5, color: Colors.black54, fontFamily: 'Arial', fontWeight: FontWeight.normal, decoration: TextDecoration.none))
                    ),
                  ),*/
                  //const SizedBox(height: MediaQuery.of(context).viewInsets.bottom * 0.99), // When open keyboard
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _checkPhone({ required BuildContext context }) async {
    try {
      final UserService userService = UserService();

      if(FirebaseAuth.instance.currentUser == null)
        FirebaseAuth.instance.signInAnonymously();

      if(_phoneNumberController != null && _phoneNumberController.text != null && _countrySelectedCode.isNotEmpty) {
        final String phone = _phoneNumberController.text.replaceAll(RegExp(r'[ ()-]'), '');

        final bool isValidated = await _isPhoneValidated(phone: phone, regionCode: _countrySelectedCode.toUpperCase());

        if(isValidated) {
          final bool isWaitingList = await userService.existsByPhone(user: Users.onlyPhone(phone: phone));

          if(!isWaitingList) {
            await _savePhone(context: context, phone: phone);
          } else {
            final bool isWaiting = await userService.isWaiting(user: Users.onlyPhone(phone: phone));

            if(!isWaiting)
              await _verifyPhoneNumber(context: context, number: phone);
            else {
              final bool hasInvite = await userService.hasInvite(user: Users.onlyPhone(phone: phone));

              if(!hasInvite)
                Navigator.of(context).pushAndRemoveUntil(UtilService.createRoute(WaitingListMessage()), (Route<dynamic> route) => false);
              else {
                UtilService.onLoading(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.LOADING.CHECKING_PHONE') }');

                final Users guest = await userService.getUserWaitingByPhone(phone: phone);

                if(guest == null || guest.username == null || guest.username.isEmpty) {
                  await mainStore.auth.addPhoneNumber(number: phone);

                  final Users invitedBy = await userService.getInvitedBy(user: Users.onlyPhone(phone: phone));

                  Navigator.pop(context); //close loading

                  Navigator.of(context).push(UtilService.createRoute(
                    UsernameCheck(
                      type: LoginType.BY_INVITE,
                      invitedBy: invitedBy,
                    ),
                  ));
                } else {
                  await mainStore.auth.saveLocalUser(user: guest);

                  Navigator.pop(context); //close loading

                  Navigator.of(context).pushAndRemoveUntil(UtilService.createRoute(WelcomeMessage()), (Route<dynamic> route) => false);
                }
              }
            }
          }
        } else
          await UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.PHONE_NUMBER_INVALID') }');
      } else
        await UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.PHONE_NUMBER_INVALID') }');
    } on PlatformException catch(e) {
      if(e != null && e.code != null && e.code.compareTo('InvalidNumber') == 0) {
        await UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.PHONE_NUMBER_INVALID') }');
      }
    }
  }

  Future<bool> _isPhoneValidated({ required String phone, required String regionCode }) async {
    final PhoneNumberUtil plugin = PhoneNumberUtil();
    final bool isValid = await plugin.validate(phone, regionCode: regionCode);

    return isValid;
  }

  Future<void> _savePhone({ required BuildContext context, required String phone }) async {
    await mainStore.auth.addPhoneNumber(number: phone);

    Navigator.of(context).push(UtilService.createRoute(
      UsernameCheck(type: LoginType.BY_WAITING_LIST),
    ));
  }

  Future<void> _verifyPhoneNumber({ required BuildContext context, required String number }) async {

    UtilService.onLoading(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.LOADING.CHECKING_PHONE') }');

    final FirebaseAuth _auth = FirebaseAuth.instance;

    await _auth.verifyPhoneNumber(
      phoneNumber: number,
      timeout: const Duration(seconds: 120),
      verificationCompleted: (PhoneAuthCredential credential) {},
      verificationFailed: (FirebaseAuthException e) {
        switch(e.code) {
          case 'invalid-phone-number':
            UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.PHONE_NUMBER_INVALID') }');
            break;
          case 'too-many-requests': {
            UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.TOO_MANY_REQUESTS') }');
          } break;
          case 'app-not-authorized': {
            UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.APP_NOT_AUTHORIZED') }');
            break;
          }
          default:
            UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.VERIFICATION_FAILED') }');
            break;
        }

        Navigator.pop(context); //close loading
      },
      codeSent: (String? verificationId, int? resendToken) async {

        Navigator.pop(context); //close loading

        Navigator.push(context,
          MaterialPageRoute(builder: (context) => SmsCode(verificationId: verificationId ?? '')),
        );
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }

  _showPrivacyPolicy({ required BuildContext context }) {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => const DocumentViewer(
        type: ReportType.PRIVACY_POLICY,
      )),
    );
  }

  _showTermsOfUse({ required BuildContext context }) {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => const DocumentViewer(
        type: ReportType.TERMS_OF_USE,
      )),
    );
  }

}
