
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/domain/repository/media/media_repository.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/internet_loading_failed.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/shared/message_default.dart';
import 'package:bridge/widgets/media/media_group.dart';
import 'package:connectivity/connectivity.dart';
import '../main.dart';

class RecommendedMediaPage extends StatefulWidget  {
  @override
  _RecommendedMediaPageState createState() => _RecommendedMediaPageState();
}

class _RecommendedMediaPageState extends State<RecommendedMediaPage> with AutomaticKeepAliveClientMixin {

  final _mediaService = MediaService();
  final _mediaRepository = MediaRepository();

  List<Media> _medias = List<Media>.of({});
  List<QueryDocumentSnapshot> _docs = List<QueryDocumentSnapshot>.of({});

  late Translate _intl;

  @override
  void initState() {
    load();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant RecommendedMediaPage oldWidget) {
    load();
    super.didUpdateWidget(oldWidget);
  }

  load() async {
    await mainStore.recommendation.addRecommendedMedias();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return FutureBuilder(
      future: Future.value(Connectivity().checkConnectivity()),
      builder: (context, snapshot) {

        if(snapshot.hasData) {
          final ConnectivityResult connectivity = snapshot.data as ConnectivityResult;

          if(connectivity != ConnectivityResult.none) {

            return Observer(
              builder: (_) {

                return StreamBuilder(
                  stream: mainStore.recommendation.recommendedMedias,
                  builder: (context, snapshot) {

                    switch(snapshot.connectionState) {
                      case ConnectionState.none:
                        return InternetLoadingFailed();
                        break;
                      case ConnectionState.waiting:
                        return Loading();
                        break;
                      case ConnectionState.done:
                        return Loading();
                        break;
                      case ConnectionState.active: {

                        if(snapshot.hasData) {

                          _docs = snapshot.data as List<QueryDocumentSnapshot>;

                          if(_docs.isNotEmpty) {

                            return FutureBuilder(
                              future: _mediaService.makeMediaList(docs: _docs),
                              builder: (context, snapshot) {

                                if(snapshot.hasData) {

                                  _medias = snapshot.data as List<Media>;

                                  if(_medias.isNotEmpty) {

                                    return MediaGroup(
                                      type: MediaScreenType.HOME,
                                      keyName: 'recommend',
                                      medias: _medias.toSet(),
                                      isHome: true,
                                      isRecommend: true,
                                    );
                                  }
                                }

                                return Loading();
                              },
                            );
                          } else {
                            return MessageDefault(
                              title: '${ _intl.translate('COMPONENT.MESSAGE_DEFAULT.RECOMMENDATION_NO_DATA.TITLE') }',
                              subtitle: '${ _intl.translate('COMPONENT.MESSAGE_DEFAULT.RECOMMENDATION_NO_DATA.SUBTITLE') }',
                              textButton: '${ _intl.translate('COMPONENT.MESSAGE_DEFAULT.RECOMMENDATION_NO_DATA.BUTTON.LOAD_MEDIA') }',
                              onTap: () async {
                                await mainStore.recommendation.addRecommendedMedias();

                                if(!mainStore.recommendation.recommendedMedias.hasValue ||
                                    mainStore.recommendation.recommendedMedias.value == null ||
                                    mainStore.recommendation.recommendedMedias.value.length == 0){
                                  UtilService.notify(context: context, message: '${ _intl.translate('COMPONENT.MESSAGE_DEFAULT.RECOMMENDATION_NO_DATA.ALERT.NO_DATA') }');
                                }
                              },
                            );
                          }
                        }

                      } break;
                    }

                    return Loading();

                  },
                );
              },
            );
          } else
            return InternetLoadingFailed();
        }

        return Loading();
      },
    );

  }

  @override
  bool get wantKeepAlive => true;
}
