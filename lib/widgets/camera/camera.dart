
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/file_uploader/preview_screen.dart';

class Camera extends StatefulWidget {

  List<CameraDescription> cameras;

  Camera({ required this.cameras });

  @override
  _CameraState createState() => _CameraState();
}

class _CameraState extends State<Camera> with AutomaticKeepAliveClientMixin {

  late CameraController _firstCameraController;
  late CameraController _secondCameraController;
  late Future<void> _initControllerFuture;
  bool _isInverted = true; //Load second camera

  @override
  void initState() {
    loadCameras();
    super.initState();
  }

  loadCameras(){
    if(widget.cameras != null) {
      _firstCameraController = CameraController(
        widget.cameras.first,
        ResolutionPreset.high,
        enableAudio: true,
      );

      _secondCameraController = CameraController(
        widget.cameras.elementAt(1),
        ResolutionPreset.high,
        enableAudio: true,
      );

      _initControllerFuture = _secondCameraController.initialize();
    }
  }

  @override
  void dispose() {
    _firstCameraController.dispose();
    _secondCameraController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context);
        Navigator.pop(context);
        return true;
      },
      child: Stack(
        children: [
          FutureBuilder<void>(
            future: _initControllerFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                final cameraController = !_isInverted ? _firstCameraController : _secondCameraController;

                return Transform.scale(
                  scale: 1.25,
                  child: AspectRatio(
                    aspectRatio: cameraController.value.aspectRatio * 0.30,
                    child: CameraPreview(cameraController),
                  ),
                );
              }

              return Container(color: Colors.black);
            },
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Header(
                color: Colors.transparent,
                left: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Image.asset('assets/icons/close.png', color: Colors.white, scale: 1.65),
                  ),
                  //child: Icon(Icons.close, size: 35, color: Colors.white70),
                ),
                right: Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: Stack(
                    children: [
                      Visibility(
                        visible: !_isInverted,
                        child: GestureDetector(
                          onTap: () {
                            _initControllerFuture = _secondCameraController.initialize();
                            setState(() => _isInverted = !_isInverted );
                          },
                          child: Image.asset('assets/icons/reverse.png', color: Colors.white, scale: 1.2),
                        ),
                      ),
                      Visibility(
                        visible: _isInverted,
                        child: GestureDetector(
                          onTap: () {
                            _initControllerFuture = _firstCameraController.initialize();
                            setState(() => _isInverted = !_isInverted );
                          },
                          child: Image.asset('assets/icons/reverse.png', color: Colors.white, scale: 1.2),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              //Record button
              Padding(
                padding: const EdgeInsets.only(left: 28, right: 28, bottom: 28),
                child: Container(
                  width: 65,
                  height: 65,
                  child: GestureDetector(
                    onTap: () => _takePicture(),
                    child: Container(
                      width: 60,
                      height: 60,
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: Image.asset('assets/icons/take_photo.png', scale: 4.25, color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  _takePicture() async {
    final controller = !_isInverted ? _firstCameraController : _secondCameraController;
    final String _mediaName = Uuid().v4();

    await _initControllerFuture;

    //_mediaPath = join((await getTemporaryDirectory()).path, '${ _mediaName }.jpg');
    //await controller.takePicture(_mediaPath);

    //File image = File(_mediaPath);
    final XFile xImage = await controller.takePicture();
    final File image = File(xImage.path);

    Navigator.of(context).push(UtilService.createRoute(PreviewScreen(preview: image)));

  }

  @override
  bool get wantKeepAlive => true;
}
