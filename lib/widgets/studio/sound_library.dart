
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/utilities/loading_icon_type.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/services/watson/watson_service.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/widgets/studio/creating_captions.dart';
import 'package:bridge/widgets/studio/media_publishing.dart';

class SoundLibrary extends StatefulWidget {

  final Set<Media> search;
  final Future<Set<Media>> library;
  final String type;

  const SoundLibrary({Key? key, required this.library, required this.search, required this.type }) : super(key: key);

  @override
  _SoundLibraryState createState() => _SoundLibraryState();
}

class _SoundLibraryState extends State<SoundLibrary> {

  final AudioPlayer _audioPlayer = AudioPlayer();

  String _audioName = '';

  late Translate _intl;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _audioPlayer.stop();
    super.dispose();
  }

  _controlAudioPlayer({ required Media media }){
    if(media != null) {
      _audioPlayer.onPlayerStateChanged.listen((s) {
        if ( mounted && (s == PlayerState.completed || s == PlayerState.stopped)) {
          setState(() => media.isPlaying = false );
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return FutureBuilder(
      future: widget.library,
      builder: (context, snapshot) {

        switch(snapshot.connectionState) {
          case ConnectionState.none: {} break;
          case ConnectionState.active: {} break;
          case ConnectionState.waiting: {} break;
          case ConnectionState.done: {
            if(snapshot.hasData) {

              final Set<Media> audios = snapshot.data as Set<Media>;

              return NotificationListener<OverscrollIndicatorNotification>(
                onNotification: (overscroll) {
                  overscroll.disallowIndicator();

                  return false;
                },
                child: ListView.builder(
                  itemCount: widget.search.isEmpty ? audios.length : widget.search.length,
                  padding: EdgeInsets.all(6),
                  itemBuilder: (context, position) {

                    Media audio = widget.search.isEmpty ? audios.elementAt(position) : widget.search.elementAt(position); //_audioService.createAudio(song: song, postedBy: widget.currentUser);
                    audio.isSaved = false;

                    _controlAudioPlayer(media: audio);

                    if(audio.title == null || audio.title.isEmpty)
                      _audioName = '${ _intl.translate('SCREEN.STUDIO.PUBLISH.AUDIO_SEARCH.FIELD.UNKNOWN_MEDIA') }';
                    else {
                      _audioName = audio.title;
                      if(_audioName.contains('.'))
                        _audioName = _audioName.replaceRange(_audioName.indexOf('.'), _audioName.length, '');
                    }

                    return GestureDetector(
                      onTap: () => controlAudioStatus(audio: audio),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 14, bottom: 14),
                        child: Container(
                          color: Colors.transparent,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 8),
                                    child: Container(
                                      width: 50,
                                      height: 50,
                                      decoration: BoxDecoration(
                                        image: audio.image == null || audio.image.isEmpty ?
                                          DecorationImage(image: AssetImage('assets/icons/headphone-white.png')) :
                                          DecorationImage(image: NetworkImage(audio.image)),
                                        color: Colors.grey, //Color.fromRGBO(230, 230, 230, 1),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(8),
                                        child: BackdropFilter(
                                          filter: ImageFilter.blur(sigmaX: 4.0, sigmaY: 4.0),
                                          child: Stack(
                                            fit: StackFit.expand,
                                            children: [
                                              !audio.isPlaying ?
                                              Image.asset('assets/icons/arrow.png', color: Colors.black.withOpacity(0.65), scale: 3) :
                                              Image.asset('assets/icons/pause.png', scale: 1.5)
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(width: 12),
                                  Container(
                                    width: MediaQuery.of(context).size.width * 0.6,
                                    child: Column(
                                      children: [
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(_audioName, maxLines: 1, overflow: TextOverflow.ellipsis,
                                            style: TextStyle(color: Colors.grey, fontSize: 15, fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        const SizedBox(height: 4),
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Visibility(
                                            visible: audio.isOfficial,
                                            child: const Text('Reveals', maxLines: 1, overflow: TextOverflow.ellipsis,
                                              style: TextStyle(color: Colors.grey, fontSize: 15),
                                            ),
                                            replacement: Text(audio != null ? audio.author : 'Unknown', maxLines: 1, overflow: TextOverflow.ellipsis,
                                              style: TextStyle(color: Colors.grey, fontSize: 15),
                                            ),
                                          ),
                                        ),
                                        const SizedBox(height: 4),
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(UtilService.formatCustomDuration(duration: Duration(milliseconds: audio != null ? audio.duration : 0)), maxLines: 1, overflow: TextOverflow.ellipsis,
                                            style: TextStyle(color: Colors.grey, fontSize: 15),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              GestureDetector(
                                onTap: () => _choose(context: context, audio: audio),
                                child: Visibility(
                                  visible: audio.isPlaying,
                                  child: Container(
                                    width: 50,
                                    height: 30,
                                    padding: const EdgeInsets.all(2),
                                    decoration: BoxDecoration(
                                      color: Colors.black,
                                      border: Border.all(
                                        color: Colors.black,
                                        width: 0.5,
                                      ),
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                    child: Icon(Icons.check,
                                      color: Colors.white,
                                      size: 20,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              );

            }
          } break;
        }

        return Loading();
      },
    );
  }

  controlAudioStatus({ required Media audio }) {
    if(_audioPlayer.state == null || _audioPlayer.state.index == 0 || _audioPlayer.state.index == 3){
      if(!audio.isPlaying)
        _audioPlayer.play(new AssetSource(audio.path));
      else
        _audioPlayer.stop();
    } else
      _audioPlayer.stop();

    setState(() => audio.isPlaying = !audio.isPlaying );
  }

  _choose({ required BuildContext context, required Media audio }) async {
    if(audio != null) {
      if(widget.type != null && widget.type.compareTo('upload') != 0) {
        if(audio.isPlaying) {
          //await mainStore.studio.addBackgroundSound(sound: audio);
          Navigator.pop(context);
        }
      } else { //to upload sound
        final mediaService = MediaService();

        final int inMiliseconds = await mediaService.getDuration(path: audio.path);
        final double inSeconds = inMiliseconds / 998;

        if(inSeconds > -1 && inSeconds <= 60) {
          await mainStore.studio.addAudioToSend(audio: audio);

          Navigator.pop(context);

          Navigator.of(context).push(UtilService.createRoute(CreatingCaptions(media: audio)));

          //Navigator.of(context).push(UtilService.createRoute(MediaPublishing(showModal: true)));
        } else
          UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.STUDIO.SOUND_LIBRARY.NOTIFICATION.TIME_NOT_ALLOWED') }');
      }
    }
  }

}
