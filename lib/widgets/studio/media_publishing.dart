
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/domain/model/media/media_dto.dart';
import 'package:bridge/main.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/services/hashtag/hashtag_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/shared/loading_modal.dart';
import 'package:bridge/widgets/header.dart';
import 'package:connectivity/connectivity.dart';
import 'package:bridge/shared/modal_dialog.dart';
import 'package:devicelocale/devicelocale.dart';
import 'package:bridge/widgets/studio/metadata_config.dart';

class MediaPublishing extends StatefulWidget {

  final bool showModal;

  const MediaPublishing({Key? key, required this.showModal }) : super(key: key);

  @override
  _MediaPublishingState createState() => _MediaPublishingState();
}

class _MediaPublishingState extends State<MediaPublishing> {

  final _titleController = TextEditingController();
  final _messageController = TextEditingController();
  final _mediaService = MediaService();
  final _hashTagService = HashTagService();

  late Translate _intl;

  late bool _isLoading;

  @override
  void initState() {
    _isLoading = false;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: WillPopScope(
        onWillPop: () async {
          if(widget.showModal) {
            return await _showAudioDiscardDialog(context: context);
          }
          return true;
        },
        child: Material(
          child: Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Header(
                  color: Colors.transparent,
                  left: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () => Navigator.pop(context),
                        child: Container(
                          child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                        ),
                      ),
                      Text('${ _intl.translate('SCREEN.STUDIO.PUBLISH.TITLE') }', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                    ],
                  ),
                  right: GestureDetector(
                    onTap: () async {
                      final ConnectivityResult connectivityResult = await Connectivity().checkConnectivity();

                      if(connectivityResult != ConnectivityResult.none)
                        await _publish(context: context);
                      else
                        UtilService.notify(context: context, message: '${ _intl.translate('CONNECTION.INTERNET.FAILED') }');
                    },
                    child: Container(
                      padding: EdgeInsets.only(top: 8, bottom: 8, right: 18, left: 18),
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Text('Publish', style: TextStyle(color: Colors.white)),
                    ),
                  ),
                  hasBorder: true,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 25, bottom: 25, left: 18, right: 18),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text('${ _intl.translate('SCREEN.STUDIO.PUBLISH.TIP.DESCRIPTION') }', style: TextStyle(color: Colors.grey[600], fontSize: 12)),
                            ),
                            const SizedBox(height: 6),
                            CustomField(
                              controller: _messageController,
                              autofocus: false,
                              textColor: Colors.grey,
                              borderColor: Colors.grey[300],
                              backgroundColor: Colors.white,
                              maxLines: 5,
                              style: TextStyle(height: 1.5),
                              contentPadding: EdgeInsets.all(18),
                              maxLength: 100,
                              buildCounter: (BuildContext context, { int? currentLength, int? maxLength, bool? isFocused }) {

                                return Align(
                                  alignment: Alignment.centerRight,
                                  child: Text('${ currentLength }/${ maxLength }',
                                    style: TextStyle(
                                      color: (currentLength ?? 0) <= (maxLength ?? 0) ? Colors.grey : Colors.red,
                                    ),
                                  ),
                                );
                              },
                              text: '${ _intl.translate('SCREEN.STUDIO.PUBLISH.FIELD.DESCRIPTION') }',
                            ),
                          ],
                        ),
                        const SizedBox(height: 18),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Insere os metadados do seu áudio:',
                                style: TextStyle(color: Colors.grey[600])),
                            GestureDetector(
                              onTap: () => Navigator.of(context).push(UtilService.createRoute(MetadataConfig())),
                              child: Container(
                                padding: const EdgeInsets.all(8.0),
                                child: Icon(Icons.settings, color: Colors.black, size: 25),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showLoading() {
    showModalBottomSheet<void>( //loading
      context: context, elevation: 0, barrierColor: Colors.white.withAlpha(1),
      isDismissible: false,
      builder: (BuildContext context) {
        String message = _intl.translate('SCREEN.STUDIO.MODAL.LOADING.SAVING_MEDIA');
        return LoadingModal(message: message);
      },
    );
  }

  _publish({ required BuildContext context }) async {
    setState(() => _isLoading = true );

    final String title = _titleController.text.trim();
    final String message = _messageController.text.trim();

    if(title.isNotEmpty) {
      if(message.isNotEmpty) {

        showLoading();

        if(mainStore.auth.currentUser != null) {
          final List<String> hashtags = _hashTagService.findByText(text: message);

          final String currentLocale = (await Devicelocale.currentLocale)!;
          final String authorLocale = currentLocale.toLowerCase();
          final Media longAudio = await mainStore.studio.longAudioSelected!;
          final Media audio = await mainStore.studio.audioToSend!;

          final String title = await mainStore.studio.title!;
          final String author = await mainStore.studio.author!;

          final MediaDTO audioDTO = MediaDTO(
            media: audio,
            title: title,
            author: author,
            message: message,
            hashtags: hashtags,
            locale: authorLocale,
            isOfficial: false,
            linkId: longAudio != null ? longAudio.id : '',
            linkType: longAudio != null ? longAudio.linkType : '',
         );

          await _mediaService.save(media: audioDTO);
          await _hashTagService.save(media: audioDTO);
        }

        await mainStore.studio.clearTimer();
        await mainStore.studio.clearAudioToSend();
        await mainStore.imageChossing.clear();

        Navigator.pop(context);
        Navigator.pop(context);

        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.STUDIO.PUBLISH.ALERT.SUCCESS') }');
      } else
        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.STUDIO.PUBLISH.ALERT.MSG_EMPTY') }');
    } else
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.STUDIO.PUBLISH.ALERT.TITLE_EMPTY') }');
  }

  Future<bool> _showAudioDiscardDialog({ required BuildContext context }) async {
    bool? isDiscarded = await showDialog<bool>(
      context: context,
      barrierDismissible: true,
      barrierColor: Colors.white.withAlpha(1),
      builder: (BuildContext context) {
        return ModalDialog(
          title: '${ _intl.translate('SCREEN.STUDIO.MODAL.DISCARD.TITLE') }',
          positiveButtonName: '${ _intl.translate('SCREEN.STUDIO.MODAL.DISCARD.ACTION.KEEP') }',
          negativeButtonName: '${ _intl.translate('SCREEN.STUDIO.MODAL.DISCARD.ACTION.DISCARD') }',
        );
      },
    );

    if(isDiscarded ?? false) {
      await mainStore.studio.clearAudioToSend();
    }

    return isDiscarded ?? false;
  }
}
