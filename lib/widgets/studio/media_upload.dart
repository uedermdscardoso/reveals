
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/main.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/studio/sound_library.dart';

class MediaUpload extends StatefulWidget {
  @override
  _MediaUploadState createState() => _MediaUploadState();
}

class _MediaUploadState extends State<MediaUpload> {

  final _searchEditingController = TextEditingController();

  Set<Media> _audios = Set<Media>();
  Set<Media> _soundSearch = Set<Media>();

  bool _showSearch = false;

  late Translate _intl;

  @override
  void initState() {
    mainStore.studio.addLocalSoundLibrary();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              Header(
                onTapLeft: () => Navigator.pop(context),
                color: Colors.white,
                center: Expanded(
                  child: FutureBuilder(
                    future: mainStore.studio.localSoundLibrary,
                    builder: (context, snapshot) {

                      if(snapshot.hasData)
                        _audios = snapshot.data as Set<Media>;

                      return CustomField(
                        controller: _searchEditingController,
                        autofocus: false,
                        suffixIcon: Visibility(
                          visible: _searchEditingController.text != null && _searchEditingController.text.isNotEmpty,
                          child: GestureDetector(
                            onTap: () {
                              _searchEditingController.text = '';
                              setState(() => _soundSearch.clear());
                            },
                            child: Icon(Icons.close, size: 25, color: Colors.grey),
                          ),
                        ),
                        text: '${ _intl.translate('SCREEN.MEDIA.BUTTON.SEARCH') }',
                        maxLines: 1,
                        backgroundColor: Colors.white,
                        textColor: Colors.grey,
                        borderColor: Colors.white,
                        onChanged: (value){
                          _soundSearch.clear();

                          if(_audios.isNotEmpty) {
                            if(!value.toString().isEmpty) {
                              _audios.forEach((elem) {
                                if(elem.title.toLowerCase().startsWith(value.toLowerCase()))
                                  setState(() => _soundSearch.add(elem) );
                              });
                              setState(() => _showSearch = true);
                            } else
                              setState(() => _showSearch = false);
                          }
                        },
                      );
                    },
                  ),
                ),
                hasBorder: true,
              ),
              const SizedBox(height: 8),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8, bottom: 18),
                          child: Text('${ _intl.translate('SCREEN.STUDIO.PUBLISH.TIP.OWN_AUDIO') }', style: TextStyle(color: Colors.grey, fontSize: 15)),
                        ),
                      ),
                      Expanded(
                        child: Observer(
                          builder: (_) {

                            return SoundLibrary(
                              library: mainStore.studio.localSoundLibrary!,
                              search: _soundSearch,
                              type: 'upload',
                            );
                          },
                        ),
                      ),
                      const SizedBox(height: 12),
                      Container(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
