
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/widgets/studio/sound_library.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/shared/internet_loading_failed.dart';
import 'package:connectivity/connectivity.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/main.dart';

class BackgroundSoundSelection extends StatefulWidget {
  @override
  _BackgroundSoundSelectionState createState() => _BackgroundSoundSelectionState();
}

class _BackgroundSoundSelectionState extends State<BackgroundSoundSelection> with AutomaticKeepAliveClientMixin {

  final _searchEditingController = TextEditingController();
  final _pageController = PageController(initialPage: 0);

  Set<Media> _audios = Set<Media>();
  Set<Media> _soundSearch = Set<Media>();

  bool _showSearch = false;
  int page = 0;

  late Translate _intl;

  @override
  void initState() {
    mainStore.studio.addSoundLibrary();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: WillPopScope(
        onWillPop: () async {
          Navigator.pop(context);

          return false;
        },
        child: Material(
          child: Container(
            color: Colors.white,
            child: Column(
              children: [
                Header(
                  onTapLeft: () => Navigator.pop(context),
                  color: Colors.white,
                  center: Expanded(
                    child: FutureBuilder(
                      future: _getLibrary(page: page),
                      builder: (context, snapshot) {

                        if(snapshot.hasData)
                          _audios = snapshot.data as Set<Media>;

                        return CustomField(
                          controller: _searchEditingController,
                          autofocus: false,
                          suffixIcon: Visibility(
                            visible: _searchEditingController.text != null && _searchEditingController.text.isNotEmpty,
                            child: GestureDetector(
                              onTap: () {
                                _searchEditingController.text = '';
                                setState(() => _soundSearch.clear());
                              },
                              child: Icon(Icons.close, size: 25, color: Colors.grey),
                            ),
                          ),
                          text: '${ _intl.translate('SCREEN.MEDIA.BUTTON.SEARCH') }',
                          maxLines: 1,
                          backgroundColor: Colors.white,
                          textColor: Colors.grey,
                          borderColor: Colors.white,
                          onChanged: (value){
                            _soundSearch.clear();

                            if(_audios.isNotEmpty) {
                              if(!value.toString().isEmpty) {
                                _audios.forEach((elem) {
                                  if (elem.title.toLowerCase().startsWith(
                                      value.toLowerCase()))
                                    setState(() => _soundSearch.add(elem));
                                });
                                setState(() => _showSearch = true);
                              } else
                                setState(() => _showSearch = false);
                            }
                          },
                        );
                      },
                    ),
                  ),
                  right: GestureDetector(
                    onTap: () {
                      //mainStore.studio.clearBackgroundSound();
                      Navigator.pop(context);
                    },
                    child: Image.asset('assets/icons/none.png', scale: 1.6),
                  ),
                  hasBorder: true,
                ),
                const SizedBox(height: 8),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(12),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 8, bottom: 18),
                            child: Text('${ _intl.translate('SCREEN.STUDIO.PUBLISH.TIP.CLOUD_AUDIO') }', style: TextStyle(color: Colors.grey, fontSize: 15)),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  _pageController.jumpToPage(0);
                                  _soundSearch.clear();
                                  setState(() => page = 0 );
                                },
                                child: Container(
                                  padding: EdgeInsets.all(14),
                                  decoration: BoxDecoration(
                                    border: page == 0 ?
                                      Border(bottom: BorderSide(color: Colors.black, width: 0.75)) :
                                      Border(bottom: BorderSide(color: Colors.grey[200]!, width: 0.75)),
                                  ),
                                  child: Text('${ _intl.translate('SCREEN.STUDIO.PUBLISH.AUDIO_SEARCH.TYPE.OFFICIAL') }',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: page == 0 ? Colors.black : Colors.grey, fontSize: 15)),
                                ),
                              ),
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  _pageController.jumpToPage(1);
                                  _soundSearch.clear();
                                  setState(() => page = 1 );
                                },
                                child: Container(
                                  padding: EdgeInsets.all(14),
                                  decoration: BoxDecoration(
                                    border: page == 1 ?
                                    Border(bottom: BorderSide(color: Colors.black, width: 0.75)) :
                                    Border(bottom: BorderSide(color: Colors.grey[200]!, width: 0.75)),
                                  ),
                                  child: Text('${ _intl.translate('SCREEN.STUDIO.PUBLISH.AUDIO_SEARCH.TYPE.OTHER') }', //'${ _intl.translate('SCREEN.STUDIO.PUBLISH.AUDIO_SEARCH.TYPE.CLOUD') }',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: page == 1 ? Colors.black : Colors.grey, fontSize: 15)),
                                ),
                              ),
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  _pageController.jumpToPage(2);
                                  _soundSearch.clear();
                                  setState(() => page = 2 );
                                },
                                child: Container(
                                  padding: EdgeInsets.all(14),
                                  decoration: BoxDecoration(
                                    border: page == 2 ?
                                      Border(bottom: BorderSide(color: Colors.black, width: 0.75)) :
                                      Border(bottom: BorderSide(color: Colors.grey[200]!, width: 0.75)),
                                  ),
                                  child: Text('${ _intl.translate('SCREEN.STUDIO.PUBLISH.AUDIO_SEARCH.TYPE.PHONE') }',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: page == 2 ? Colors.black : Colors.grey, fontSize: 15)),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 12),
                        Expanded(
                          child: Observer(
                            builder: (_) {

                              return PageView(
                                physics: NeverScrollableScrollPhysics(),
                                controller: _pageController,
                                children: <Widget>[
                                  FutureBuilder(
                                    future: Connectivity().checkConnectivity(),
                                    builder: (context, snapshot) {

                                      if(snapshot.hasData) {
                                        final ConnectivityResult connectivityResult = snapshot.data as ConnectivityResult;

                                        if(connectivityResult != ConnectivityResult.none)
                                          return SoundLibrary(type: 'selection', library: mainStore.studio.cloudOfficialSoundLibrary!, search: _soundSearch);
                                      }

                                      return InternetLoadingFailed();
                                    },
                                  ),
                                  SoundLibrary(type: 'selection', library: mainStore.studio.cloudOtherSoundLibrary!, search: _soundSearch),
                                  SoundLibrary(type: 'selection', library: mainStore.studio.localSoundLibrary!, search: _soundSearch),
                                ],
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  _getLibrary({ required int page }) {
    if(page == 0)
      return mainStore.studio.cloudOfficialSoundLibrary;
    else if(page == 1)
      return mainStore.studio.cloudOtherSoundLibrary;
    else if(page == 2)
      return mainStore.studio.localSoundLibrary;
  }
}
