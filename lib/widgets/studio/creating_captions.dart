
import 'dart:io';

import 'package:bridge/domain/model/media/media_type.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/domain/model/media/media_language.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/domain/model/utilities/loading_icon_type.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/media/caption/custom_caption.dart';
import 'package:bridge/services/media/caption/caption_timestamp.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/services/watson/watson_service.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/media/media_group.dart';
import 'package:bridge/widgets/media/media_screen.dart';
import 'package:bridge/widgets/media/media_preview.dart';

class CreatingCaptions extends StatefulWidget {

  final Media media;

  const CreatingCaptions({ Key? key, required this.media }) : super(key: key);

  @override
  _CreatingCaptionsState createState() => _CreatingCaptionsState();
}

class _CreatingCaptionsState extends State<CreatingCaptions> {

  final ScrollController _controller = ScrollController();

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  _init() {
    //_loadCaptions(context: context, audio: widget.audio);
  }

  @override
  void dispose() {
    mainStore.watson.removeCaptions();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(
              left: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                    ),
                  ),
                  const Text('Creating Captions', style: TextStyle(color: Colors.grey, fontSize: 18, fontWeight: FontWeight.bold)),
                ],
              ),
              right: GestureDetector(
                onTap: () => _preview(context: context),
                child: Container(
                  padding: EdgeInsets.only(top: 8, bottom: 8, right: 18, left: 18),
                  decoration: BoxDecoration(
                    color: Colors.grey[800],
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Text('Preview', style: TextStyle(color: Colors.white)),
                ),
              ),
              color: Colors.transparent,
              hasBorder: true,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 6),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: GestureDetector(
                          onTap: () {
                            final String languageCode = _intl.locale.languageCode.toUpperCase();
                            MediaLanguage currentLanguage = MediaLanguage.EN_US;

                            switch(languageCode) {
                              case "EN":
                                currentLanguage = MediaLanguage.EN_US;
                                break;
                              case "PT":
                                currentLanguage = MediaLanguage.PT_BR;
                                break;
                              case "ES":
                                currentLanguage = MediaLanguage.ES_ES;
                                break;
                            }

                            _loadCaptions(context: context, language: currentLanguage, audio: widget.media.mediaFile!);
                          },
                          child: Container(
                            padding: const EdgeInsets.only(top: 8, bottom: 8, right: 18, left: 18),
                            decoration: BoxDecoration(
                              color: Colors.grey[100],
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: const Text('Recognize', style: TextStyle(color: Colors.black)),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(color: Colors.grey[400]!, width: 0.6),
                          ),
                          child: Observer(
                            builder: (_) {

                              return FutureBuilder(
                                future: mainStore.watson.captions,
                                builder: (context, snapshot) {

                                  if(snapshot.hasData) {

                                    final List<CustomCaption> captions = snapshot.data as List<CustomCaption>;

                                    if(captions.isNotEmpty) {

                                      return NotificationListener<OverscrollIndicatorNotification>(
                                        onNotification: (overscroll) {
                                          overscroll.disallowIndicator();

                                          return false;
                                        },
                                        child: ListView.builder(
                                          padding: EdgeInsets.all(8),
                                          itemCount: captions.length,
                                          controller: _controller,
                                          scrollDirection: Axis.vertical,
                                          itemBuilder: (context, position) {

                                            final CustomCaption caption = captions.elementAt(position);

                                            final String dots = "...";

                                            return Container(
                                              padding: EdgeInsets.only(top: 25, bottom: 25, left: 12, right: 12),
                                              decoration: BoxDecoration(
                                                border: Border(bottom: BorderSide(color: Colors.grey[400]!, width: 0.6)),
                                              ),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Text("${ (position+1) }", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25)),
                                                  const SizedBox(width: 15),
                                                  Expanded(
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        Align(
                                                          alignment: Alignment.centerLeft,
                                                          child: Text("${ position > 0 ? dots : '' }${ caption.transcript }${ dots }",
                                                              style: TextStyle(height: 1.5, color: Colors.grey[800])),
                                                        ),
                                                        const SizedBox(height: 8),
                                                        FutureBuilder(
                                                          future: Future.value(caption.timestamps),
                                                          builder: (context, snapshot) {

                                                            if(snapshot.hasData) {

                                                              final List<CaptionTimestamp> timestamps = snapshot.data as List<CaptionTimestamp>;

                                                              if(timestamps.isNotEmpty) {

                                                                final CaptionTimestamp start = timestamps.first;
                                                                final CaptionTimestamp end = timestamps.last;

                                                                return Row(
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  children: [
                                                                    Text("${ UtilService.formatDuration(duration: Duration(milliseconds: ((start.startTime) * 1000).toInt())) }",
                                                                        style: TextStyle(color: Colors.grey[500])),
                                                                    const SizedBox(width: 10),
                                                                    Text("-", style: TextStyle(color: Colors.grey[500])),
                                                                    const SizedBox(width: 10),
                                                                    Text("${ UtilService.formatDuration(duration: Duration(milliseconds: ((end.endTime) * 1000).toInt())) }",
                                                                        style: TextStyle(color: Colors.grey[500])),
                                                                  ],
                                                                );
                                                              }
                                                            }

                                                            return Container();
                                                          },
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  const SizedBox(width: 15),
                                                  Container(
                                                    padding: EdgeInsets.all(8),
                                                    decoration: BoxDecoration(
                                                      border: Border.all(color: Colors.grey[300]!),
                                                      borderRadius: BorderRadius.circular(10),
                                                    ),
                                                    child: Icon(Icons.edit, color: Colors.grey, size: 25),
                                                  ),
                                                ],
                                              ),
                                            );
                                          },
                                        ),
                                      );
                                    }
                                  }

                                  return NoResultFound(icon: Icons.data_usage, color: Colors.grey);
                                },
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _loadCaptions({ required BuildContext context, required MediaLanguage language, required File audio }) async {
    UtilService.onLoading(context: context, type: LoadingIconType.CIRCLE, message: "loading captions", isDark: true);

    final WatsonService watsonService = WatsonService();

    if((await audio.exists()))
      watsonService.recognize(language: language, audio: audio);

    Navigator.pop(context);
  }

  _preview({ required BuildContext context }) async {

    final AuthenticationService authService = new AuthenticationService();
    final MediaService mediaService = MediaService();

    if(widget.media == null || authService.isAnonymous())
      return;

    final Users createdBy = await authService.getCurrentUser();

    final Media preview = Media.toLongDetails(
      title: "preview",
      createdBy: createdBy,
      message: 'This is a preview #demo',
      mediaFile: widget.media.mediaFile,
      type: MediaType.SHORT_FORM_AUDIO,
      isOfficial: false,
      duration: await mediaService.getDuration(path: widget.media.mediaFile!.path),
      isLikedByCurrentUser: false,
    );

    Navigator.of(context).push(UtilService.createRoute(MediaPreview(
      medias: [ preview ].toSet(),
    )));
  }
}
