
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/media/effect.dart';
import 'package:bridge/shared/skeleton_loading.dart';

class EffectItem extends StatelessWidget {

  final Effect effect;

  const EffectItem({Key? key, required this.effect }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 50,
      decoration: BoxDecoration(
        color: Colors.grey[800],
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: CachedNetworkImage(
        imageUrl: effect.image ?? '',
        placeholder: (context, url) => SkeletonLoading(width: 50, height: 50),
        //progressIndicatorBuilder: (context, url, downloadProgress) =>
        //    CircularProgressIndicator(value: downloadProgress.progress),
        errorWidget: (context, url, error) => Image.asset('assets/icons/no_image.png', scale: 5, color: Colors.white),
      ),
    );
  }
}
