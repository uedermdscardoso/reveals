
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/studio/media_publishing.dart';

class MediaLink extends StatefulWidget {

  const MediaLink({ Key? key }) : super(key: key);

  @override
  _MediaLinkState createState() => _MediaLinkState();
}

class _MediaLinkState extends State<MediaLink> {

  @override
  Widget build(BuildContext context) {

    return Material(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Header(
            left: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: Container(
                    child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                  ),
                ),
                Text('Media Link', style: TextStyle(color: Colors.grey, fontSize: 18, fontWeight: FontWeight.bold)),
              ],
            ),
            right: GestureDetector(
              onTap: () => _nextStep(context: context),
              child: Container(
                padding: EdgeInsets.only(top: 8, bottom: 8, right: 18, left: 18),
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Text('Next', style: TextStyle(color: Colors.white)),
              ),
            ),
            color: Colors.transparent,
            hasBorder: false,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 18, right: 18),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text('Select episode or audiobook', style: TextStyle(color: Colors.grey)),
                          ),
                          const SizedBox(height: 18),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: GestureDetector(
                              onTap: () {
                                //Navigator.push(context, UtilService.createRoute(LongAudioChoose()));
                              },
                              child: Container(
                                width: 110,
                                height: 110,
                                decoration: BoxDecoration(
                                  color: Colors.grey[100],
                                  border: Border.all(color: Colors.grey, width: 0.5, style: BorderStyle.solid),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: FutureBuilder(
                                  future: mainStore.studio.longAudioSelected,
                                  builder: (context, snapshot) {

                                    if(snapshot.hasData) {

                                      final Media longAudio = snapshot.data as Media;

                                      return Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          ClipRRect(
                                            borderRadius: BorderRadius.circular(18),
                                            child: CachedNetworkImage(
                                              imageUrl: longAudio.image,
                                              width: 180, //175
                                              height: 180, //175
                                              fit: BoxFit.cover,
                                              placeholder: (context, url) => Image.asset('assets/images/thinker-dark.jpg', fit: BoxFit.cover),
                                              errorWidget: (context, url, error) => Image.asset('assets/images/thinker-dark.jpg', fit: BoxFit.cover)
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.all(10),
                                            decoration: BoxDecoration(
                                              border: Border.all(color: Colors.white, width: 1.25),
                                              borderRadius: BorderRadius.circular(100),
                                            ),
                                            child: Icon(Icons.edit, color: Colors.white, size: 30),
                                          ),
                                        ],
                                      );
                                    }

                                    return Icon(Icons.add, size: 50, color: Colors.grey);
                                  },
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _nextStep({ required BuildContext context }) async {
    Navigator.of(context).push(UtilService.createRoute(MediaPublishing(showModal: false)));
  }

}
