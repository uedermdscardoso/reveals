
import 'package:bridge/domain/model/media/media_type.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/media/cutted_media.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/widgets/media/media_details.dart';

class MediaAuthorDetails extends StatefulWidget {

  final bool isDemo;
  final bool isOriginal;
  final MediaType type;
  final Media? media;

  const MediaAuthorDetails({
    Key? key,
    this.isDemo : false,
    this.isOriginal : true,
    this.type : MediaType.SHORT_FORM_AUDIO,
    this.media,
  }) : super(key: key);

  @override
  _MediaAuthorDetailsState createState() => _MediaAuthorDetailsState();
}

class _MediaAuthorDetailsState extends State<MediaAuthorDetails> {

  CuttedMedia _clippedMedia = CuttedMedia();

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    _clippedMedia = widget.media?.originalMedia?.cuttedMedia ?? CuttedMedia();
  }

  @override
  Widget build(BuildContext context) {

    return Column(
      key: widget.key,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(18),
          child: CachedNetworkImage(
            imageUrl: widget.isOriginal! ? _clippedMedia.image ?? '' : widget.media?.image ?? '',
            width: 180, //1751
            height: 180, //175
            fit: BoxFit.cover,
            placeholder: (context, url) => Image.asset('assets/images/thinker-dark.jpg', fit: BoxFit.cover),
            errorWidget: (context, url, error) => Image.asset('assets/images/thinker-dark.jpg', fit: BoxFit.cover),
          ), //2.0
        ),
        MediaDetails(
          isDemo: widget.isDemo,
          isOriginal: widget.isOriginal,
          type: widget.type,
          media: widget.media ?? Media(),
        ),
      ],
    );
  }


}
