
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:bridge/domain/model/media/media_type.dart';
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/media/caption/custom_caption.dart';
import 'package:bridge/services/media/caption/caption_timestamp.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/animated_text.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/shared/loading_modal.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:bridge/widgets/auth/anonymous_profile_screen.dart';
import 'package:bridge/widgets/comment/comment_modal.dart';
import 'package:bridge/widgets/media/media_details.dart';
import 'package:bridge/widgets/media/audio_used/audio_used_group.dart';
import 'package:bridge/widgets/studio/audio_cut.dart';
import 'package:bridge/widgets/studio/recording_studio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:video_player/video_player.dart';
import 'package:visibility_detector/visibility_detector.dart';
import 'package:bridge/widgets/header.dart';

class MediaScreen extends StatefulWidget {

  final String keyName;
  final bool isDemo;
  final bool isCurrent;
  final num position;
  final Media media;
  final MediaType mediaType;
  final MediaScreenType type;
  final bool isHome;

  const MediaScreen({
   required this.keyName,
   this.isDemo : false,
   required this.media,
   this.position : 0,
   this.isCurrent : false,
   this.mediaType : MediaType.SHORT_FORM_AUDIO,
   this.type : MediaScreenType.DEFAULT,
   this.isHome : false,
  });

  @override
  State<StatefulWidget> createState() => _MediaScreenState();
}

class _MediaScreenState extends State<MediaScreen> with TickerProviderStateMixin {

  final ICON_ACTIONS_ASSET = 'assets/icons/actions';
  final IMAGE_ASSET = 'assets/images';
  final ICON_ASSET = 'assets/icons';

  final AuthenticationService _authService = AuthenticationService();
  final MediaService _mediaService = MediaService();
  final ScrollController _scrollCtrl = ScrollController(initialScrollOffset: 0);

  //final AudioPlayer _player = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
  late VideoPlayerController _controller;
  late Future<void> _initializeVideoPlayerFuture;
  bool _showArrow = false;

  late AnimationController _animateCtrl;
  late AnimationController _animationController;
  late Animation<int> _animation;
  late AnimationController _diskAnimationController;

  Duration _currentPosition = Duration(milliseconds: 0);
  double offset = 0.0;
  num _likes = 0;
  num _comments = 0;
  bool _isLiked = false;
  bool _isFollowing = false;
  int _currentCaptionNumber = 0;
  List<int> _currentWords = [];

  late CustomCaption _previousCaption;
  late CustomCaption _currentCaption;
  late CustomCaption _nextCaption;

  late Translate _intl;

  @override
  void initState() {
    _init();
    _initAnimations();
    super.initState();
  }

  @override
  void dispose() {
    _showArrow = false;
    _animationController.dispose();
    _diskAnimationController.dispose();
    _animateCtrl.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant MediaScreen oldWidget) {
    this._showArrow = false;
    super.didUpdateWidget(oldWidget);
  }

  _init() {
    _initAudio();
    _loadMediaDetails();
    _saveMediaView();
  }

  _loadMediaDetails() {
    _isLiked = widget.media.isLikedByCurrentUser;
    _likes = widget.media.likes;
    _comments = widget.media.comments;
  }

  _initAudio() {
    if(!widget.isDemo) {
      this._controller = VideoPlayerController.network(widget.media.path);
    } else {
      this._controller = VideoPlayerController.file(widget.media.mediaFile!);
    }
    this._controller.setLooping(true);
    this._controller.setVolume(1.0);

    this._initializeVideoPlayerFuture = _controller.initialize().catchError((e) => UtilService.notify(context: context, message: '${ Translate.of(context).translate('SCREEN.MEDIA.ALERT.FAILED') }'));

    if(widget.isCurrent) {
      this._controller.play();
    }
  }

  _initAnimations() {
    this._animationController = AnimationController(vsync: this, duration: const Duration(seconds: 1))..repeat();
    this._diskAnimationController = AnimationController(vsync: this, duration: new Duration(seconds: 7))..repeat();
    this._animation = new IntTween(begin: 10, end: 24).animate(_animationController);
    _animateText();
  }

  _animateText() {
    _animateCtrl = AnimationController(vsync: this, duration: Duration(seconds: 15), value: 0);
    _animateCtrl..addListener(() {
        if(_animateCtrl.isCompleted)
          _animateCtrl.repeat();

        if(_scrollCtrl.positions.isNotEmpty) {
          offset += 2.5;
          if(offset - 2.5 > _scrollCtrl.offset) {
            offset = 0.0;

            setState(() => _scrollCtrl.jumpTo(offset));
          }
        }
      });
    _animateCtrl.forward();
    _animateCtrl.repeat();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Material(
      color: Colors.transparent,
      child: GestureDetector(
        onTap: () => _controlPause(),
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Color.fromRGBO(92, 92, 92, 1),
                Colors.black,
              ],
            ),
          ),
          child: Stack(
            children: [
              FutureBuilder(
                future: _mediaService.getMediaDetails(media: widget.media),
                builder: (context, snapshot) {

                  if(snapshot.hasData) {

                    return StreamBuilder(
                      stream: snapshot.data as Stream<QuerySnapshot>,
                      builder: (context, snapshot) {

                        if(snapshot.hasData) {

                          final QuerySnapshot snap = snapshot.data as QuerySnapshot;

                          if(snap.docs.isNotEmpty)
                            _comments = snap.docs.elementAt(0).get('comments');
                        }

                        return Stack(
                          fit: StackFit.expand,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: VisibilityDetector(
                                    key: Key("${ widget.keyName }_${ widget.position }"),
                                    onVisibilityChanged: (VisibilityInfo info) {

                                      if(info.visibleFraction == 0 || (!widget.isCurrent && info.visibleFraction < 1)) {
                                        this._controller.setVolume(0.0);
                                        this._controller.pause();
                                        _stopAnimation();
                                        _resetPosition();
                                      } else {
                                        if(!this._showArrow) {
                                          this._controller.setVolume(1.0);
                                          this._controller.play();
                                          _startAnimation();
                                        }
                                      }
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.only( //50 tamanho do menuBottom (height)
                                        bottom: widget.isHome ? 60 : 0, //55
                                      ),
                                      child: Stack( //Body
                                        fit: StackFit.expand,
                                        children: [
                                          Image.asset('assets/images/anonymous.jpg', scale: 1, fit: BoxFit.cover),
                                          Container(
                                            decoration: BoxDecoration(
                                              gradient: LinearGradient(begin: Alignment.topRight, stops: const [
                                                0.1,
                                                0.9
                                              ],
                                              colors: [
                                                Colors.black.withOpacity(.90),
                                                Colors.black.withOpacity(.20),
                                              ]),
                                            ),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                gradient: LinearGradient(begin: Alignment.bottomRight, stops: const [
                                                  0.1,
                                                  0.9
                                                ], colors: [
                                                  Colors.black.withOpacity(.90),
                                                  Colors.black.withOpacity(.20),
                                                ]),
                                              ),
                                            ),
                                          ),
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Observer(
                                                builder: (_) {

                                                  return FutureBuilder(
                                                    future: mainStore.watson.captions,
                                                    builder: (context ,snapshot) {

                                                      if(snapshot.hasData) {

                                                        final List<CustomCaption> captions = snapshot.data as List<CustomCaption>;

                                                        if(captions.isNotEmpty) {

                                                            if(_currentCaptionNumber < captions.length) {
                                                              _currentCaption = captions.elementAt(_currentCaptionNumber);

                                                            final List<CaptionTimestamp> timestamps = _currentCaption.timestamps!;
                                                            final int endTime = (timestamps.last.endTime * 1000000).toInt();

                                                            if(_currentPosition.inMicroseconds >= endTime)
                                                              _currentCaptionNumber++;

                                                            _updatePosition();

                                                            timestamps.asMap().forEach((pos, timestamp) {
                                                              int startTime = (timestamp.startTime * 1000000).toInt();
                                                              int endTime = (timestamp.endTime * 1000000).toInt();

                                                              if(_currentPosition.inMicroseconds >= startTime && _currentPosition.inMicroseconds <= endTime)
                                                                _currentWords.add(pos);

                                                              if(_currentPosition.inMicroseconds > endTime)
                                                                _currentWords.clear();
                                                            });
                                                          } else {
                                                            _currentCaptionNumber = 0;
                                                            _currentCaption = captions.elementAt(_currentCaptionNumber);
                                                          }

                                                          _previousCaption = _currentCaptionNumber > 0 && captions.length > _currentCaptionNumber ? captions.elementAt(_currentCaptionNumber-1) : CustomCaption();
                                                          _nextCaption = (captions.length-1) > _currentCaptionNumber ? captions.elementAt(_currentCaptionNumber+1) : CustomCaption();

                                                          return Container(
                                                            padding: EdgeInsets.only(top: 100, left: 25, right: 25),
                                                            child: Column(
                                                              children: [
                                                                Visibility(
                                                                  visible: captions.length > 0 && captions.length > _currentCaptionNumber,
                                                                  child: Align(
                                                                    alignment: Alignment.topLeft,
                                                                    child: Text('${ _previousCaption.transcript ?? '' }',
                                                                      maxLines: 1,
                                                                      overflow: TextOverflow.ellipsis,
                                                                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.grey, fontSize: 20)),
                                                                  ),
                                                                ),
                                                                const SizedBox(height: 15),
                                                                Align(
                                                                  alignment: Alignment.topLeft,
                                                                  child: FutureBuilder(
                                                                    future: Future.value(_currentCaption),
                                                                    builder: (context, snapshot) {

                                                                      if(snapshot.hasData) {

                                                                        final CustomCaption caption = snapshot.data as CustomCaption;
                                                                        final List<String> currentCaptionList = caption.transcript!.split(" ");

                                                                        return RichText(
                                                                          text: TextSpan(
                                                                            children: List.generate(currentCaptionList.length, (index) {

                                                                              final String word = currentCaptionList.elementAt(index);

                                                                              return TextSpan(
                                                                                text: "${ word } ",
                                                                                style: TextStyle(color: _currentWords.contains(index) ? Colors.white : Colors.grey, fontWeight: FontWeight.bold, fontSize: 30),
                                                                              );
                                                                            }),
                                                                          ),
                                                                        );
                                                                      }

                                                                      return Container();
                                                                    },
                                                                  ),
                                                                ),
                                                                const SizedBox(height: 15),
                                                                Visibility(
                                                                  visible: (captions.length-1) > _currentCaptionNumber,
                                                                  child: Align(
                                                                    alignment: Alignment.topLeft,
                                                                    child: Text('${ _nextCaption.transcript ?? '' }',
                                                                      maxLines: 2,
                                                                      overflow: TextOverflow.ellipsis,
                                                                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.grey, fontSize: 20)),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          );
                                                        }
                                                      }

                                                      return Container();
                                                    },
                                                  );
                                                },
                                              ),
                                              FutureBuilder(
                                                future: this._controller.position,
                                                builder: (context, snapshot) {

                                                  if(snapshot.hasData) {

                                                    final Duration currentPosition = snapshot.data as Duration;
                                                    final double totalDuration = widget.media != null && widget.media.originalMedia != null ? ((widget.media.originalMedia!.duration) / 1000) : 0;

                                                    return Padding(
                                                      padding: const EdgeInsets.only(bottom: 25),
                                                      child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: [
                                                            Visibility(
                                                              visible: widget.mediaType == MediaType.STITCHED_AUDIO,
                                                              child: Visibility(
                                                                visible: currentPosition.inSeconds <= totalDuration,
                                                                child: MediaDetails(
                                                                  key: Key('stitched_audio'),
                                                                  isDemo: widget.isDemo,
                                                                  isOriginal: true,
                                                                  media: widget.media,
                                                                ),
                                                                replacement: MediaDetails(
                                                                  key: Key('my_audio'),
                                                                  isDemo: widget.isDemo,
                                                                  isOriginal: false,
                                                                  media: widget.media,
                                                                ),
                                                              ),
                                                              replacement: MediaDetails(
                                                                key: Key('my_audio'),
                                                                isDemo: widget.isDemo,
                                                                isOriginal: false,
                                                                type: widget.mediaType,
                                                                media: widget.media,
                                                              ),
                                                            ),
                                                            /*Container(
                                                              color: Colors.black,
                                                              width: MediaQuery.of(context).size.width,
                                                              height: 60,
                                                              child: AnimatedBuilder(
                                                                animation: this._animation,
                                                                builder: (BuildContext context, Widget child) {
                                                                  String frame = this._animation.value.toString().padLeft(2, '0');
                                                                  return Image.asset(
                                                                    'assets/images/image_split/audio_spectrum_effect/frame_${frame}_delay-0.04s.gif',
                                                                    scale: 2.8, //2.5
                                                                    gaplessPlayback: true,
                                                                  );
                                                                },
                                                              ),
                                                            ),*/
                                                          ],
                                                        ),
                                                    );
                                                  }

                                                  return Container();
                                                }
                                              ),
                                            ],
                                          ),
                                          Column(
                                            children: <Widget>[
                                              Expanded(
                                                child: Stack(
                                                  fit: StackFit.expand,
                                                  children: [
                                                    //Actions
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Column(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          crossAxisAlignment: CrossAxisAlignment.end,
                                                          children: [
                                                            Padding(
                                                              padding: const EdgeInsets.only(bottom: 28, right: 15),
                                                              child: Column(
                                                                mainAxisAlignment: MainAxisAlignment.end,
                                                                children: [
                                                                  Column( //52 width, 62 height
                                                                    children: <Widget>[
                                                                      GestureDetector(
                                                                        onTap: () async => await _changeLikeStatus(context: context),
                                                                        child: Image.asset( // 254, 179, 0, 1 - gold
                                                                          '${ ICON_ACTIONS_ASSET }/star-dark.png', //237, 196, 6, 1  yellow
                                                                          color: _isLiked ? Color.fromRGBO(237, 196, 6, 1) : Color(int.parse("FFE0E0E0", radix: 16)), scale: 1.2)
                                                                      ),
                                                                      const SizedBox(height: 4), //likes
                                                                      Text('${ _likes }', style: TextStyle(color: Colors.white, fontSize: 12, fontFamily: "Arial", fontWeight: FontWeight.normal, decoration: TextDecoration.none))
                                                                    ],
                                                                  ),
                                                                  const SizedBox(height: 18),
                                                                  FutureBuilder(
                                                                    future: mainStore.media.isOpenChatWindow,
                                                                    builder: (context, snapshot) {

                                                                      if(snapshot.hasData) {
                                                                        final bool isOpenChatWindow = snapshot.data as bool;

                                                                        if(isOpenChatWindow) {
                                                                          _openComments(context: context);

                                                                          mainStore.media.closeChatWindow();
                                                                        }
                                                                      }

                                                                      return Column(
                                                                        children: <Widget>[
                                                                          GestureDetector(
                                                                              onTap: () => _openComments(context: context),
                                                                              child: Image.asset('${ ICON_ACTIONS_ASSET }/chat.png', color: Color(int.parse("FFE0E0E0", radix: 16)), scale: 1.4)
                                                                          ),
                                                                          const SizedBox(height: 6), //comments
                                                                          Text('${ _comments }', style: TextStyle(color: Colors.white, fontSize: 12, fontFamily: "Arial", fontWeight: FontWeight.normal, decoration: TextDecoration.none))
                                                                        ],
                                                                      );
                                                                    },
                                                                  ),
                                                                  const SizedBox(height: 18),
                                                                  Column(
                                                                    children: <Widget>[
                                                                      GestureDetector(
                                                                        onTap: () => _openSharing(context: context),
                                                                        child: Image.asset('${ ICON_ACTIONS_ASSET }/share.png', color: Color(int.parse("FFE0E0E0", radix: 16)), scale: 1.4)
                                                                      ),
                                                                      const SizedBox(height: 6),
                                                                      //Text('${ audio.shared ?? 0 }', style: TextStyle(color: Colors.white, fontSize: 12.0, fontFamily: "Arial", fontWeight: FontWeight.normal, decoration: TextDecoration.none))
                                                                    ],
                                                                  ),
                                                                  const SizedBox(height: 18),
                                                                  GestureDetector(
                                                                    onTap: () => _openMore(context: context),
                                                                    child: Column(
                                                                      children: [
                                                                        Icon(Icons.more_horiz, color: Color(int.parse("FFE0E0E0", radix: 16)), size: 30),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  const SizedBox(height: 18),
                                                                ],
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets.only(right: 8, bottom: 8),
                                                              child: AnimatedBuilder(
                                                                animation: this._diskAnimationController,
                                                                builder: (BuildContext context, Widget? widget) {

                                                                  return Transform.rotate(
                                                                    angle: this._diskAnimationController.value * 6.3,
                                                                    child: widget ?? Container(),
                                                                  );
                                                                },
                                                                child: GestureDetector(
                                                                  onTap: () => _seeAudios(context: context),
                                                                  child: Container(
                                                                    width: 42, //60
                                                                    height: 42, //60
                                                                    decoration: BoxDecoration(
                                                                      color: Colors.white60, //Color(int.parse("FF2C2C2C", radix: 16)),
                                                                      borderRadius: BorderRadius.circular(15),
                                                                      border: Border.all(
                                                                        width: 10,
                                                                        color: Colors.white60, //Color(int.parse("FF2C2C2C", radix: 16))
                                                                      ),
                                                                    ),
                                                                    child: ProfilePhoto(
                                                                      width: 25,
                                                                      height: 25,
                                                                      user: widget.media.createdBy!,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              const SizedBox(height: 8),
                                              AnimatedText(
                                                width: MediaQuery.of(context).size.width * 0.85, //0.60
                                                alignment: MainAxisAlignment.center,
                                                text: widget.media != null &&
                                                    widget.media.title != null &&
                                                    widget.media.title.isNotEmpty ?
                                                widget.media.title : '${ _intl.translate('SCREEN.MEDIA.DETAILS.AUDIO_NAME') }',
                                                icon: Icons.music_note,
                                                direction: Axis.horizontal,
                                                scrollCtrl: _scrollCtrl,
                                              ),
                                              const SizedBox(height: 8),//Body
                                              Container(
                                                width: MediaQuery.of(context).size.width,
                                                height: 6,
                                                child: VideoProgressIndicator(
                                                    this._controller,
                                                    allowScrubbing: true,
                                                    colors: VideoProgressColors(backgroundColor: Colors.white38, playedColor: Colors.white)
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () => _controlPause(),
                                      child: Opacity(
                                        opacity: _showArrow && !this._controller.value.isPlaying ? 1 : 0,
                                        child: Image.asset("assets/icons/arrow.png", color: Colors.white70),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        );
                      },
                    );
                  }

                  return Center(
                    child: Loading(),
                  );
                },
              ),
              Visibility(
                visible: widget.type.index != MediaScreenType.DEFAULT.index,
                child: Header(
                  color: Colors.transparent,
                  hasBorder: false,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _controlPause() {
    if(this._controller != null) {
      if(this._controller.value.isPlaying) {
        this._controller.pause();
        _stopAnimation();

        setState(() => this._showArrow = true);
      } else {
        this._controller.play();
        _startAnimation();

        setState(() => this._showArrow = false);
      }
    }
  }

  _saveMediaView() async { //View media
    if(widget.type.index == MediaScreenType.HOME.index)
      await mainStore.media.saveLocalMediaView(media: widget.media);
  }

  _startAnimation() {
    if(mounted) {
      _animationController.repeat();
      _diskAnimationController.repeat();
      _animateCtrl.repeat();
    }
  }

  _stopAnimation() {
    if(mounted) {
      _animationController.stop();
      _diskAnimationController.stop();
      _animateCtrl.stop();
    }
  }

  _changeLikeStatus({ required BuildContext context }) async {
    if(widget.isDemo) {
      UtilService.notify(context: context, message: '');
      return;
    }

    if(_authService.isAnonymous())
      Navigator.of(context).push(UtilService.createRoute(AnonymousProfileScreen()));
    else {
      if(!_isLiked) {
        setState(() {
          _likes += 1;
          _isLiked = true;
        });
        await mainStore.media.addLike(media: widget.media);
      } else {
        setState(() {
          _likes -= 1;
          _isLiked = false;
        });
        await mainStore.media.removeLike(media: widget.media);
      }
    }
  }

  _openComments({ required BuildContext context }) async {
    if(widget.isDemo) {
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.MEDIA.ALBUM.DETAILS.NOTIFICATION.DEMO') }');
      return;
    }

    bool isAnonymous = (await this._authService.isAnonymous());

    showModalBottomSheet<void>(
      context: context,
      elevation: 0,
      barrierColor: Colors.white.withAlpha(1),
      builder: (BuildContext context) {

        return CommentModal(
          media: widget.media,
          isAnonymous: isAnonymous,
        );
      },
    );
  }

  _openSharing({ required BuildContext context }) async {
    if(widget.isDemo) {
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.MEDIA.ALBUM.DETAILS.NOTIFICATION.DEMO') }');
      return;
    }

    showModalBottomSheet<void>( //loading
      context: context, elevation: 0, barrierColor: Colors.white.withAlpha(1),
      isDismissible: false,
      builder: (BuildContext context) {
        String message = _intl.translate('SCREEN.MEDIA.MODAL.LOADING.SHARE_MEDIA');
        return LoadingModal(message: message);
      },
    );

    final Uint8List media = await UtilService.getFileFromUrl(url: widget.media.path);
    //await Share.file('Audio', '${ widget.media.id }.mp4', media, 'video/mp4');

    final Directory directory = await getTemporaryDirectory();
    final File file = File('${ directory.path }/${ widget.media.id }.mp3');
    file.writeAsBytes(media);

    await Share.shareFiles([ '${ file.path }' ], text: 'Audio', mimeTypes: ['audio/mpeg']);

    Navigator.pop(context);
  }

  _openMore({ required BuildContext context }) async {
    if(widget.isDemo) {
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.MEDIA.ALBUM.DETAILS.NOTIFICATION.DEMO') }');
      return;
    }

    showModalBottomSheet<void>( //loading
      context: context, elevation: 0, barrierColor: Colors.white.withAlpha(1),
      isDismissible: true,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {

        return Container(
          padding: EdgeInsets.all(25),
          height: 125,
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () => _downloadAudio(context: context),
                child: Container(
                  padding: EdgeInsets.all(12),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 0.25),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.save_alt, color: Color(int.parse("FFE0E0E0", radix: 16)), size: 22),
                      const SizedBox(height: 8),
                      Text('Save', style: TextStyle(color: Colors.grey, fontSize: 16)),
                    ],
                  ),
                ),
              ),
              const SizedBox(width: 12),
              GestureDetector(
                onTap: () {
                  mainStore.studio.addMedia(media: widget.media);

                  Navigator.of(context).push(UtilService.createRoute(RecordingStudio(type: MediaType.DUET)));
                },
                child: Container(
                  padding: EdgeInsets.all(12),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 0.25),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Stack(
                        children: [
                          Container(
                            width: 12,
                            height: 17,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.white, width: 0.5),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 6, top: 4),
                            width: 12,
                            height: 17,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey, width: 0.5),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 8),
                      Text('Duet', style: TextStyle(color: Colors.grey, fontSize: 16)),
                    ],
                  ),
                ),
              ),
              const SizedBox(width: 12),
              GestureDetector(
                onTap: () => Navigator.push(context, UtilService.createRoute(AudioCut(media: widget.media))),
                child: Container(
                  padding: const EdgeInsets.all(12),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 0.25),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.cut, color: Color(int.parse("FFE0E0E0", radix: 16)), size: 20),
                      const SizedBox(height: 8),
                      Text('Stitch', style: TextStyle(color: Colors.grey, fontSize: 16)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );

  }

  _downloadAudio({ required BuildContext context }) async {

  }

  _seeAudios({ required BuildContext context }) async {
    if(widget.isDemo) {
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.MEDIA.ALBUM.DETAILS.NOTIFICATION.DEMO') }');
      return;
    }

    Navigator.of(context).push(UtilService.createRoute(AudioUsedGroup()));
  }

  _updatePosition() async {
    final Duration position = (await this._controller.position)!;

    setState(() => this._currentPosition = position);
  }

  _resetPosition() async {
    final Duration duration = (await this._controller.position)!;

    if(duration.inMilliseconds > 0)
      this._controller.seekTo(Duration(milliseconds: 0));
  }

}
