
import 'dart:ui';

import 'package:audioplayers/audioplayers.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/studio/recording_studio.dart';

class AudioUsedData extends StatefulWidget {

  final Media media;

  const AudioUsedData({Key? key,  required this.media }) : super(key: key);

  @override
  _AudioUsedDataDataState createState() => _AudioUsedDataDataState();
}

class _AudioUsedDataDataState extends State<AudioUsedData> {

  final _authService = AuthenticationService();
  final _audioPlayer = AudioPlayer();

  Media _media = Media();
  bool _isPlaying = false;

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    _audioPlayer.stop();
    super.dispose();
  }

  _init() async {
    _media = widget.media;
    _isPlaying = false;
  }

  _controlAudioPlayer() {
    if(_media != null) {
      _audioPlayer.onPlayerStateChanged.listen((s) {
        if ( mounted && (s == PlayerState.completed || s == PlayerState.stopped)) {
          setState(() => _isPlaying = false );
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    _controlAudioPlayer();

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(bottom: BorderSide(color: Colors.grey, width: 0.3)),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 30, bottom: 30),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  if(_media != null) {
                      if(!_isPlaying)
                        _audioPlayer.play(new AssetSource(_media.path));
                      else
                        _audioPlayer.stop();
                    setState(() => _isPlaying = !_isPlaying );
                  } else
                    UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.MEDIA.AUDIO.ALERT.UNAVAILABLE') }');
                },
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    image: _media == null || _media.image == null || _media.image.isEmpty ?
                      DecorationImage(image: AssetImage('assets/icons/headphone-light.png')) :
                      DecorationImage(image: NetworkImage(_media.image)),
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 4.0, sigmaY: 4.0),
                      child: Stack(
                        fit: StackFit.expand,
                        children: [
                          !_isPlaying ?
                          Image.asset('assets/icons/arrow.png', color: Colors.black.withOpacity(0.65), scale: 1.5) :
                          Image.asset('assets/icons/pause.png', scale: 0.75)
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(width: 12),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('${ _media != null ? _media.title : 'No title' }', style: TextStyle(fontWeight: FontWeight.bold)),
                  Padding(
                    padding: const EdgeInsets.only(top: 15, bottom: 15),
                    child: Container(
                      width: 175,
                      child: Text('${ _media != null ? _media.message : 'No description' }',
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      if(await _authService.isAnonymous()) {
                        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.MEDIA.AUDIO.ALERT.NO_LOGGED_IN') }');
                        //Navigator.of(context).push(UtilService.createRoute(AnonymousProfileScreen()));
                      } else {
                        if(_media != null) {
                          Navigator.of(context).push(UtilService.createRoute(RecordingStudio())); //Para regravar os áudios
                        } else
                          UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.MEDIA.AUDIO.ALERT.UNAVAILABLE') }');
                      }
                    },
                    child: Container(
                        padding: EdgeInsets.only(top: 8, bottom: 8, left: 25, right: 25),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Colors.black, width: 0.5),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Text('${ _intl.translate('SCREEN.MEDIA.AUDIO.BUTTON.REMIX_AUDIO') }',
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15))
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
