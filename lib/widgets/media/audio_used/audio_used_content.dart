
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:bridge/widgets/profile/content/audio_mini/audio_mini_group.dart';
import 'package:bridge/main.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:bridge/shared/internet_loading_failed.dart';

class AudioUsedContent  extends StatelessWidget {

  const AudioUsedContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      color: Colors.white,
      child: FutureBuilder(
        future: mainStore.media.reRecordedMedias,
        builder: (context, snapshot) {

          Image noData = Image.asset('assets/icons/music_dark.png', color: Colors.grey[400], scale: 1.6);

          switch(snapshot.connectionState) {
            case ConnectionState.none:
              return InternetLoadingFailed();
              break;
            case ConnectionState.waiting:
              return Loading();
              break;
            case ConnectionState.active:
              return NoResultFound(image: noData);
              break;
            case ConnectionState.done: {

              if(snapshot.hasData) {

                final Query query = snapshot.data as Query;

                return Padding(
                  padding: const EdgeInsets.all(8),
                  child: AudioMiniGroup(
                    type: MediaScreenType.MINI,
                    color: Colors.purple,
                    query: query,
                  ),
                );
              }

              return NoResultFound(image: noData);
            } break;
          }

          return NoResultFound(image: noData);
        },
      ),
    );
  }
}
