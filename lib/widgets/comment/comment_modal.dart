
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/domain/model/comment/comment.dart';
import 'package:bridge/main.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/widgets/comment/comment_group.dart';

//Modal Bottom Sheet
class CommentModal extends StatefulWidget {

  Media media;
  bool isAnonymous;

  CommentModal({Key? key, required this.media, required this.isAnonymous }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CommentModalState();
}

class _CommentModalState extends State<CommentModal> {

  final _commentTextController = TextEditingController();

  late Translate _intl;

  @override
  void initState() {
    mainStore.comment.addMediaComment(media: widget.media);
    super.initState();
  }

  @override
  void dispose() {
    mainStore.comment.clearMediaComments();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Container(
      height: 500,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(top: BorderSide(color: Colors.grey, width: 0.5)),
      ),
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(bottom: BorderSide(color: Colors.grey, width: 0.30)),
                ),
                height: 50,
                child: Center(
                  child: Text('${ _intl.translate('SCREEN.MEDIA.COMMENT.TITLE') }',
                    style: TextStyle(
                      color: Colors.black,
                      decoration: TextDecoration.none,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Arial')
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  color: Colors.white,
                  child: CommentGroup(
                    media: widget.media,
                  ),
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                color: Colors.white,
                height: 50,
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width-45,
                      child: CustomField(
                          controller: _commentTextController,
                          readOnly: widget.isAnonymous,
                          autofocus: false,
                          obscureText: false,
                          textAlign: TextAlign.start,
                          backgroundColor: Colors.white,
                          textColor: Colors.grey,
                          borderColor: Colors.white,
                          text: '${ _intl.translate('SCREEN.MEDIA.COMMENT.FIELD.COMMENT') }'
                      ),
                    ),
                    GestureDetector(
                        onTap: () async => _saveComment(),
                        child: Icon(Icons.play_arrow, color: Colors.grey, size: 30)
                    ),
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).viewInsets.bottom * 0.99), // When open keyboard
            ],
          ),
        ],
      ),
    );
  }

  _saveComment() async {
    await mainStore.comment.saveComment(
      media: widget.media,
      comment: Comment(
        message: _commentTextController.text,
      ),
    );

    _commentTextController.clear();
  }

}
