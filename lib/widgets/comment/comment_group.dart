
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/comment/comment_service.dart';
import 'package:shimmer/shimmer.dart';

class CommentGroup extends StatefulWidget {

  Media media;

  CommentGroup({Key? key, required this.media }) : super(key: key);

  @override
  _CommentGroupState createState() => _CommentGroupState();
}

class _CommentGroupState extends State<CommentGroup> {

  final ScrollController _scrollController = ScrollController();
  final _commentService = CommentService();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
      future: mainStore.comment.mediaComments,
      builder: (context, snapshot) {

        if(snapshot.hasData) {

          final Query query = snapshot.data as Query;

          return Container();

          /*return RealtimePagination(
            itemsPerPage: 10,
            initialLoading: Loading(),
            emptyDisplay: NoResultFound(icon: Icons.comment),
            bottomLoader: Loading(),
            itemBuilder: (int index, BuildContext context, DocumentSnapshot document) {

              return FutureBuilder(
                future: _commentService.makeComment(comment: document),
                builder: (commentContext, commentSnapshot){
                  if(commentSnapshot.hasData) {

                    Comment comment = commentSnapshot.data as Comment;

                    return CommentView(comment: comment, media: widget.media);
                  }

                  return showLoading();

                },
              );
            },
            query: query,
            scrollController: _scrollController,
            customPaginatedBuilder: (itemCount, controller, itemBuilder) {

              return NotificationListener<OverscrollIndicatorNotification>(
                onNotification: (overscroll) {
                  overscroll.disallowIndicator();

                  return false;
                },
                child: ListView.builder(
                  padding: const EdgeInsets.all(0),
                  physics: const PageScrollPhysics(),
                  itemCount: itemCount,
                  scrollDirection: Axis.vertical,
                  controller: controller,
                  itemBuilder: itemBuilder,
                ),
              );
            },
          );*/
        }

        return Container();
      },
    );

    /*return StreamBuilder(
      stream: mainStore.comment.mediaComments,
      builder: (context, snapshot) {

        if(snapshot.hasData) {

          final QuerySnapshot data = snapshot.data;
          final List<QueryDocumentSnapshot> comments = data.docs;

          return ListView.builder(
            padding: EdgeInsets.all(0),
            physics: const PageScrollPhysics(),
            itemCount: comments.length,
            addAutomaticKeepAlives: true,
            scrollDirection: Axis.vertical,
            itemBuilder: (context, position){

              return FutureBuilder(
                future: _commentService.makeComment(comment: comments.elementAt(position)),
                builder: (commentContext, commentSnapshot){
                  if(commentSnapshot.hasData){

                    Comment comment = commentSnapshot.data;

                    return CommentView(comment: comment, media: widget.media);
                  }

                  return showLoading();

                },
              );
            },
          );
        }

        return NoResultFound(icon: Icons.comment);
      },
    );*/
  }

  Widget showLoading(){
    return Padding(
      padding: const EdgeInsets.all(12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Container(
              color: Colors.transparent,
              child: Row(
                children: [
                  Shimmer.fromColors(
                    baseColor: Color.fromRGBO(224, 224, 224, 1),
                    highlightColor: Colors.white,
                    child: Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(100))
                      ),
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded( //Ocupa o meio inteiro
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Shimmer.fromColors(
                            baseColor: Color.fromRGBO(224, 224, 224, 1),
                            highlightColor: Colors.white,
                            child: Container(
                              color: Colors.white,
                              width: 60,
                              height: 16, //14
                            ),
                          ),
                          const SizedBox(height: 5),
                          Shimmer.fromColors(
                            baseColor: Color.fromRGBO(224, 224, 224, 1),
                            highlightColor: Colors.white,
                            child: Container(
                              color: Colors.white,
                              width: 180,
                              height: 16, //14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Shimmer.fromColors(
            baseColor: Color.fromRGBO(224, 224, 224, 1),
            highlightColor: Colors.white,
            child: Container(
              color: Colors.white,
              width: 25,
              height: 25,
            ),
          ),
        ],
      ),
    );
  }

}
